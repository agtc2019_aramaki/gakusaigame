//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							HitCheckInfo.cpp													//
//																								//
//							作成日時 : 2020.10/20												//
//							作成者   : 天童律希													//
//																								//
//							ヒットチェック系関数まとめ											//
//							壁などは二枚セットで使うのでグローバル関数にする					//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include <math.h>
#include "Common.h"

//==============================================================//
//		プレイヤー対床											//
//--------------------------------------------------------------//
//		IN  : ヒットチェック情報,プレイヤー速度					//
//		OUT : 補正後のプレイヤー速度							//
//==============================================================//
HitResultStg	HitCheck_Player_Stage	( const HitCheckInfo &arg_HitPlayer, const VECTOR &arg_Spd, const HitCheckInfo* argp_HitStage )
{
	HITRESULT_LINE	HitRes ;															// --- ヒットチェック結果
	VECTOR			CheckVec1 = VAdd( arg_HitPlayer.m_Param[eHitStart], arg_Spd ) ;		// --- ヒットチェック線分始点
	VECTOR			CheckVec2 = VAdd( arg_HitPlayer.m_Param[eHitEnd], arg_Spd ) ;		// --- ヒットチェック線分終点
	HitResultStg	ReturnRes ;													// --- 返値用データ

	for ( int i = 0 ; i < 2 ; i++ ) {
		HitRes = HitCheck_Line_Triangle(
			CheckVec1,
			CheckVec2,
			argp_HitStage[i].m_Param[0],
			argp_HitStage[i].m_Param[1],
			argp_HitStage[i].m_Param[2]
		) ;

		if ( HitRes.HitFlag == 1 ) {	// --- ヒットしたとき
			ReturnRes.Hit	= TRUE ;
			ReturnRes.Speed	= VSub( arg_Spd, VSub(CheckVec1,HitRes.Position) ) ;	// --- 速度からめり込む文を引く
			return ReturnRes ;
		}
	}

	ReturnRes.Hit	= FALSE ;
	ReturnRes.Speed	= arg_Spd ;

	return ReturnRes ;	// --- ヒットしていないとき引数のスピードをそのまま返す
}

//==============================================================//
//		プレイヤー対壁											//
//--------------------------------------------------------------//
//		IN  : ヒットチェック情報,プレイヤー速度					//
//		OUT : 壁までの最短距離									//
//==============================================================//
float HitCheck_Player_Wall( const HitCheckInfo &arg_HitPlayer, const VECTOR &arg_Spd, const HitCheckInfo* argp_HitStage )
{
	HitResultSide	Res ;				// --- 結果
	float			Length[2] ;			// --- 壁との距離
//	float			MinLength ;			// --- 壁との距離
//	float			OverDif ;			// --- どれくらいめり込んだか
//	float			SpdAng ;			// --- 進んでいる方向
	VECTOR			CheckNowPos[2] ;	// --- 現在のヒットチェック座標
	VECTOR			CheckMovePos[2] ;	// --- 移動後のヒットチェック座標

	// --- 現在と移動後のヒットチェック座標を保存 --- //
	for ( int i = 0 ; i < 2 ; i++ ) {
		CheckNowPos[i] = arg_HitPlayer.m_Param[i] ;
		CheckMovePos[i] = VAdd( arg_HitPlayer.m_Param[i], arg_Spd ) ;
	}

	Res.Hit = FALSE ;

	// --- 移動後のヒットチェック座標と二枚の三角形との距離を比較
	for ( int i = 0 ; i < 2 ; i++ ) {
		Length[i] = Segment_Triangle_MinLength( 
			CheckMovePos[0], CheckMovePos[1],
			argp_HitStage[i].m_Param[0],
			argp_HitStage[i].m_Param[1], 
			argp_HitStage[i].m_Param[2] ) ;
	}

	if ( Length[0] < Length[1] ) {
		return Length[0] ;
	}
	return Length[1] ;

/*	// --- 移動後のヒットチェック座標が壁と接触していたら
	if ( (Length[0] < PLAYER_HITCHECK_WIDTH) || (Length[1] < PLAYER_HITCHECK_WIDTH) ) {
		// --- 二枚の三角形の内、近い方の距離を保存
		if ( Length[0] < Length[1] ) {
			MinLength = Length[0] ;
		}
		else {
			MinLength = Length[1] ;
		}
		Res.Hit		= TRUE ;
		OverDif		= PLAYER_HITCHECK_WIDTH - MinLength ;	// --- どれくらいめり込んだか
		// --- 移動量線分と壁のヒットチェック
		if ( HitCheck_MoveSegment_Wall(CheckNowPos,CheckMovePos,argp_HitStage) ) {
			OverDif += PLAYER_HITCHECK_WIDTH ;				// --- ヒットしていたらヒットチェック半径を足す
		}
		SpdAng		= atan2( arg_Spd.z, arg_Spd.x ) ;		// --- 速度の方向
		Res.xDif	= OverDif * cos( SpdAng ) ;				// --- X補正値
		Res.zDif	= OverDif * sin( SpdAng ) ;				// --- Z補正値

		return Res ;
	}
		
	return Res ;		// --- 結果を返す*/
}

//==============================================================//
//		移動量の線分対壁(プレイヤー対壁の中で呼び出す)			//
//--------------------------------------------------------------//
//		IN  : 移動前後の座標,壁のヒットチェック情報				//
//		OUT : 壁までの最短距離									//
//==============================================================//
int HitCheck_MoveSegment_Wall( const VECTOR* arg_NowPos, const VECTOR* arg_MovePos, const HitCheckInfo* argp_HitStage )
{
	int Res ;	// --- 結果

	// --- 二枚の三角形と比較
	for ( int i = 0 ; i < 2 ; i++ ) {
		// --- 二本の線分と比較
		for ( int j = 0 ; j < 2 ; j++ ) {
			Res = HitCheck_Line_Triangle(
				arg_NowPos[i], arg_MovePos[i],
				argp_HitStage[j].m_Param[0],
				argp_HitStage[j].m_Param[1],
				argp_HitStage[j].m_Param[2] ).HitFlag ;
			// --- ヒットしていたら
			if ( Res ) {
				return TRUE ;
			}
		}
	}

	return FALSE ;
}

//==============================================================//
//		弾丸対床												//
//--------------------------------------------------------------//
//		IN  : ヒットチェック情報								//
//		OUT : ヒットしたかどうか								//
//==============================================================//
int HitCheck_Bullet_Stage ( const HitCheckInfo &arg_HitBullet, const HitCheckInfo* argp_HitStage )
{
	int Res ;

	// --- 原点からの距離
	if ( VSquareSize(arg_HitBullet.m_Param[0]) >= (BULLET_DISAPLINE * BULLET_DISAPLINE) ){
		return TRUE ;
	}

	// --- 二枚の三角形とそれぞれチェック
	for ( int i= 0 ; i < 2 ; i++ ) {
		Res = HitCheck_Sphere_Triangle( 
				arg_HitBullet.m_Param[0],
				arg_HitBullet.m_CapRad,
				argp_HitStage[i].m_Param[0],
				argp_HitStage[i].m_Param[1],
				argp_HitStage[i].m_Param[2] ) ;
		if ( Res ) {
			return TRUE ;
		}
	}

	return FALSE ;
}

//==============================================================//
//		弾丸対箱												//
//--------------------------------------------------------------//
//		IN  : ヒットチェック情報								//
//		OUT : ヒットしたかどうか								//
//==============================================================//
int HitCheck_Bullet_Box ( const HitCheckInfo &arg_HitBullet, const HitCheckInfo* argp_HitBox )
{
	int Res ;

	// --- 二枚の三角形とそれぞれチェック
	for ( int i= 0 ; i < 2 ; i++ ) {
		Res = HitCheck_Sphere_Triangle( 
				arg_HitBullet.m_Param[0],
				arg_HitBullet.m_CapRad,
				argp_HitBox[i].m_Param[0],
				argp_HitBox[i].m_Param[1],
				argp_HitBox[i].m_Param[2] ) ;
		if ( Res ) {
			return TRUE ;
		}
	}

	return FALSE ;
}

//==============================================================//
//		プレイヤー対弾丸										//
//--------------------------------------------------------------//
//		IN  : ヒットチェック情報								//
//		OUT : ヒットしたかどうか								//
//==============================================================//
int HitCheck_Player_Bullet( const HitCheckInfo &arg_HitPlayer, const HitCheckInfo &arg_HitBullet )
{
	int Res ;

	// --- 球をカプセルとしてカプセル同士のヒットチェック
	Res = HitCheck_Capsule_Capsule(
		arg_HitPlayer.m_Param[0],
		arg_HitPlayer.m_Param[1],
		arg_HitPlayer.m_CapRad,
		arg_HitBullet.m_Param[0],
		arg_HitBullet.m_Param[0],
		arg_HitBullet.m_CapRad ) ;

	return Res ;
}

//==============================================================//
//		プレイヤー対プレイヤー									//
//--------------------------------------------------------------//
//		IN  : ヒットチェック情報,速度							//
//		OUT : プレイヤー同士の距離								//
//==============================================================//
HitResultSide Hitcheck_Player_Player( const HitCheckInfo &arg_HitMainPl, const VECTOR &arg_Spd,  const HitCheckInfo &arg_HittargetPl )
{
	// --- プレイヤーヒットチェック半径*2の二乗
	float			CheckLen = (PLAYER_HITCHECK_WIDTH + PLAYER_HITCHECK_WIDTH) * (PLAYER_HITCHECK_WIDTH + PLAYER_HITCHECK_WIDTH) ;
	float			Length ;		// --- プレイヤー同士の距離
	VECTOR			CheckVecSt[2] ;	// --- ヒットチェック線分
	VECTOR			CheckVecEn[2] ;
	HitResultSide	Res ;			// --- 結果
	float			xDif ;			// --- X座標の差
	float			zDif ;			// --- Z座標の差
	float			HitAng ;		// --- ヒットした方向
	float			OverDif ;		// --- めり込みの絶対値

	// --- 自分のヒットチェック情報格納
	CheckVecSt[0] = VAdd( arg_HitMainPl.m_Param[eHitStart], arg_Spd ) ;
	CheckVecEn[0] = VAdd( arg_HitMainPl.m_Param[eHitEnd], arg_Spd ) ;

	// --- 相手のヒットチェック情報格納
	CheckVecSt[1] = arg_HittargetPl.m_Param[eHitStart] ;
	CheckVecEn[1] = arg_HittargetPl.m_Param[eHitEnd] ;

	// --- 線分の最短距離を求める
	Length = Segment_Segment_MinLength_Square( 
		CheckVecSt[0],
		CheckVecEn[0],
		CheckVecSt[1],
		CheckVecEn[1] ) ;

	// --- ヒットチェック
	if ( Length < CheckLen ) {
		Res.Hit	= TRUE ;										// --- ヒットしている
		xDif	= CheckVecSt[0].x - CheckVecSt[1].x ;			// --- X座標の差
		zDif	= CheckVecSt[0].z - CheckVecSt[1].z ;			// --- Z座標の差
		HitAng	= atan2( zDif, xDif ) ;							// --- ヒットした角度
		OverDif	= sqrt( CheckLen ) - sqrt( Length ) ;			// --- どれくらいめり込んだか
		Res.xDif = OverDif * cos( HitAng ) ;					// --- X補正値
		Res.zDif = OverDif * sin( HitAng ) ;					// --- Z補正値
	}
	else {
		Res.Hit	= FALSE ;		// --- ヒットしていない
	}

	return Res ;
}


