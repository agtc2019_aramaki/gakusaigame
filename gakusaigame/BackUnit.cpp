//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							BackUnit.cpp														//
//																								//
//							作成日時 : 2020.10/9												//
//							作成者   : 天童律希													//
//																								//
//							背中装備データクラスメソッド定義									//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include "Common.h"

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//	グローバルデータ
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//

//==============================================================//
//		データ初期セット										//
//--------------------------------------------------------------//
//		IN  : 保存データ										//
//		OUT : 終了判定											//
//==============================================================//
int BackUnitData::InitializeData( int arg_RelSpd, int arg_Type, int arg_gAnimNo )
{
	m_Type			= arg_Type ;			// --- 種類
	m_gAnimNo		= arg_gAnimNo ;			// --- アニメーションハンドル	

	return 0 ;
}

//==============================================================//
//		発動													//
//--------------------------------------------------------------//
//		IN  : ターゲット,装備しているフレーム					//
//		OUT : 終了判定											//
//==============================================================//
int BackUnitData::Activate( VECTOR* arg_Target, int arg_ActModel, int arg_PlayerNo )
{
	int		BulNum ;			// --- 弾丸配列番号
	int		ShotFrameIdx ;		// --- 発射フレームインデックス
	MATRIX	ShotMatrix ;		// --- 発射フレームのマトリックス
	VECTOR	ShotPos ;			// --- 発射位置
	float	f ;					// --- 発射角補正

	// --- 私は大砲よ --------------------------------------------------------- //
	if ( m_Type == eUnitCannon ) {
		BulNum = BulletObj::Search() ;
		if ( BulNum != -1 ) {
			ShotFrameIdx	= MV1SearchFrame( arg_ActModel, "Muzzule" ) ;
			ShotMatrix		= MV1GetFrameLocalWorldMatrix( arg_ActModel, ShotFrameIdx ) ;
			ShotPos			= MGetTranslateElem( ShotMatrix ) ;
			g_Bullet[BulNum].SetPosition( ShotPos ) ;												// --- 弾丸の座標をセット
			g_Bullet[BulNum].InitBul( &g_BulletData[eBulCannon], arg_Target, arg_PlayerNo ) ;		// --- 弾丸データセット
			g_Bullet[BulNum].SetFirstSpeed( 0.0f ) ;												// --- 初速セット

//			g_Sound.Play( SOUND_MISSILESE_NO ) ;
		}
	}
	// --- 私はミサイルよ ----------------------------------------------------- //
	else {
//		g_Sound.Play( SOUND_CANNONSE_NO ) ;

		// --- 左ポッド
		ShotFrameIdx	= MV1SearchFrame( arg_ActModel, "Left_MIssile_Bone" ) ;
		ShotMatrix		= MV1GetFrameLocalWorldMatrix( arg_ActModel, ShotFrameIdx ) ;
		ShotPos			= MGetTranslateElem( ShotMatrix ) ;
		f = -30.0f ;
		for ( int i = 0 ; i < 4 ; i++ ) {
			BulNum = BulletObj::Search() ;
			if ( BulNum != -1 ) {
				g_Bullet[BulNum].SetPosition( ShotPos ) ;												// --- 弾丸の座標をセット
				g_Bullet[BulNum].InitBul( &g_BulletData[eBulMissile], arg_Target, arg_PlayerNo ) ;		// --- 弾丸データセット
				g_Bullet[BulNum].SetFirstSpeed( f ) ;													// --- 初速セット

				f += 10.0f ;
			}
		}
		// --- 右ポッド
		ShotFrameIdx	= MV1SearchFrame( arg_ActModel, "Right_Missile_Bone" ) ;
		ShotMatrix		= MV1GetFrameLocalWorldMatrix( arg_ActModel, ShotFrameIdx ) ;
		ShotPos			= MGetTranslateElem( ShotMatrix ) ;
		f = 30.0f ;
		for ( int i = 0 ; i < 4 ; i++ ) {
			BulNum = BulletObj::Search() ;
			if ( BulNum != -1 ) {
				g_Bullet[BulNum].SetPosition( ShotPos ) ;												// --- 弾丸の座標をセット
				g_Bullet[BulNum].InitBul( &g_BulletData[eBulMissile], arg_Target, arg_PlayerNo ) ;		// --- 弾丸データセット
				g_Bullet[BulNum].SetFirstSpeed( f ) ;													// --- 初速セット

				f -= 10.0f ;
			}
		}
	}

	return 0 ;
}

//==============================================================//
//		指定データを取得										//
//--------------------------------------------------------------//
//		IN  : 必要データの番号									//
//		OUT : データ											//
//==============================================================//
int BackUnitData::GetData( int arg_eData )
{
	switch ( arg_eData )
	{
		case eUnitType :		return m_Type ;				// --- 種類
		case eUnitModel :		return m_hModelEq ;			// --- モデルハンドル
		case eUnitAnimNo :		return m_gAnimNo ;			// --- アニメーションハンドル
	}

	return -1 ;
}

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//	メンバーデータ
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//

//==============================================================//
//		参照するグローバルデータセット							//
//--------------------------------------------------------------//
//		IN  : 参照するグローバルデータ							//
//		OUT : 終了判定											//
//==============================================================//
int BackUnitMember::SetGrobalDeta( int arg_UnitNo )
{
	mp_GrobalData	= &g_BackData[arg_UnitNo] ;									// --- ポインタセット
	m_Type			= mp_GrobalData->GetData(eUnitType) ;						// --- 種類セット
	m_hModel		= MV1DuplicateModel( mp_GrobalData->GetData(eUnitModel) ) ;	// --- モデル複製
	m_Animation.SetAnim( mp_GrobalData->GetData(eUnitAnimNo), 1 ) ;				// --- アニメーション複製
	m_Animation.AnimAttach( m_hModel, 0 ) ;
	m_Animation.m_AnimSpd = 1.0f ;

	m_Activating	= FALSE ;
	m_ActivateFlg	= TRUE ;

	m_NowBullet		= DATA_BACKUNIT_MAXBUL ;

	return 0 ;
}

//==============================================================//
//		発射													//
//--------------------------------------------------------------//
//		IN  : ターゲット										//
//		OUT : 終了判定											//
//==============================================================//
int BackUnitMember::Shot( VECTOR* arg_Target, int arg_PlayerNo )
{
	// --- 発動
	if ( m_Type != eUnitWing ) {
		mp_GrobalData->Activate( arg_Target, m_hModel, arg_PlayerNo ) ;	// --- グローバルデータの射撃関数を呼び出し
		m_Animation.SetReverse() ;
		m_Closing = TRUE ;
	}
	// --- 翼は別処理
	else {
		g_Sound.Play( SOUND_WINGSE_NO ) ;

		m_Activating		= TRUE ;				// --- 発動中に
		m_ActivatingTime	= DATA_WING_ACTTIME ;	// --- 発動時間
	}

	m_ActivateFlg = FALSE ;
	m_NowBullet-- ;

	return 0 ;
}

//==============================================================//
//		描画													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int BackUnitMember::DrawEq()
{
	MATRIX DrawFrameMtx ;	// --- 描画するワールドマトリックス

	DrawFrameMtx = MV1GetFrameLocalWorldMatrix( m_JoinModel, m_JoinFrameIdx ) ;					// --- 接続しているフレームのワールドマトリックスを取得
	MV1SetMatrix( m_hModel, DrawFrameMtx ) ;													// --- モデルにワールドマトリックスの値を割り当て
	MV1SetAttachAnimTime( m_hModel, m_Animation.m_AttachIdx, m_Animation.m_AnimNowTime ) ;		// --- 現在のアニメーション状態をセット

	MV1DrawModel( m_hModel ) ;	// --- 描画

	return 0 ;
}

//==============================================================//
//		展開													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : アニメーション終了判定							//
//==============================================================//
int BackUnitMember::Open()
{
	m_Animation.AnimProg() ;		// --- アニメーション進行

	return m_Animation.m_EndFlg ;	// --- 終了フラグを返す
}

//==============================================================//
//		アニメーション反転										//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int BackUnitMember::AnimRev()
{
	m_Animation.SetReverse() ;

	return 0 ;
}

//==============================================================//
//		状態のリセット											//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int BackUnitMember::StateReset()
{
	m_Animation.AnimStart() ;
	m_Animation.m_AnimNowTime = 0.0f ;

	m_Activating = FALSE ;
	m_Closing = FALSE ;

	return 0 ;
}

//==============================================================//
//		再使用可能にす											//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int BackUnitMember::ResetActiv()
{
	StateReset() ;

	m_ActivateFlg	= TRUE ;
	m_NowBullet		= DATA_BACKUNIT_MAXBUL ;

	return 0 ;
}
