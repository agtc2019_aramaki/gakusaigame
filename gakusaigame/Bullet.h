//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							Bullet.h															//
//																								//
//							作成日時 : 2020.10/25												//
//							作成者   : 天童律希													//
//																								//
//							弾丸オブジェクト													//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		Define定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
// --- ヒットチェックデータ
#define BULLET_HITCHECK_RADIUS				7.0f			// --- 球の半径
#define BULLET_DISAPLINE					500.0f			// --- 原点からここまで離れたら消える

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		enum定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
// --- 弾丸タイプ
enum BulletType {
	eBulSyuriken,	// --- 手裏剣
	eBulBee,		// --- 蜂
	eBulFish,		// --- 魚
	eBulEagle,		// --- 鳥
	eBulMissile,	// --- ミサイル
	eBulCannon,		// --- 大砲
} ;

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		クラス定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
// --- 弾丸グローバルデータ
class BulletData {
	public :
		int			InitializeData		( int arg_Type, int arg_gModelNo, int arg_gAnimNo,
											float arg_HomPow, int arg_HomTime, float arg_Spd, float arg_Power ) ;		// --- データセット
		// --- 参照が多いので公開
		int			m_Type ;				// --- 種類
		int			m_gModelNo ;			// --- グローバルモデル配列番号
		int			m_gAnimNo ;				// --- グローバルアニメーション配列番号
		float		m_HomingPower ;			// --- ホーミング力
		int			m_HomingTime ;			// --- ホーミング持続時間
		float		m_SpdAbs ;				// --- 速度
		float		m_DamegePower ;			// --- 威力
} ;

// --- 実際の弾丸
class BulletObj : public Character {
	public :
		BulletObj					() ;															// --- コンストラクタ
		int			Action			() override ;													// --- アクション
		int			InitBul			( BulletData* argp_gData, VECTOR* argp_Target, int arg_PlNo ) ;	// --- 弾丸の初期セット
		int			Homing			() ;													 		// --- ホーミング→速度確定
		int			SetFirstSpeed	( float arg_AngleOffset_Y ) ;									// --- 初速セット
		static int	Search			() ;															// --- 空きオブジェクトサーチ
		int			LookMoveDir		() ;															// --- 進行方向を向く
		float		GetDamege		() ;															// --- 威力の取得
		int			SetHoming		( int arg_HomingFlg ) ;											// --- ホーミングフラグセット

		// --- 参照回数が多いので公開
		HitCheckInfo	m_HitCaps ;			// --- ヒットチェック情報
		int				m_PlayerNo ;		// --- 発射したプレイヤー

	private :
		VECTOR*			mp_Target ;				// --- ターゲット
		int				m_Type ;				// --- 種類
		int				m_HomingFlg ;			// --- ホーミングフラグ
		float			m_HomingPower ;			// --- ホーミング力
		int				m_HomingTime ;			// --- ホーミング持続時間
		float			m_SpdAbs ;				// --- 速度
		float			m_DamegePower ;			// --- 威力

} ;


