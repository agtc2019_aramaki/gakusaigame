///////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							GunData.h															//
//																								//
//							作成日時 : 2020.10/7												//
//							作成者   : 天童律希													//
//																								//
//							銃データクラスメソッド定義											//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include "Common.h"

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//	グローバルデータ
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//

GunShotTbl ShotTbl[4] ;

//==============================================================//
//		データ初期セット										//
//--------------------------------------------------------------//
//		IN  : 保存データ										//
//		OUT : 終了判定											//
//==============================================================//
int GunData::InitializeData( int arg_Type )
{
	m_Type		= arg_Type ;			// --- 種類

	return 0 ;
}

//==============================================================//
//		発動													//
//--------------------------------------------------------------//
//		IN  : ターゲット,装備しているフレーム					//
//		OUT : 終了判定											//
//==============================================================//
int GunData::Activate( VECTOR* argp_Target, int arg_ActModel, int arg_FrameIdx, int arg_PlayerNo )
{
	VECTOR	SetPos ;

	SetPos = MGetTranslateElem( MV1GetFrameLocalWorldMatrix(arg_ActModel, arg_FrameIdx) ) ;		// --- 発射するフレームのワールド行列から座標のみを取得
	ShotTbl[m_Type]( SetPos, argp_Target, arg_PlayerNo ) ;

	g_Sound.Play( SOUND_SHOTSE_NO ) ;

	return 0 ;
}

//==============================================================//
//		射撃関数ポインタを保存する								//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int GunData::SetActivateFunction()
{
	ShotTbl[eGunSyuriken]	= Shot_S ;
	ShotTbl[eGunBeard]		= Shot_E ;
	ShotTbl[eGunBee]		= Shot_B ;
	ShotTbl[eGunFish]		= Shot_F ;

	return 0 ;
}

//==============================================================//
//																//
//		射撃関数_手裏剣											//
//																//
//==============================================================//
int Shot_S( VECTOR arg_ShotPos, VECTOR* argp_Tgt, int argPlNo )
{
	int		BulNum ;		// --- 弾丸配列番号
	float	f = 20.0 ;		// --- 発射角

	for ( int i = 0 ; i < 5 ; i++ ) {
		BulNum = BulletObj::Search() ;
		if ( BulNum != -1 ) {
			g_Bullet[BulNum].SetPosition( arg_ShotPos ) ;												// --- 弾丸の座標をセット
			g_Bullet[BulNum].InitBul( &g_BulletData[eBulSyuriken], argp_Tgt, argPlNo ) ;				// --- 弾丸データセット
			g_Bullet[BulNum].SetFirstSpeed( f ) ;														// --- 初速セット
		}
		f -= 10.0f ;
	}

	return 0 ;
}

//==============================================================//
//																//
//		射撃関数_蜂												//
//																//
//==============================================================//
int Shot_B( VECTOR arg_ShotPos, VECTOR* argp_Tgt, int argPlNo )
{
	int		BulNum ;		// --- 弾丸配列番号
	float	f = 60.0 ;		// --- 発射角

	for ( int i = 0 ; i < 3 ; i++ ) {
		BulNum = BulletObj::Search() ;
		if ( BulNum != -1 ) {
			g_Bullet[BulNum].SetPosition( arg_ShotPos ) ;												// --- 弾丸の座標をセット
			g_Bullet[BulNum].InitBul( &g_BulletData[eBulBee], argp_Tgt, argPlNo ) ;						// --- 弾丸データセット
			g_Bullet[BulNum].SetFirstSpeed( f ) ;														// --- 初速セット
		}
		f -= 60.0f ;
	}

	return 0 ;
}

//==============================================================//
//																//
//		射撃関数_魚												//
//																//
//==============================================================//
int Shot_F( VECTOR arg_ShotPos, VECTOR* argp_Tgt, int argPlNo )
{
	int		BulNum ;		// --- 弾丸配列番号
	float	f = 60.0 ;		// --- 発射角

	for ( int i = 0 ; i < 2 ; i++ ) {
		BulNum = BulletObj::Search() ;
		if ( BulNum != -1 ) {
			g_Bullet[BulNum].SetPosition( arg_ShotPos ) ;												// --- 弾丸の座標をセット
			g_Bullet[BulNum].InitBul( &g_BulletData[eBulFish], argp_Tgt, argPlNo ) ;					// --- 弾丸データセット
			g_Bullet[BulNum].SetFirstSpeed( f ) ;														// --- 初速セット
		}
		f -= 120.0f ;
	}

	return 0 ;
}

//==============================================================//
//																//
//		射撃関数_鳥												//
//																//
//==============================================================//
int Shot_E( VECTOR arg_ShotPos, VECTOR* argp_Tgt, int argPlNo )
{
	int		BulNum ;		// --- 弾丸配列番号
	float	f = 30.0 ;		// --- 発射角

	for ( int i = 0 ; i < 3 ; i++ ) {
		BulNum = BulletObj::Search() ;
		if ( BulNum != -1 ) {
			g_Bullet[BulNum].SetPosition( arg_ShotPos ) ;												// --- 弾丸の座標をセット
			g_Bullet[BulNum].InitBul( &g_BulletData[eBulEagle], argp_Tgt, argPlNo ) ;					// --- 弾丸データセット
			g_Bullet[BulNum].SetFirstSpeed( f ) ;														// --- 初速セット
		}
		f -= 30.0f ;
	}

	return 0 ;
}

//==============================================================//
//		指定データを取得										//
//--------------------------------------------------------------//
//		IN  : 必要データの番号									//
//		OUT : データ											//
//==============================================================//
int GunData::GetData( int arg_eData )
{
	switch ( arg_eData )
	{
		case eGunType :		return m_Type ;			// --- 種類
		case eGunModel :	return m_hModelEq ;		// --- モデルハンドル
	}

	return -1 ;
}

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//	メンバーデータ
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//==============================================================//
//		参照するグローバルデータセット							//
//--------------------------------------------------------------//
//		IN  : 参照するグローバルデータ							//
//		OUT : 終了判定											//
//==============================================================//
int GunMember::SetGrobalDeta( int arg_GunNo )
{
	mp_GrobalData	= &g_GunData[arg_GunNo] ;									// --- ポインタセット
	m_hModel		= MV1DuplicateModel( mp_GrobalData->GetData(eGunModel) ) ;	// --- モデル複製
	m_Type			= mp_GrobalData->GetData(eGunType) ;						// --- 種類セット

	m_NowBullet = GUN_DATA_MAXBUL ;		// --- 弾数を設定
	m_ReloadSpd = GUN_DATA_RELSPD ;		// --- リロード速度をセット

	m_ActivateFlg = TRUE ;

	return 0 ;
}

//==============================================================//
//		発射													//
//--------------------------------------------------------------//
//		IN  : ターゲット										//
//		OUT : 終了判定											//
//==============================================================//
int GunMember::Shot( VECTOR* arg_Target, int arg_PlayerNo )
{
	if ( m_ActivateFlg ) {
		mp_GrobalData->Activate( arg_Target, m_JoinModel, m_JoinFrameIdx, arg_PlayerNo ) ;	// --- グローバルデータの射撃関数を呼び出し

		m_NowBullet-- ;					// --- 残弾を減らす
		if ( m_NowBullet <= 0 ) {		// --- 残弾が0以下になったら
			m_ActivateFlg = FALSE ;		// --- 使用不可
			m_NowReloadTime = 0 ;		// --- リロード進行セット
		}
	}

	return 0 ;
}

//==============================================================//
//		残弾リセット											//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int GunMember::ResetBul()
{
	m_ActivateFlg	= TRUE ;				// --- 発射可能
	m_NowBullet		= GUN_DATA_MAXBUL ;		// --- 残弾セット

	return 0 ;
}
