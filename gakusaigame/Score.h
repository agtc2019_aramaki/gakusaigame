///////////////////////////////////////////////////////////////////////////////////////////
//																						//
//							Score.h														//
//																						//
//							作成日時 : 2020.11/08										//
//							作成者   : 荒巻伸悟											//
//																						//
//							撃破オブジェクトのクラス									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

class Score : public GameObject_2D{
	public:
		int Action() override ;		// --- アクション...使用するオブジェクトのクラスでオーバーライド
		int Draw() override ;		// --- 描画
		int Initialize( int arg_ScoreNo ) ;			// --- 背景画像の初期セット
		int SetActionNo( int arg_ActNo ) ;			// --- アクション番号のセット
		int	LoadCutGraph( int arg_gGraphNo , RECT* arg_CutRect , int arg_mGraphNo = NULL ) ;// --- グラフィックのロード
		int Getter() ;

		RECT ScDrawRect[2] ;
		POINT ScCut_Pt[2] ;

		int m_ScGraph[2] ;
		int StoringSc[2] ;
		int m_ScoreNumber ; 
		int m_PlayerNo ;
} ;



