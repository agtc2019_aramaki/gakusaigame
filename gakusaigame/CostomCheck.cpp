//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							CustomCheck.cpp														//
//																								//
//							作成日時 : 2020.11/08												//
//							作成者   : 天童律希													//
//																								//
//							カスタマイズシーンに使用するアレコレ								//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include "Common.h"

//==================================================================//
//						データのロード								//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int CustomCheck::LoadData()
{
	for ( int i = 0 ; i < 8 ; i++ ) {
		m_DrawData[i].m_DrawFlg = FALSE ;
	}

	// --- "SelectYourWeapons" -------------------------------------------------- //
	m_DrawData[eCsText_Select].m_hGraph	= DerivationGraph(
		0, 0,
		SIZE_SELECTTEXT_W,
		SIZE_SELECTTEXT_H,
		g_hGraph[IMAGE_CUSTTEXT_NO] ) ;

	m_DrawData[eCsText_Select].m_DrawPt.x = WINDOW_SIZE_W / 2 ;
	m_DrawData[eCsText_Select].m_DrawPt.y = 20 ;
	m_DrawData[eCsText_Select].m_DrawOffset.x = SIZE_SELECTTEXT_W / 2 ;
	m_DrawData[eCsText_Select].m_DrawOffset.y = 0 ;

	// --- "Standby"1P -------------------------------------------------- //
	m_DrawData[eCsText_Standby_1P].m_hGraph	= DerivationGraph(
		0, SIZE_RUREADY_H + SIZE_YESNO_H,
		SIZE_STANDBYTEXT_W,
		SIZE_STANDBYTEXT_H,
		g_hGraph[IMAGE_CUSTOMIZE_NO] ) ;

	m_DrawData[eCsText_Standby_1P].m_DrawPt.x = WINDOW_SIZE_W / 4 ;
	m_DrawData[eCsText_Standby_1P].m_DrawPt.y = 250 ;
	m_DrawData[eCsText_Standby_1P].m_DrawOffset.x = SIZE_STANDBYTEXT_W / 2 ;
	m_DrawData[eCsText_Standby_1P].m_DrawOffset.y = 0 ;

	// --- "Standby"2P -------------------------------------------------- //
	m_DrawData[eCsText_Standby_2P].m_hGraph	= DerivationGraph(
		0, SIZE_RUREADY_H + SIZE_YESNO_H,
		SIZE_STANDBYTEXT_W,
		SIZE_STANDBYTEXT_H,
		g_hGraph[IMAGE_CUSTOMIZE_NO] ) ;

	m_DrawData[eCsText_Standby_2P].m_DrawPt.x = (WINDOW_SIZE_W / 4) * 3 ;
	m_DrawData[eCsText_Standby_2P].m_DrawPt.y = 250 ;
	m_DrawData[eCsText_Standby_2P].m_DrawOffset.x = SIZE_STANDBYTEXT_W / 2 ;
	m_DrawData[eCsText_Standby_2P].m_DrawOffset.y = 0 ;

	// --- "AreYouReady?" -------------------------------------------------- //
	m_DrawData[eCsText_RUReady].m_hGraph	= DerivationGraph(
		0, 0,
		SIZE_RUREADY_W,
		SIZE_RUREADY_H,
		g_hGraph[IMAGE_CUSTOMIZE_NO] ) ;

	m_DrawData[eCsText_RUReady].m_DrawPt.x = WINDOW_SIZE_W / 2 ;
	m_DrawData[eCsText_RUReady].m_DrawPt.y = 60 ;
	m_DrawData[eCsText_RUReady].m_DrawOffset.x = SIZE_RUREADY_W / 2 ;
	m_DrawData[eCsText_RUReady].m_DrawOffset.y = 0 ;

	// --- "YES" -------------------------------------------------- //
	m_DrawData[eCsYes].m_hGraph	= DerivationGraph(
		0, SIZE_RUREADY_H,
		SIZE_YESNO_W,
		SIZE_YESNO_H,
		g_hGraph[IMAGE_CUSTOMIZE_NO] ) ;

	m_DrawData[eCsYes].m_DrawPt.x = WINDOW_SIZE_W / 4 ;
	m_DrawData[eCsYes].m_DrawPt.y = 250 ;
	m_DrawData[eCsYes].m_DrawOffset.x = SIZE_YESNO_W / 2 ;
	m_DrawData[eCsYes].m_DrawOffset.y = 0 ;

	// --- "NO" -------------------------------------------------- //
	m_DrawData[eCsNo].m_hGraph	= DerivationGraph(
		SIZE_YESNO_W, SIZE_RUREADY_H,
		SIZE_YESNO_W,
		SIZE_YESNO_H,
		g_hGraph[IMAGE_CUSTOMIZE_NO] ) ;

	m_DrawData[eCsNo].m_DrawPt.x = (WINDOW_SIZE_W / 4) * 3 ;
	m_DrawData[eCsNo].m_DrawPt.y = 250 ;
	m_DrawData[eCsNo].m_DrawOffset.x = SIZE_YESNO_W / 2 ;
	m_DrawData[eCsNo].m_DrawOffset.y = 0 ;

	// --- 1P選択カーソル -------------------------------------------------- //
	m_DrawData[eCsCursor_1P].m_hGraph	= DerivationGraph(
		SIZE_YESNO_W + SIZE_YESNO_W, SIZE_RUREADY_H,
		SIZE_CURSOR_W,
		SIZE_CURSOR_H,
		g_hGraph[IMAGE_CUSTOMIZE_NO] ) ;

	m_DrawData[eCsCursor_1P].m_DrawPt.x = (WINDOW_SIZE_W / 4) * 3 ;
	m_DrawData[eCsCursor_1P].m_DrawPt.y = 250 - SIZE_CURSOR_H ;
	m_DrawData[eCsCursor_1P].m_DrawOffset.x = SIZE_CURSOR_W / 2 ;
	m_DrawData[eCsCursor_1P].m_DrawOffset.y = SIZE_CURSOR_H / 2 ;

	// --- 2P選択カーソル -------------------------------------------------- //
	m_DrawData[eCsCursor_2P].m_hGraph	= DerivationGraph(
		SIZE_YESNO_W + SIZE_YESNO_W + SIZE_CURSOR_W, SIZE_RUREADY_H,
		SIZE_CURSOR_W,
		SIZE_CURSOR_H,
		g_hGraph[IMAGE_CUSTOMIZE_NO] ) ;

	m_DrawData[eCsCursor_2P].m_DrawPt.x = (WINDOW_SIZE_W / 4) * 3 ;
	m_DrawData[eCsCursor_2P].m_DrawPt.y = 250 + SIZE_YESNO_H + SIZE_CURSOR_H ;
	m_DrawData[eCsCursor_2P].m_DrawOffset.x = SIZE_CURSOR_W / 2 ;
	m_DrawData[eCsCursor_2P].m_DrawOffset.y = SIZE_CURSOR_H / 2 ;

	return 0 ;
}

//==================================================================//
//						アクション									//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int CustomCheck::Action()
{
	switch ( m_ActionNo )
	{		
		// --- 選択中
		case eCsSelect :
			// --- 装備確定フラグを描画フラグにセット
			m_DrawData[eCsText_Standby_1P].m_DrawFlg = FALSE ;
			m_DrawData[eCsText_Standby_2P].m_DrawFlg = FALSE ;
			if ( g_Player[ePlayer_1].GetFlg(ePlConfEq) == 0 ) {
				m_DrawData[eCsText_Standby_1P].m_DrawFlg = TRUE ;
			}
			if ( g_Player[ePlayer_2].GetFlg(ePlConfEq) == 0 ) {
				m_DrawData[eCsText_Standby_2P].m_DrawFlg = TRUE ;
			}

			if ( m_DrawData[eCsText_Standby_1P].m_DrawFlg & m_DrawData[eCsText_Standby_2P].m_DrawFlg ) {
				m_ActionNo = eCsConfirmationInit ;
			}
			break ;

		// --- 最終確認
		case eCsConfirmationInit :
			m_DrawData[eCsText_Select].m_DrawFlg		= FALSE ;
			m_DrawData[eCsText_Standby_1P].m_DrawFlg	= FALSE ;
			m_DrawData[eCsText_Standby_2P].m_DrawFlg	= FALSE ;

			m_DrawData[eCsText_RUReady].m_DrawFlg		= TRUE ;
			m_DrawData[eCsYes].m_DrawFlg				= TRUE ;
			m_DrawData[eCsNo].m_DrawFlg					= TRUE ;
			m_DrawData[eCsCursor_1P].m_DrawFlg			= TRUE ;
			m_DrawData[eCsCursor_2P].m_DrawFlg			= TRUE ;

			// --- 矢印の描画ループ
			for( int i = 0 ; i < OBJ_ARROWGRAPH_MAX ; i++ )
			{
				g_Arrow[i].SetUseFlg( FALSE ) ;
			}

			// --- プレイヤーのキー受付をロック
			g_Player[0].KeyLock( TRUE ) ;
			g_Player[1].KeyLock( TRUE ) ;

			m_CursorPos[0] = FALSE ;		// --- 確定フラグ
			m_CursorPos[1] = FALSE ;		// --- 確定フラグ

			KeyCheck() ;

			m_ActionNo = eCsConfirmation ;
			break ;

		// --- 最終確認
		case eCsConfirmation :
			KeyCheck() ;
			for ( int i = 0 ; i < 2 ; i++ ) {
				// --- 〇ボタントリガー
				if ( (m_Key[i][ePlKeyTrg] & PAD_INPUT_B) != 0 ) {
					g_Sound.Play( SOUND_CURSORCK_NO ) ;
					if ( m_CursorPos[i] ) {
						m_Confirmation[i] ^= TRUE ;
					}
					else {
						BackToSelect() ;
					}
				}
				if ( m_Confirmation[i] != TRUE ) {
					// --- 左右キーチェック
					if ( ((m_Key[i][ePlKeyTrg] & PAD_INPUT_RIGHT) != 0) || 
						((m_Key[i][ePlKeyTrg] & PAD_INPUT_LEFT) != 0) ) {
						g_Sound.Play( SOUND_CURSORMOVE_NO ) ;
						m_CursorPos[i] ^= TRUE ;
					}
					// --- カーソル位置決定
					m_DrawData[eCsCursor_1P+i].m_DrawPt.x = m_DrawData[eCsNo].m_DrawPt.x ;
					if ( m_CursorPos[i] ) {
						m_DrawData[eCsCursor_1P+i].m_DrawPt.x = m_DrawData[eCsYes].m_DrawPt.x ;
					}
				}
			}
			if ( m_Confirmation[0] & m_Confirmation[1] ) {
				m_ActionNo = eCsSceneAdd ;
			}
			break ;

		// --- シーン遷移
		case eCsSceneAdd :
			m_SceneFlg = TRUE ;
			break ;
	}


	return 0 ;
}

//==============================================================//
//		描画													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int CustomCheck::Draw()
{
	for ( int i = 0 ; i < 8 ; i ++ ) {
		if ( m_DrawData[i].m_DrawFlg ) {
			DrawGraph(
				m_DrawData[i].m_DrawPt.x - m_DrawData[i].m_DrawOffset.x,
				m_DrawData[i].m_DrawPt.y - m_DrawData[i].m_DrawOffset.y,
				m_DrawData[i].m_hGraph,
				TRUE ) ;
		}
	}

	return 0 ;
}

//==============================================================//
//		初期セット												//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int CustomCheck::Initialize()
{
	for ( int i = 1 ; i < 8 ; i++ ) {
		m_DrawData[i].m_DrawFlg = FALSE ;
	}

	m_DrawData[eCsText_Select].m_DrawFlg = TRUE ;

	m_Confirmation[0] = FALSE ;		// --- 確定フラグ
	m_Confirmation[1] = FALSE ;		// --- 確定フラグ

	m_SceneFlg = FALSE ;

	m_ActionNo = eCsSelect ;

	m_UseFlg = TRUE ;

	return 0 ;
}

//==============================================================//
//		選択に戻る												//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int CustomCheck::BackToSelect()
{
	m_SceneFlg = -1 ;

	return 0 ;
}

//==================================================================//
//						キー情報入力								//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int CustomCheck::KeyCheck()
{
	for ( int i = 0 ; i < 2 ; i++ ) {
		m_Key[i][ePlKeyTrg] &= 0x00000000 ;
		m_Key[i][ePlKeyOld] = m_Key[i][ePlKeyData] ;
		m_Key[i][ePlKeyData] = g_Player[i].KeyInput() ;

		// --- 右キートリガー
		if ( (m_Key[i][ePlKeyOld] & PAD_INPUT_RIGHT) == 0
				&& ((m_Key[i][ePlKeyData] & PAD_INPUT_RIGHT)) != 0 ) {
					m_Key[i][ePlKeyTrg] |= PAD_INPUT_RIGHT ;
		}

		// --- 左キートリガー
		if ( (m_Key[i][ePlKeyOld] & PAD_INPUT_LEFT) == 0
				&& ((m_Key[i][ePlKeyData] & PAD_INPUT_LEFT)) != 0 ) {
					m_Key[i][ePlKeyTrg] |= PAD_INPUT_LEFT ;
		}

		// --- ○キートリガー
		if ( (m_Key[i][ePlKeyOld] & PAD_INPUT_B) == 0
				&& ((m_Key[i][ePlKeyData] & PAD_INPUT_B)) != 0 ) {
					m_Key[i][ePlKeyTrg] |= PAD_INPUT_B ;
		}
	}

	return 0 ;
}
