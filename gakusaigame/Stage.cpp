//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							Stage.h																//
//																								//
//							作成日時 : 2020.10/15												//
//							作成者   : 天童律希													//
//																								//
//							ステージ															//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include "Common.h"

//==============================================================//
//		初期セット												//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Stage::Initialize()
{
	// --- モデルハンドルの複製
	LoadModel( MODEL_STAGE_NO ) ;				// --- モデル

	// --- ヒットチェック用データ初期セット --------------------------------------- //
	m_HitStg[0].m_Param[0].y = 0.0f ;
	m_HitStg[0].m_Param[1].y = 0.0f ;
	m_HitStg[0].m_Param[2].y = 0.0f ;
	m_HitStg[1].m_Param[0].y = 0.0f ;
	m_HitStg[1].m_Param[1].y = 0.0f ;
	m_HitStg[1].m_Param[2].y = 0.0f ;

	m_HitWall[eStgTop]		[0].m_Param[0].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgTop]		[0].m_Param[1].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgTop]		[0].m_Param[2].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgTop]		[1].m_Param[0].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgTop]		[1].m_Param[1].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgTop]		[1].m_Param[2].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgBottom]	[0].m_Param[0].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgBottom]	[0].m_Param[1].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgBottom]	[0].m_Param[2].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgBottom]	[1].m_Param[0].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgBottom]	[1].m_Param[1].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgBottom]	[1].m_Param[2].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgLeft]		[0].m_Param[0].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgLeft]		[0].m_Param[1].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgLeft]		[0].m_Param[2].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgLeft]		[1].m_Param[0].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgLeft]		[1].m_Param[1].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgLeft]		[1].m_Param[2].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgRight]	[0].m_Param[0].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgRight]	[0].m_Param[1].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgRight]	[0].m_Param[2].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgRight]	[1].m_Param[0].y = SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgRight]	[1].m_Param[1].y = -SATAGE_HITCHECK_WIDTH ;
	m_HitWall[eStgRight]	[1].m_Param[2].y = SATAGE_HITCHECK_WIDTH ;

	return 0 ;
}

//==============================================================//
//		戦闘初期セット											//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Stage::InitializeBattle()
{
	m_ActionNo = eStNon ;					// --- 拡縮なし

	m_NowSize = STAGE_PARAM_EXTENDNORMAL ;	// --- 基準サイズ
	m_Size = VGet(1.0f,1.0f,1.0f) ;

	ChangeSize( SATAGE_HITCHECK_WIDTH ) ;

	m_UseFlg = TRUE ;						// --- 使用

	return 0 ;
}

//==================================================================//
//		アクション													//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int Stage::Action()
{
	float fScale ;

	if ( m_ActionNo == eStExtend ) {
		m_NowSize += m_ExtendSpd ;
		fScale = m_NowSize / STAGE_PARAM_EXTENDMAGNI ;
		ChangeSize( fScale * SATAGE_HITCHECK_WIDTH ) ;
		m_Size = VGet( fScale, fScale, fScale ) ;
		if ( m_NowSize == m_NextSize ) {
			m_ActionNo = eStNon ;
		}
	}

	return 0 ;
}

//==================================================================//
//		ヒットチェック用データ更新									//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int Stage::ChangeSize( float arg_Size )
{
	// --- 床 ------------------------------------------------------------------- //
	m_HitStg[0].m_Param[0].x = arg_Size ;
	m_HitStg[0].m_Param[0].z = arg_Size ;

	m_HitStg[0].m_Param[1].x = arg_Size ;
	m_HitStg[0].m_Param[1].z = arg_Size * -1.0f ;

	m_HitStg[0].m_Param[2].x = arg_Size * -1.0f ;
	m_HitStg[0].m_Param[2].z = arg_Size ;

	m_HitStg[1].m_Param[0].x = arg_Size * -1.0f ;
	m_HitStg[1].m_Param[0].z = arg_Size * -1.0f ;

	m_HitStg[1].m_Param[1].x = arg_Size ;
	m_HitStg[1].m_Param[1].z = arg_Size * -1.0f ;

	m_HitStg[1].m_Param[2].x = arg_Size * -1.0f ;
	m_HitStg[1].m_Param[2].z = arg_Size ;

	// --- 奥
	m_HitWall[eStgTop][0].m_Param[0].x = arg_Size * -1.0f ;
	m_HitWall[eStgTop][0].m_Param[0].z = arg_Size ;

	m_HitWall[eStgTop][0].m_Param[1].x = arg_Size ;
	m_HitWall[eStgTop][0].m_Param[1].z = arg_Size ;

	m_HitWall[eStgTop][0].m_Param[2].x = arg_Size * -1.0f ;
	m_HitWall[eStgTop][0].m_Param[2].z = arg_Size ;

	m_HitWall[eStgTop][1].m_Param[0].x = arg_Size ;
	m_HitWall[eStgTop][1].m_Param[0].z = arg_Size ;

	m_HitWall[eStgTop][1].m_Param[1].x = arg_Size ;
	m_HitWall[eStgTop][1].m_Param[1].z = arg_Size ;

	m_HitWall[eStgTop][1].m_Param[2].x = arg_Size * -1.0f ;
	m_HitWall[eStgTop][1].m_Param[2].z = arg_Size ;

	// --- 手前
	m_HitWall[eStgBottom][0].m_Param[0].x = arg_Size * -1.0f ;
	m_HitWall[eStgBottom][0].m_Param[0].z = arg_Size * -1.0f ;

	m_HitWall[eStgBottom][0].m_Param[1].x = arg_Size ;
	m_HitWall[eStgBottom][0].m_Param[1].z = arg_Size * -1.0f ;

	m_HitWall[eStgBottom][0].m_Param[2].x = arg_Size * -1.0f ;
	m_HitWall[eStgBottom][0].m_Param[2].z = arg_Size * -1.0f ;

	m_HitWall[eStgBottom][1].m_Param[0].x = arg_Size ;
	m_HitWall[eStgBottom][1].m_Param[0].z = arg_Size * -1.0f ;

	m_HitWall[eStgBottom][1].m_Param[1].x = arg_Size ;
	m_HitWall[eStgBottom][1].m_Param[1].z = arg_Size * -1.0f ;

	m_HitWall[eStgBottom][1].m_Param[2].x = arg_Size * -1.0f ;
	m_HitWall[eStgBottom][1].m_Param[2].z = arg_Size * -1.0f ;

	// --- 左
	m_HitWall[eStgLeft][0].m_Param[0].x = arg_Size * -1.0f ;
	m_HitWall[eStgLeft][0].m_Param[0].z = arg_Size * -1.0f ;

	m_HitWall[eStgLeft][0].m_Param[1].x = arg_Size * -1.0f ;
	m_HitWall[eStgLeft][0].m_Param[1].z = arg_Size ;

	m_HitWall[eStgLeft][0].m_Param[2].x = arg_Size * -1.0f ;
	m_HitWall[eStgLeft][0].m_Param[2].z = arg_Size * -1.0f ;

	m_HitWall[eStgLeft][1].m_Param[0].x = arg_Size * -1.0f ;
	m_HitWall[eStgLeft][1].m_Param[0].z = arg_Size ;

	m_HitWall[eStgLeft][1].m_Param[1].x = arg_Size * -1.0f ;
	m_HitWall[eStgLeft][1].m_Param[1].z = arg_Size ;

	m_HitWall[eStgLeft][1].m_Param[2].z = arg_Size * -1.0f ;
	m_HitWall[eStgLeft][1].m_Param[2].x = arg_Size * -1.0f ;

	// --- 右
	m_HitWall[eStgRight][0].m_Param[0].x = arg_Size ;
	m_HitWall[eStgRight][0].m_Param[0].z = arg_Size * -1.0f ;

	m_HitWall[eStgRight][0].m_Param[1].x = arg_Size ;
	m_HitWall[eStgRight][0].m_Param[1].z = arg_Size ;

	m_HitWall[eStgRight][0].m_Param[2].x = arg_Size ;
	m_HitWall[eStgRight][0].m_Param[2].z = arg_Size * -1.0f ;

	m_HitWall[eStgRight][1].m_Param[0].x = arg_Size ;
	m_HitWall[eStgRight][1].m_Param[0].z = arg_Size ;

	m_HitWall[eStgRight][1].m_Param[1].x = arg_Size ;
	m_HitWall[eStgRight][1].m_Param[1].z = arg_Size ;

	m_HitWall[eStgRight][1].m_Param[2].x = arg_Size ;
	m_HitWall[eStgRight][1].m_Param[2].z = arg_Size * -1.0f ;

	return 0 ;
}

//==================================================================//
//		サイズ変更状態へ											//
//------------------------------------------------------------------//
//		IN  :	変化後のサイズ										//
//		OUT :	終了判定											//
//==================================================================//
int Stage::SetNextSize( int arg_Size )
{
	m_NextSize = arg_Size ;												// --- 目標サイズを設定

	if ( m_NextSize > m_NowSize ) {
		m_ExtendSpd = STAGE_PARAM_EXTENDSPD ;							// --- 拡縮速度を設定
	}
	else if ( m_NextSize < m_NowSize ){
		m_ExtendSpd = STAGE_PARAM_EXTENDSPD * -1 ;						// --- 拡縮速度を設定
	}
	else {
		m_ExtendSpd = 0 ;												// --- 拡縮速度を設定
	}

	m_ActionNo = eStExtend ;

	return 0 ;
}
