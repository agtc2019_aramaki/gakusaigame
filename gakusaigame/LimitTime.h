///////////////////////////////////////////////////////////////////////////////////////////
//																						 //
//								LimitTime.h						 				 		 //
//																	 					 //
//							作成日時 : 2020.10/22					 					 //
//							作成者   : 荒巻伸悟						 					 //
//																	 					 //
//							制限時間処理のクラス				 						 //
//																						 //
///////////////////////////////////////////////////////////////////////////////////////////


#define NUMBER_WIDTH 40
#define NUMBER_HEIGHT 50


enum LimitTimeNo{
	Number_00,
	Number_01,
	Number_02,
	Number_03,
	Number_04,
	Number_05,
	Number_06,
	Number_07,
	Number_08,
	Number_09
} ;

class LimitTime : public GameObject_2D
{
	public :
		int Action() override ;				// --- アクション...実際に使用するオブジェクトのクラスでオーバーライド
		int Draw() override ;				// --- 描画
		int LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo = NULL ) ;	// --- グラフィックのロード
		int Initialize( int arg_LimitNo ) ;	// --- HP関連画像の初期セット
		int StartBattle() ;

		int StoringTime[2] ;	// --- 画像位置を分ける
		int timediff ;			// --- リミットから引いた時間を格納
		LONGLONG timelimit ;	// --- 初期時間を格納する
		LONGLONG starttime ;	// --- バトル開始時間を格納
		int m_time_s ;			// --- timediff(1,000,000)で1秒
		bool ChangeTime ;		// --- 時間変えるフラグ
		bool AbsFlg ;
} ;



