//////////////////////////////////////////////////////////////////////////////////////////////
//																							//
//							Player.cpp														//
//																							//
//							作成日時 : 2020.10/02											//
//							作成者   : 天童律希												//
//																							//
//							プレイヤークラスのメソッド定義									//
//																							//
//////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h> 
#include <math.h>
#include "Common.h"

//==================================================================//
//						アクション									//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int Player::Action()
{
	KeyInput() ;

	switch ( m_ActionNo )
	{
		// --- デバッグ用
		case ePlDebug :
//			m_ActionNo = ePlStay ;
			break ;

		// --- 初期セット
		case ePlInit :
			break ;

		// --- 静止
		case ePlStay :
			m_Spd = VGet(0.0f,0.0f,0.0f) ;				// --- 移動を0に
			Move() ;
			// --- 十字キーのチェック
			m_MoveDir = MoveState() ;
			if ( m_MoveDir != ePlNoMove ) {				// --- 十字キーが押されている時
				m_SpdAbs = PLAYER_PARAM_MOVESPD_MIN ;	// --- 移動初速セット
				ChangeAnim( ePlAnimMv, FALSE ) ;		// --- 移動アニメーションをアタッチ
				m_ActionNo = ePlMove ;					// --- 移動へ
			}
			// --- トリガーチェック
			switch ( TriggerState() )
			{
				// --- △ボタン
				case ePlTrgXButton :
					InitBackActiv() ;
					break ;

				// --- 〇ボタン
				case ePlTrgAButton :
					// --- ダッシュ可能フラグがオンの時
					if ( (m_Flg & PLAYER_FLG_DASH) != 0 ) {
						InitDash() ;
					}
					break ;

				// --- ×ボタン
				case ePlTrgBButton :
					InitJump() ;
					break ;

				// --- R1ボタン
				case ePlTrgR1Button :
					InitShot( ePlGunR ) ;
					break ;

				// --- L1ボタン
				case ePlTrgL1Button :
					InitShot( ePlGunL ) ;
					break ;
			}
			break ;

		// --- 移動
		case ePlMove :
			// --- 十字キーのチェック
			m_MoveDir = MoveState() ;
			if ( m_MoveDir == ePlNoMove ) {		// --- 十字キーが押されていない時
				if ( m_SpdAbs <= PLAYER_PARAM_MOVESPD_MAX ) {	// --- スピードが乗っていない時
					InitStay() ;
				}
				else {	// --- スピードが乗っている時
					ChangeAnim( ePlAnimStop, FALSE ) ;// --- 停止アニメーションをアタッチ
					m_ActionNo = ePlStop ;
				}
			}
			else {		// --- 十字キーが押されている時
				if ( m_SpdAbs <= PLAYER_PARAM_MOVESPD_MAX ) {
					m_SpdAbs += PLAYER_PARAM_MOVEAXL ;				// --- 加速
				}
				m_Rotate.y = PLAYER_MOVE_DIRECTION * m_MoveDir ;	// --- 回転をセット
				m_Spd.x = sin( m_Rotate.y + DX_PI_F) * m_SpdAbs ;	// --- 速度をセット
				m_Spd.z = cos( m_Rotate.y + DX_PI_F) * m_SpdAbs ;
				Move() ;											// --- 座標更新
			}

			// --- トリガーチェック
			switch ( TriggerState() )
			{
				// --- △ボタン
				case ePlTrgXButton :
					InitBackActiv() ;
					break ;

				// --- 〇ボタン
				case ePlTrgAButton :
					// --- ダッシュ可能フラグがオンの時
					if ( (m_Flg & PLAYER_FLG_DASH) != 0 ) {
						InitDash() ;
					}
					break ;

				// --- ×ボタン
				case ePlTrgBButton :
					InitJump() ;
					break ;

				// --- R1ボタン
				case ePlTrgR1Button :
					InitShot( ePlGunR ) ;
					break ;

				// --- L1ボタン
				case ePlTrgL1Button :
					InitShot( ePlGunL ) ;
					break ;
			}
			break ;

		// --- 停止
		case ePlStop :
			m_SpdAbs -= PLAYER_PARAM_MOVEAXL ;					// --- 減速
			m_Spd.x = sin( m_Rotate.y ) * (m_SpdAbs * -1) ;		// --- 速度をセット
			m_Spd.z = cos( m_Rotate.y ) * (m_SpdAbs * -1) ;
			Move() ;											// --- 座標更新
			if ( m_AnimationData.m_EndFlg == TRUE ) {			// --- アニメーションが終わった時
				InitStay() ;
			}
			break ;

		// --- ジャンプ
		case ePlJump :
			// --- 十字キーのチェック
			ControlAirSpd( MoveState() ) ;						// --- 速度制御
			m_Spd.y -= PLAYER_PARAM_JUMPGRAV ;					// --- 重力加速
			Move() ;											// --- 座標更新
			if ( m_Spd.y < 0.0f ) {								// --- 速度が負の値になったら
				ChangeAnim( ePlAnimFall, FALSE ) ;				// --- 降下アニメーションをアタッチ
				m_ActionNo = ePlFall ;							// --- 降下へ
			}
			// --- トリガーチェック
			switch ( TriggerState() )
			{
				// --- △ボタン
				case ePlTrgXButton :
					InitBackActiv() ;
					break ;

				// --- 〇ボタン
				case ePlTrgAButton :
					// --- ダッシュ可能フラグがオンの時
					if ( (m_Flg & PLAYER_FLG_DASH) != 0 ) {
						InitDash() ;
					}
					break ;

				// --- R1ボタン
				case ePlTrgR1Button :
					InitShot( ePlGunR ) ;				// --- 射撃へ
					break ;

				// --- L1ボタン
				case ePlTrgL1Button :
					InitShot( ePlGunL ) ;				// --- 射撃へ
					break ;
			}
			break ;

		// --- 降下
		case ePlFall :
			// --- 十字キーのチェック
			ControlAirSpd( MoveState() ) ;						// --- 速度制御
			m_Spd.y -= PLAYER_PARAM_JUMPGRAV ;					// --- 重力加速
			if ( Move() != 0 ) {								// --- 座標更新
				g_Sound.Play( SOUND_RANDINGSE_NO ) ;
				ChangeAnim( ePlAnimLanding, FALSE ) ;			// --- 着地硬直アニメーションをアタッチ
				m_Spd = VGet(0.0f,0.0f,0.0f) ;
				m_ActionNo = ePlLanding ;
			}
			// --- トリガーチェック
			switch ( TriggerState() )
			{
				// --- △ボタン
				case ePlTrgXButton :
					InitBackActiv() ;
					break ;

				// --- 〇ボタン
				case ePlTrgAButton :
					// --- ダッシュ可能フラグがオンの時
					if ( (m_Flg & PLAYER_FLG_DASH) != 0 ) {
						InitDash() ;
					}
					break ;

				// --- R1ボタン
				case ePlTrgR1Button :
					InitShot( ePlGunR ) ;				// --- 射撃へ
					break ;

				// --- L1ボタン
				case ePlTrgL1Button :
					InitShot( ePlGunL ) ;				// --- 射撃へ
					break ;
			}
			break ;

		// --- ダッシュ
		case ePlDash :
			Move() ;
			if ( m_AnimationData.m_EndFlg == TRUE ) {			// --- アニメーションが終わった時
				ChangeAnim( ePlAnimFall, FALSE ) ;				// --- 降下アニメーションをアタッチ
				m_AnimationData.m_AnimNowTime = 30.0f ;
				m_ActionNo = ePlFall ;							// --- 降下へ
			}
			break ;

		// --- 着地硬直
		case ePlLanding :
			if ( m_AnimationData.m_EndFlg == TRUE ) {			// --- アニメーションが終わった時
				InitStay() ;
			}
			break ;

		// --- 射撃
		case ePlShot :
			m_Spd.y -= PLAYER_PARAM_SHOTGRAV ;					// --- 重力加速
			LookTarget() ;										// --- ターゲットの方向を向く
			Move() ;
			if ( m_AnimationData.m_EndFlg == TRUE ) {			// --- アニメーションが終わった時
				ChangeAnim( ePlAnimFall, FALSE ) ;				// --- 降下アニメーションをアタッチ
				m_AnimationData.m_AnimNowTime = 30.0f ;
				m_ActionNo = ePlFall ;							// --- 降下へ
			}
			break ;

		// --- 背中装備発動
		case ePlBackActiv :
			LookTarget() ;
			if ( m_AnimationData.m_EndFlg == TRUE ) {
				if ( m_EquipBack.Open() ) {
					m_EquipBack.Shot( &mp_Target->m_HomingPt, m_PlayerNo ) ;

					ChangeAnim( ePlAnimFall, FALSE ) ;				// --- 降下アニメーションをアタッチ
					m_AnimationData.m_AnimNowTime = 30.0f ;
					m_ActionNo = ePlFall ;							// --- 降下へ

				}
			}
			break ;

		// --- 死に
		case ePlDie :
			m_Spd.y -= PLAYER_PARAM_JUMPGRAV ;					// --- 重力加速
			Move() ;											// --- 座標更新
			CutHoming() ;	// --- 誘導切り
			// --- 復活待機
			m_RespawnTime-- ;
			if ( m_RespawnTime <= 0 ) {
				m_ActionNo = ePlRespawn ;
			}
			break ;

		// --- 戦闘復帰
		case ePlRespawn :
			m_HitPoint = PLAYER_PARAM_HP_MAX ;			// --- 体力回復

			m_Flg |= PLAYER_FLG_INVINCIBLE ;			// --- 無敵フラグをオフに
			SetPosition( VGet(0.0f, 100.0f, 100.0f) ) ;	// --- リスポーン位置
			Move() ;									// --- ヒットチェック位置更新
			SetRotate( VGet(0.0f,0.0f,0.0f) ) ;			// --- 回転リセット

			// --- 武器の状態リセット
			m_EquipGun[0].ResetBul() ;
			m_EquipGun[1].ResetBul() ;
			m_EquipBack.ResetActiv() ;


			ChangeAnim( ePlAnimFall, FALSE ) ;				// --- 降下アニメーションをアタッチ
			m_ActionNo = ePlFall ;							// --- 降下へ
			break ;

		// --- カスタマイズ画面
		case ePlCustEq :
			m_Rotate.y += 0.0314f ;
			if ( (m_Flg & PLAYER_FLG_GETKEY) != 0 ) {
				SelectEquip() ;
			}
			break ;

		// --- それ以外(エラー)
		default :
			break ;
	}

	// --- 弾丸とのヒットチェック
	if ( (m_Flg & PLAYER_FLG_INVINCIBLE) != 0 ) {											// --- 無敵ではない時
		for ( int i = 0 ; i < OBJ_BULLET_MAX ; i++ ) {										// --- 弾丸オブジェクト最大数まで繰り返し	
			if ( g_Bullet[i].GetUseFlg() ) {												// --- 使用フラグを取得
				if ( m_PlayerNo != g_Bullet[i].m_PlayerNo ) {								// --- 相手の弾丸の時
					if ( HitCheck_Player_Bullet( m_HitCaps, g_Bullet[i].m_HitCaps) ) {		// --- ヒットしている時
						g_Sound.Play( SOUND_DAMEGESE_NO ) ;									// --- ダメージSE

						g_BattleManager.SetLastAttack( m_PlayerNo ) ;						// --- 最後に攻撃したプレイヤーをセット
						g_Bullet[i].SetUseFlg( FALSE ) ;									// --- 使用フラグをFALSEに
					if( g_LimitTime[0].AbsFlg == TRUE ){
						m_HitPoint -= g_Bullet[i].GetDamege() ;								// --- 体力減少
					}
						if ( m_HitPoint <= 0.0f ) {											// --- 体力が0以下になったら
							InitDie() ;														// --- 死ぬ
							break ;															// --- 死んだら抜ける
						}
					}
				}
			}
		}
	}

	// --- 翼発動時間の減少
	if ( m_EquipBack.m_Activating ) {
		m_EquipBack.m_ActivatingTime-- ;
		if ( m_EquipBack.m_ActivatingTime <= 0 ) {
			m_EquipBack.m_Activating	= FALSE ;
			m_EquipBack.AnimRev() ;
			m_EquipBack.m_Closing		= TRUE ;
		}
	}

	// --- 装備を閉じる
	if ( m_EquipBack.m_Closing ) {
		if ( m_EquipBack.Open() ) {
			m_EquipBack.StateReset() ;
		}
	}

	// --- リロード進行
	for ( int i = 0 ; i < 2 ; i++ ) {
		if ( m_EquipGun[i].GetActivFlg() != TRUE ) {
			if ( m_EquipGun[i].Reload() ) {
				m_EquipGun[i].ResetBul() ;
			}
		}
	}

	return 0 ;
}

//==================================================================//
//						キー情報入力								//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	トリガー判定										//
//==================================================================//
int Player::KeyInput()
{
	m_Key[ePlKeyTrg]	&= 0x00000000 ;							// ---トリガー情報をリセット
	m_Key[ePlKeyOld]	= m_Key[ePlKeyData] ;					// --- 1フレーム前の情報を保存
	m_Key[ePlKeyData]	= GetJoypadInputState( m_ConNo ) ;		// --- 現在のキー情報を取得

	// --- 右キートリガー
	if ( (m_Key[ePlKeyOld] & PAD_INPUT_RIGHT) == 0
			&& ((m_Key[ePlKeyData] & PAD_INPUT_RIGHT)) != 0 ) {
				m_Key[ePlKeyTrg] |= PAD_INPUT_RIGHT ;
	}

	// --- 左キートリガー
	if ( (m_Key[ePlKeyOld] & PAD_INPUT_LEFT) == 0
			&& ((m_Key[ePlKeyData] & PAD_INPUT_LEFT)) != 0 ) {
				m_Key[ePlKeyTrg] |= PAD_INPUT_LEFT ;
	}

	// --- 上キートリガー
	if ( (m_Key[ePlKeyOld] & PAD_INPUT_UP) == 0
			&& ((m_Key[ePlKeyData] & PAD_INPUT_UP)) != 0 ) {
				m_Key[ePlKeyTrg] |= PAD_INPUT_UP ;
	}

	// --- 下キートリガー
	if ( (m_Key[ePlKeyOld] & PAD_INPUT_DOWN) == 0
			&& ((m_Key[ePlKeyData] & PAD_INPUT_DOWN)) != 0 ) {
				m_Key[ePlKeyTrg] |= PAD_INPUT_DOWN ;
	}

	// --- △キートリガー
	if ( (m_Key[ePlKeyOld] & PAD_INPUT_A) == 0
			&& ((m_Key[ePlKeyData] & PAD_INPUT_A)) != 0 ) {
				m_Key[ePlKeyTrg] |= PAD_INPUT_A ;
	}

	// --- ○キートリガー
	if ( (m_Key[ePlKeyOld] & PAD_INPUT_B) == 0
			&& ((m_Key[ePlKeyData] & PAD_INPUT_B)) != 0 ) {
				m_Key[ePlKeyTrg] |= PAD_INPUT_B ;
	}

	// --- ×キートリガー
	if ( (m_Key[ePlKeyOld] & PAD_INPUT_C) == 0
			&& ((m_Key[ePlKeyData] & PAD_INPUT_C)) != 0 ) {
				m_Key[ePlKeyTrg] |= PAD_INPUT_C ;
	}

	// --- L1キートリガー
	if ( (m_Key[ePlKeyOld] & PAD_INPUT_L) == 0
			&& ((m_Key[ePlKeyData] & PAD_INPUT_L)) != 0 ) {
				m_Key[ePlKeyTrg] |= PAD_INPUT_L ;
	}

	// --- R1キートリガー
	if ( (m_Key[ePlKeyOld] & PAD_INPUT_R) == 0
			&& ((m_Key[ePlKeyData] & PAD_INPUT_R)) != 0 ) {
				m_Key[ePlKeyTrg] |= PAD_INPUT_R ;
	}

	return m_Key[ePlKeyData] ;
}

//==============================================================//
//		描画													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int Player::Draw()
{
	MV1SetPosition		( m_hModel, m_Pos ) ;		// --- 座標セット
	MV1SetRotationXYZ	( m_hModel,	m_Rotate ) ;	// --- 回転セット
	MV1SetScale			( m_hModel, m_Size ) ;		// --- 拡縮セット

	// --- アニメーションの状態をセット
	m_AnimationData.AnimProg() ;
	MV1SetAttachAnimTime( m_hModel, m_AnimationData.m_AttachIdx, m_AnimationData.m_AnimNowTime ) ;

	MV1DrawModel		( m_hModel ) ;				// --- 描画

	if ( (m_Flg & PLAYER_FLG_USESHADOW) != 0 ) {
		m_Shadow.DrawShadow( m_Pos ) ;
	}

	m_EquipGun[0].DrawEq() ;
	m_EquipGun[1].DrawEq() ;
	m_EquipBack.DrawEq() ;

	return 0 ;
}

//==============================================================//
//		全体初期セット											//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::InitializeData()
{
	// --- モデルハンドルの複製
	m_AnimationData.SetAnim( ANIM_PLAYER_NO, PLAYER_ANIM_NUMBER ) ;	// --- アニメーション

	m_Shadow.SetModel( MODEL_SHADOW_NO ) ;

	// --- アニメーション初期セット
	m_AnimationData.m_AttachIdx		= MV1AttachAnim( m_hModel, ePlAnimSty, m_AnimationData.m_hAnimation ) ;
	m_AnimationData.m_AnimTotalTime	= MV1GetAttachAnimTotalTime( m_hModel, m_AnimationData.m_AttachIdx ) ;
	m_AnimationData.m_AnimNowTime	= 0.0f ;
	m_AnimationData.m_AnimSpd		= 4.0 ;
	m_AnimationData.m_Loop			= TRUE ;

	// --- 装備データの参照
	m_SelectingEq		= ePlGunL ;

	m_EquipNo[ePlGunL]	= eGunFish ;
	m_EquipNo[ePlGunR]	= eGunBee ;
	m_EquipNo[ePlBack]	= eUnitWing ;

	m_EquipGun[ePlGunL].SetGrobalDeta( m_EquipNo[ePlGunL] ) ;
	m_EquipGun[ePlGunR].SetGrobalDeta( m_EquipNo[ePlGunR] ) ;
	m_EquipBack.SetGrobalDeta( m_EquipNo[ePlBack] ) ;

	// --- 装備接続フレームの設定
	m_EquipGun[ePlGunL].SetJoinFrame( m_hModel, "LeftGun" ) ;
	m_EquipGun[ePlGunR].SetJoinFrame( m_hModel, "RightGun" ) ;
	m_EquipBack.SetJoinFrame( m_hModel, "BackUnit" ) ;

	// --- フラグを全てリセット ---------------------------------------- //
	m_Flg					= 0xffffffff ;

	return 0 ;
}

//==============================================================//
//		戦闘初期セット											//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::InitializeBattle()
{
	// --- アクション用データ
	m_Rotate				= VGet(0.0f,0.0f,0.0f) ;
	m_Size					= VGet(1.0f,1.0f,1.0f) ;
	m_HitPoint				= PLAYER_PARAM_HP_MAX ;
	m_Flg					= 0xffffffff ;

	// --- ヒットチェック用カプセル
	m_HitCaps.m_Param[eHitStart]	= m_Pos ;
	m_HitCaps.m_Param[eHitEnd]		= m_Pos ;
	m_HitCaps.m_Param[eHitEnd].y	+= PLAYER_HITCHECK_HIGHT ;
	m_HitCaps.m_CapRad				= PLAYER_HITCHECK_WIDTH ;
	m_HomingPt						= m_Pos ;
	m_HomingPt.y					+= PLAYER_HITCHECK_HIGHT ;

	InitStay() ;					// --- 静止へ

	m_EquipBack.StateReset() ;

	m_UseFlg = TRUE ;

	return 0 ;
}

//==============================================================//
//		カスタマイズ初期セット									//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::InitializeCustom()
{
	// --- 装備データの参照
	m_SelectingEq		= ePlGunR ;
	m_Flg				= (0xffffffff ^ PLAYER_FLG_USESHADOW) ;
//	m_Flg				|= PLAYER_FLG_EQCONFIRM | PLAYER_FLG_GETKEY ;

	ChangeAnim( ePlAnimStt, TRUE ) ;				// --- 棒立ちポーズ

	m_Size		= VGet(2.0f,2.0f,2.0f) ;
	m_Rotate	= VGet( 0.0f,0.0f,0.0f ) ;			// --- 回転なし

	m_ActionNo	= ePlCustEq ;						// --- 武器カスタマイズへ

	m_EquipBack.StateReset() ;

	m_UseFlg	= TRUE ;

	return 0 ;
}

//==============================================================//
//		リザルト初期セット										//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::InitializeResult()
{
	m_Flg				= (0xffffffff ^ PLAYER_FLG_USESHADOW) ;

	ChangeAnim( ePlAnimStt, TRUE ) ;				// --- 棒立ちポーズ

	m_Size		= VGet(2.0f,2.0f,2.0f) ;
	m_Rotate	= VGet( 0.0f,0.0f,0.0f ) ;			// --- 回転なし

	m_ActionNo = ePlDebug ;							// --- 武器カスタマイズへ

	m_EquipBack.StateReset() ;

	m_UseFlg = TRUE ;

	return 0 ;
}

//==============================================================//
//		対応コントローラーのセット								//
//--------------------------------------------------------------//
//		IN  : コントローラーの定数								//
//		OUT : 終了判定											//
//==============================================================//
int Player::SetController( int arg_ConNo )
{
	m_ConNo = arg_ConNo ;

	return 0 ;
}

//==============================================================//
//		プレイヤー番号のセット									//
//--------------------------------------------------------------//
//		IN  : プレイヤー番号									//
//		OUT : 終了判定											//
//==============================================================//
int Player::SetPlayerNo( int arg_PNo )
{
	m_PlayerNo = arg_PNo ;

	return 0 ;
}
//==============================================================//
//		ターゲットのセット										//
//--------------------------------------------------------------//
//		IN  : ターゲットのアドレス								//
//		OUT : 終了判定											//
//==============================================================//
int Player::SetTarget( Player* argp_Tgt )
{
	mp_Target = argp_Tgt ;

	return 0 ;
}

//==============================================================//
//		装備変更												//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::SelectEquip()
{
	if ( (m_Key[ePlKeyTrg] & PAD_INPUT_B) != 0 ) {	// --- 〇ボタントリガー
		g_Sound.Play( SOUND_CURSORCK_NO ) ;
		m_Flg ^= PLAYER_FLG_EQCONFIRM ;
	}

	// --- 確定されていないとき ------------------------------------------------------------ //
	if ( (m_Flg & PLAYER_FLG_EQCONFIRM) != 0 ) {
		// --- 変更する装備を選択 -------------------------------------------------- //
		if ( (m_Key[ePlKeyTrg] & PAD_INPUT_DOWN) != 0 ) {	// --- 下キートリガー
			g_Sound.Play( SOUND_CURSORMOVE_NO ) ;
			m_SelectingEq++ ;								// --- 変更する装備番号を変更
			if ( m_SelectingEq > ePlBack ) {
				m_SelectingEq = ePlGunR ;					// --- 配列サイズを上回ったら0
			}
		}
		if ( (m_Key[ePlKeyTrg] & PAD_INPUT_UP) != 0 ) {		// --- 上キートリガー
			g_Sound.Play( SOUND_CURSORMOVE_NO ) ;
			m_SelectingEq-- ;								// --- 変更する装備番号を変更
			if ( m_SelectingEq < 0 ) {
				m_SelectingEq = ePlBack ;					// --- 配列サイズを下回ったら最大値
			}
		}

		// --- 参照するグローバル配列番号を変更 ----------------------------------- //
		if ( (m_Key[ePlKeyTrg] & PAD_INPUT_RIGHT) != 0 ) {	// --- 右ボタントリガー
			g_Sound.Play( SOUND_CURSORMOVE_NO ) ;
			m_EquipNo[m_SelectingEq]++ ;					// --- 参照する配列番号を変更
		}
		if ( (m_Key[ePlKeyTrg] & PAD_INPUT_LEFT) != 0 ) {	// --- 左ボタントリガー
			g_Sound.Play( SOUND_CURSORMOVE_NO ) ;
			m_EquipNo[m_SelectingEq]-- ;					// --- 参照する配列番号を変更
		}
		// --- 銃の選択 ----------------------------------------------------- //
		if ( m_SelectingEq != ePlBack ) {
			if ( m_EquipNo[m_SelectingEq] >= DATA_GUN_MAX ) {
				m_EquipNo[m_SelectingEq] = 0 ;				// --- 配列サイズを超えたら0に
			}
			if ( m_EquipNo[m_SelectingEq] < 0 ) {
				m_EquipNo[m_SelectingEq] = DATA_GUN_MAX - 1 ;// --- 配列サイズを下回ったら最大値に
			}
			m_EquipGun[m_SelectingEq].SetGrobalDeta( m_EquipNo[m_SelectingEq] ) ;	// --- 変更後の配列データを参照
		}
		// --- 背中装備の選択 ----------------------------------------------------- //
		else if ( m_SelectingEq == ePlBack) {

			if ( m_EquipNo[m_SelectingEq] >= DATA_BACK_MAX ) {
				m_EquipNo[m_SelectingEq] = 0 ;				// --- 配列サイズを超えたら0に
			}
			if ( m_EquipNo[m_SelectingEq] < 0 ) {
				m_EquipNo[m_SelectingEq] = DATA_BACK_MAX - 1 ;// --- 配列サイズを下回ったら最大値に
			}
			m_EquipBack.SetGrobalDeta( m_EquipNo[m_SelectingEq] ) ;	// --- 変更後の配列データを参照
		}
	}

	return 0 ;
}

//==============================================================//
//		フラグの取得											//
//--------------------------------------------------------------//
//		IN  : 取得したいフラグ									//
//		OUT : フラグ											//
//==============================================================//
int Player::GetFlg( int arg_FlgNo )
{
	switch ( arg_FlgNo )
	{
		case ePlConfEq : return m_Flg & PLAYER_FLG_EQCONFIRM ;
	}

	return PLAYER_FLG_FAILURE ;
}

//==============================================================//
//		平行移動のチェック										//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 移動方向											//
//==============================================================//
int Player::MoveState()
{
	u_char move = 0x00 ;	// --- 十字キーのみチェック

	if ( (m_Key[ePlKeyData] & PAD_INPUT_UP) != 0 ) {	// --- 上キーデータ
		move |= PLAYER_MOVE_UP ;
	}
	if ( (m_Key[ePlKeyData] & PAD_INPUT_DOWN) != 0 ) {	// --- 下キーデータ
		move |= PLAYER_MOVE_DOWN ;
	}
	if ( (m_Key[ePlKeyData] & PAD_INPUT_LEFT) != 0 ) {	// --- 左キーデータ
		move |= PLAYER_MOVE_LEFT ;
	}
	if ( (m_Key[ePlKeyData] & PAD_INPUT_RIGHT) != 0 ) {	// --- 右キーデータ
		move |= PLAYER_MOVE_RIGHT ;
	}

	switch ( move )
	{
		case PLAYER_MOVE_UP :						return ePlUp ;				// --- 奥
		case PLAYER_MOVE_DOWN :						return ePlDown ;			// --- 手前
		case PLAYER_MOVE_LEFT :						return ePlLeft ;			// --- 左
		case PLAYER_MOVE_RIGHT :					return ePlRight ;			// --- 右
		case PLAYER_MOVE_UP | PLAYER_MOVE_LEFT :	return ePlLeftUp ;			// --- 左奥
		case PLAYER_MOVE_UP | PLAYER_MOVE_RIGHT :	return ePlRightUp ;			// --- 右奥
		case PLAYER_MOVE_DOWN | PLAYER_MOVE_LEFT :	return ePlLeftDown ;		// --- 左手前
		case PLAYER_MOVE_DOWN | PLAYER_MOVE_RIGHT :	return ePlRightDown ;		// --- 左奥
		default :									return ePlNoMove ;			// --- 移動なし
	}

	return -1 ;		// --- ここにたどり着いたら失敗
}

//==============================================================//
//		バトルシーン中のトリガーチェック						//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : ボタントリガー									//
//==============================================================//
int Player::TriggerState()
{
	// --- △キートリガー
	if ( (m_Key[ePlKeyTrg] & PAD_INPUT_A) != 0 ) return ePlTrgXButton ;
	// --- ○キートリガー
	if ( (m_Key[ePlKeyTrg] & PAD_INPUT_B) != 0 ) return ePlTrgAButton ;
	// --- ×キートリガー
	if ( (m_Key[ePlKeyTrg] & PAD_INPUT_C) != 0 ) return ePlTrgBButton ;
	// --- R1キートリガー
	if ( (m_Key[ePlKeyTrg] & PAD_INPUT_R) != 0 ) return ePlTrgR1Button ;
	// --- L1キートリガー
	if ( (m_Key[ePlKeyTrg] & PAD_INPUT_L) != 0 ) return ePlTrgL1Button ;

	return 0 ;
}

//==============================================================//
//		静止に必要なパラメータを設定							//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::InitStay()
{
	m_Spd = VGet(0.0f,0.0f,0.0f) ;
	ChangeAnim( ePlAnimSty, TRUE ) ;	// --- 静止アニメーションをアタッチ

	m_Flg |= PLAYER_FLG_DASH ;			// --- ダッシュ可能フラグをオンに

	m_ActionNo = ePlStay ;

	return 0 ;
}

//==============================================================//
//		ジャンプに必要なパラメータを設定						//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::InitJump()
{
	ChangeAnim( ePlAnimJp, FALSE ) ;	// --- ジャンプアニメーションをアタッチ
	m_Spd.y = PLAYER_PARAM_JUMPSPD ;	// --- ジャンプ初速セット

	g_Sound.Play( SOUND_JUMP_NO ) ;

	m_ActionNo = ePlJump ;				// --- ジャンプへ

	return 0 ;
}

//==============================================================//
//		射撃に必要なパラメータを設定							//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::InitShot( int arg_ShotLR )
{
	m_Spd	= VScale( m_Spd, 0.8f ) ;
	m_Spd.y	= 0.0f ;

	// --- ターゲットの方を向く
	LookTarget() ;
	MV1SetRotationXYZ( m_hModel, m_Rotate) ;

	ChangeAnim( arg_ShotLR + PLAYER_ANIM_PARAM_SHOTOFS, FALSE ) ;	// --- アニメーション変更

	m_EquipGun[arg_ShotLR].Shot( &mp_Target->m_HomingPt, m_PlayerNo ) ;			// --- 発射

	m_ActionNo = ePlShot ;

	return 0 ;
}

//==============================================================//
//		背中装備発動に必要なパラメータを設定					//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::InitBackActiv()
{
	// --- 発動可能な時
	if ( m_EquipBack.GetActivFlg() ) {
		m_Spd = VGet(0.0f,0.0f,0.0f) ;			// --- 速度を0に

		ChangeAnim( ePlAnimCn, FALSE ) ;		// --- アニメーション変更

		m_ActionNo = ePlBackActiv ;				// --- 背中装備発動へ
	}

	return 0 ;
}
	
//==============================================================//
//		ダッシュに必要なパラメータを設定						//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::InitDash()
{
	m_Spd = VGet(0.0f,0.0f,0.0f) ;					// --- 速度を0に

	g_Sound.Play( SOUND_DASHSE_NO ) ;

	// --- 速度セット
	m_SpdAbs = PLAYER_PARAM_DASHSPD ;				// --- 速度絶対値セット

	m_Spd.x = sin( m_Rotate.y ) * (m_SpdAbs * -1) ;	// --- 速度をセット
	m_Spd.z = cos( m_Rotate.y ) * (m_SpdAbs * -1) ;
	m_Spd.y = PLAYER_PARAM_DASHSPD_Y ;

	// --- 翼発動時間中
	if ( m_EquipBack.m_Activating ) {
		CutHoming() ;								// --- 誘導切り
	}

	ChangeAnim( ePlAnimDs, FALSE ) ;				// --- アニメーション変更

	m_Flg &= (0xffffffff ^ PLAYER_FLG_DASH) ;		// --- ダッシュ可能フラグをオフ

	m_ActionNo = ePlDash ;							// --- ダッシュへ

	return 0 ;
}

//==============================================================//
//		死亡に必要なパラメータを設定							//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::InitDie()
{
	m_Spd			= VGet(0.0f,0.0f,0.0f) ;					// --- 速度を0に

	ChangeAnim( ePlAnimDie, FALSE ) ;							// --- アニメーション変更

	m_Flg			&= (0xffffffff ^ PLAYER_FLG_INVINCIBLE) ;	// --- 無敵フラグをオン
	
	m_RespawnTime	= 200 ;										// --- 復活待機

	g_BattleManager.AddDefeatNo( m_PlayerNo ) ;

	m_ActionNo = ePlDie ;										// --- 死亡へ

	return 0 ;
}

//==================================================================//
//						座標更新									//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int Player::Move()
{
	int		StageHit ;				// --- 地面とのヒットチェック情報
	VECTOR	AddSpd ;				// --- 移動量

	AddSpd = m_Spd ;				// --- 移動量セット

	// --- 翼発動時間中
	if ( m_EquipBack.m_Activating ) {
		AddSpd.x *= 1.5f ;			// --- 平行移動量を増やす
		AddSpd.z *= 1.5f ;
	}

	HitCheckWall( AddSpd ) ;				// --- 壁とのヒットチェック
	StageHit = HitCheckStage( AddSpd ) ;	// --- 床とのヒットチェック

	// --- 座標更新 --------------------------------------------------------------- //
	m_Pos							= VAdd( m_Pos, AddSpd ) ;
	m_HitCaps.m_Param[eHitStart]	= m_Pos ;
	m_HitCaps.m_Param[eHitEnd]		= m_Pos ;
	m_HitCaps.m_Param[eHitEnd].y	+= PLAYER_HITCHECK_HIGHT ;
	m_HomingPt						= m_Pos ;
	m_HomingPt.y					+= PLAYER_HITCHECK_HIGHT ;

	return StageHit ;
}

//==============================================================//
//		足元のヒットチェック									//
//--------------------------------------------------------------//
//		IN  : 移動量											//
//		OUT : ヒット判定										//
//==============================================================//
int Player::HitCheckStage( VECTOR &arg_Spd )
{
	HitResultStg Result = HitCheck_Player_Stage( m_HitCaps, arg_Spd, g_Stage[0].m_HitStg ) ;	// --- ステージとのヒットチェック

	if ( Result.Hit == 1 ) {
		arg_Spd = Result.Speed ;
	}

	return Result.Hit ;
}

//==============================================================//
//		側面のヒットチェック									//
//--------------------------------------------------------------//
//		IN  : 移動量											//
//		OUT : 終了判定											//
//==============================================================//
int Player::HitCheckWall( VECTOR &arg_Spd )
{
	HitResultSide	HitSideRes ;		// --- 横方向ヒットチェック結果
	float			WallLength ;		// --- 壁との距離

	// --- ステージ壁 ------------------------------------------------------------------------------- //
	// --- 奥
	WallLength = HitCheck_Player_Wall( m_HitCaps, arg_Spd, g_Stage[0].m_HitWall[eStgTop] ) ;
	if ( WallLength < PLAYER_HITCHECK_WIDTH ) {
		arg_Spd.z -= PLAYER_HITCHECK_WIDTH - WallLength ;
	}
	// --- 手前
	WallLength = HitCheck_Player_Wall( m_HitCaps, arg_Spd, g_Stage[0].m_HitWall[eStgBottom] ) ;
	if ( WallLength < PLAYER_HITCHECK_WIDTH ) {
		arg_Spd.z += PLAYER_HITCHECK_WIDTH - WallLength ;
	}
	// --- 右
	WallLength = HitCheck_Player_Wall( m_HitCaps, arg_Spd, g_Stage[0].m_HitWall[eStgRight] ) ;
	if ( WallLength < PLAYER_HITCHECK_WIDTH ) {
		arg_Spd.x -= PLAYER_HITCHECK_WIDTH - WallLength ;
	}
	// --- 左
	WallLength = HitCheck_Player_Wall( m_HitCaps, arg_Spd, g_Stage[0].m_HitWall[eStgLeft] ) ;
	if ( WallLength < PLAYER_HITCHECK_WIDTH ) {
		arg_Spd.x += PLAYER_HITCHECK_WIDTH - WallLength ;
	}

	// --- ステージ障害物 --------------------------------------------------------------------------- //
	for ( int i = 0 ; i < OBJ_BOX_MAX ; i++ ) {
		if ( g_Box[i].GetUseFlg() ) {
			// --- 手前
			WallLength = HitCheck_Player_Wall( m_HitCaps, arg_Spd,
				g_Box[i].m_HitSide[eBoxTop] ) ;
			if ( WallLength < PLAYER_HITCHECK_WIDTH ) {
				arg_Spd.z -= PLAYER_HITCHECK_WIDTH - WallLength ;
			}
			// --- 奥
			WallLength = HitCheck_Player_Wall( m_HitCaps, arg_Spd,
				g_Box[i].m_HitSide[eBoxBottom] ) ;
			if ( WallLength < PLAYER_HITCHECK_WIDTH ) {
				arg_Spd.z += PLAYER_HITCHECK_WIDTH - WallLength ;
			}
			// --- 右
			WallLength = HitCheck_Player_Wall( m_HitCaps, arg_Spd,
				g_Box[i].m_HitSide[eBoxRight] ) ;
			if ( WallLength < PLAYER_HITCHECK_WIDTH ) {
				arg_Spd.x += PLAYER_HITCHECK_WIDTH - WallLength ;
			}
			// --- 左
			WallLength = HitCheck_Player_Wall( m_HitCaps, arg_Spd,
				g_Box[i].m_HitSide[eBoxLeft] ) ;
			if ( WallLength < PLAYER_HITCHECK_WIDTH ) {
				arg_Spd.x -= PLAYER_HITCHECK_WIDTH - WallLength ;
			}
		}
	}

	// --- プレイヤー
	HitSideRes = Hitcheck_Player_Player( m_HitCaps, arg_Spd, mp_Target->m_HitCaps ) ;
	if ( HitSideRes.Hit ) {
		arg_Spd.x += HitSideRes.xDif ;
		arg_Spd.z += HitSideRes.zDif ;
	}

	return 0 ;
}

//==============================================================//
//		ターゲットの方向を向く									//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::LookTarget()
{
	VECTOR	SubVec ;
	float	Angle ;

	SubVec = VSub( mp_Target->GetPosition(), m_Pos ) ;	// --- ターゲットとの座標の差を測る
	Angle = atan2( SubVec.x, SubVec.z ) ;				// --- 角度を取得

	m_Rotate.y = Angle + DX_PI_F ;						// --- 角度をセット

	return 0 ;
}

//==============================================================//
//		装備の残弾数を返す										//
//--------------------------------------------------------------//
//		IN  : 装備番号											//
//		OUT : 残弾数											//
//==============================================================//
int Player::GetEquipNowBul( int arg_EquipNo )
{
	if ( arg_EquipNo == ePlBack ) {
		return m_EquipBack.GetNowBul() ;
	}
	else {
		return m_EquipGun[arg_EquipNo].GetNowBul() ;
	}

	return -1 ;
}

//==============================================================//
//		装備の種類を返す										//
//--------------------------------------------------------------//
//		IN  : 装備番号											//
//		OUT : 種類												//
//==============================================================//
int Player::GetEquipType( int arg_EquipNo )
{
	if ( arg_EquipNo == ePlBack ) {
		return m_EquipBack.GetType() ;
	}
	else {
		return m_EquipGun[arg_EquipNo].GetType() ;
	}

	return -1 ;
}

//==============================================================//
//		空中での速度制御										//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::ControlAirSpd( int arg_Direction )
{
	// --- 十字キーが押されていないとき
	if ( arg_Direction == ePlNoMove ) {
		return 0 ;
	}
	// --- 同じ方向の時
	if ( arg_Direction == m_MoveDir ) {
		if ( m_SpdAbs <= PLAYER_PARAM_MOVESPD_MAX ) {
			m_SpdAbs += PLAYER_PARAM_MOVEAXL ;					// --- 加速
		}
		m_Spd.x = sin( m_Rotate.y + DX_PI_F ) * m_SpdAbs ;		// --- 速度をセット
		m_Spd.z = cos( m_Rotate.y + DX_PI_F ) * m_SpdAbs ;
	}
	else {
		m_Rotate.y = arg_Direction * PLAYER_MOVE_DIRECTION ;			// --- 回転を設定
	}

	return 0 ;
}

//==============================================================//
//		誘導切り												//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 終了判定											//
//==============================================================//
int Player::CutHoming()
{
	for ( int i = 0 ; i < OBJ_BULLET_MAX ; i++ ) {			// --- 弾丸オブジェクト最大数まで繰り返し	
		if ( g_Bullet[i].GetUseFlg() ) {					// --- 使用フラグを取得
			if ( m_PlayerNo != g_Bullet[i].m_PlayerNo ) {	// --- 相手の弾丸の時
				g_Bullet[i].SetHoming( FALSE ) ;			// --- 誘導切り
			}
		}
	}
	

	return 0 ;
}

//==============================================================//
//		キー受付切り替え										//
//--------------------------------------------------------------//
//		IN  : オンオフ											//
//		OUT : 終了判定											//
//==============================================================//
int Player::KeyLock( int arg_LockFlg )
{
	if ( arg_LockFlg ) {
		m_Flg			&= (0xffffffff ^ PLAYER_FLG_GETKEY) ;	// --- キー受付切りフラグをオフ
	}
	else {
		m_Flg			|=  PLAYER_FLG_GETKEY ;	// --- キー受付フラグをオン
	}

	return 0 ;
}

//==============================================================//
//		選択中の武器をゲット									//
//--------------------------------------------------------------//
//		IN  : なし												//
//		OUT : 選択中の武器を									//
//==============================================================//
int Player::GetSelecting()
{
	return m_SelectingEq ;
}