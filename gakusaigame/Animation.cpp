///////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							Animation.cpp														//
//																								//
//							作成日時 : 2020.10/16												//
//							作成者   : 天童律希													//
//																								//
//							アニメーションに必要なデータクラス									//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include "Common.h"

//==================================================================//
//		アニメーションハンドルをセット								//
//------------------------------------------------------------------//
//		IN  : int アニメーションハンドル arg_Anim,					//
//			  int 配列のサイズ arg_ArraySize						//
//		OUT : 終了判定												//
//==================================================================//
int Animation::SetAnim( int arg_gAnimNo, int arg_ArraySize )
{
	m_hAnimation	= MV1DuplicateModel( g_hAnim[arg_gAnimNo] ) ;
	m_AnimSize		= arg_ArraySize ;

	return 0 ;
}

//==================================================================//
//		アニメーション進行											//
//------------------------------------------------------------------//
//		IN  : なし													//
//		OUT : 終了判定												//
//==================================================================//
int Animation::AnimProg()
{
	if ( m_EndFlg != TRUE ) {
		if ( m_Reverse ) {
			m_AnimNowTime -= m_AnimSpd ;
			if ( m_AnimNowTime < 0.0f ) {	// --- 総時間を超えたら
				m_AnimNowTime = 0.0f ;
				m_Reverse = FALSE ;
				m_EndFlg = TRUE ;
			}
		}
		else {
			m_AnimNowTime += m_AnimSpd ;
			if ( m_AnimNowTime > m_AnimTotalTime ) {	// --- 総時間を超えたら
				if ( m_Loop ) {
					m_AnimNowTime -= m_AnimTotalTime ;	// --- アニメーションをループさせる
				}
				else {
					m_AnimNowTime = m_AnimTotalTime ;
					m_EndFlg = TRUE ;
				}
			}
		}
	}

	return m_EndFlg ;
}

//==================================================================//
//		アニメーションアタッチ										//
//------------------------------------------------------------------//
//		IN  : アタッチされるモデルハンドル,アニメーション番号		//
//		OUT : 成功判定												//
//==================================================================//
int Animation::AnimAttach( int arg_hModel, int arg_AnimNo )
{
	MV1DetachAnim( arg_hModel, m_AttachIdx ) ;									// --- 現在のアニメーションをデタッチ
	m_AttachIdx		= MV1AttachAnim( arg_hModel, arg_AnimNo, m_hAnimation ) ;	// --- 新たなアニメーションをアタッチ
	m_AnimTotalTime	= MV1GetAttachAnimTotalTime( arg_hModel, m_AttachIdx ) ;	// --- 総時間を取得
	m_AnimNowTime	= 0.0f ;													// --- 現在のアニメーション時間をリセット

	return m_AttachIdx ;
}

//==================================================================//
//		逆再生をセット												//
//------------------------------------------------------------------//
//		IN  : void													//
//		OUT : 終了判定												//
//==================================================================//
int Animation::SetReverse()
{
	m_Reverse = TRUE ;
	m_EndFlg = FALSE ;

	return 0 ;
}

//==================================================================//
//		アニメーション開始											//
//------------------------------------------------------------------//
//		IN  : void													//
//		OUT : 終了判定												//
//==================================================================//
int Animation::AnimStart()
{
	m_EndFlg = FALSE ;

	return 0 ;
}

