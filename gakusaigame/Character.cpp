//////////////////////////////////////////////////////////////////////////////////////////////
//																							//
//							Character.cpp													//
//																							//
//							作成日時 : 2020.9/17											//
//							作成者   : 天童律希												//
//																							//
//							アニメーションクラスのメソッド定義								//
//																							//
//////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include "Common.h"

//==================================================================//
//		アニメーション変更											//
//------------------------------------------------------------------//
//		IN  : int アニメーション配列番号							//
//		OUT : 成功判定												//
//==================================================================//
int Character::ChangeAnim( int arg_AnimNo, int arg_Loop )
{
	if ( (arg_AnimNo < m_AnimationData.m_AnimSize) && (arg_AnimNo >= 0) ) {		// --- 引数正しい引数が送られてきた場合
		m_AnimationData.AnimAttach( m_hModel, arg_AnimNo ) ;					// --- 新たにアニメーションをアタッチ
		m_AnimationData.m_Loop			= arg_Loop ;							// --- ループフラグをセット
		m_AnimationData.m_EndFlg		= FALSE ;								// --- 終了フラグをFALSEに
	}
	else {																		// --- 引数が異常な値の場合エラーを返す
		return 1 ;
	}

	return 0 ;
}

//==============================================================//
//		描画													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int Character::Draw()
{
	MV1SetPosition		( m_hModel, m_Pos ) ;		// --- 座標セット
	MV1SetRotationXYZ	( m_hModel,	m_Rotate ) ;	// --- 回転セット
	MV1SetScale			( m_hModel, m_Size ) ;		// --- 拡縮セット

	// --- アニメーションの状態をセット
	m_AnimationData.AnimProg() ;
	MV1SetAttachAnimTime( m_hModel, m_AnimationData.m_AttachIdx, m_AnimationData.m_AnimNowTime ) ;	// --- 現在のアニメーション状態をセット

	MV1DrawModel		( m_hModel ) ;				// --- 描画

	return 0 ;
}










/* [EOF] */