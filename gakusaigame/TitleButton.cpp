/*	=============================================================================================================

							TitleButton.cpp
									作成日時 : 2020/10/08
									作成者	 : 加藤諒人

									タイトルボタン関連の処理

	=============================================================================================================*/
#include <DxLib.h>
#include "Common.h"

/*	Action
====================================================================*/
int TitleButton::Action(){
	keyInput() ;

	switch( m_CBFlg )
	{
		/*	ボタン自動点滅
		=========================================================*/
		case  eSTActionInit :
			m_UseFlg = FALSE ;
			m_CBCnt++ ; 
			if( (m_CBCnt & 0x20) < 8 ){
				m_UseFlg = TRUE ;
			}
			/*	STARTが押された場合、ボタン高速点滅用のモードに設定
			=========================================================*/
			if( m_StartCnt == TRUE  ){
				g_Sound.Play( SOUND_CURSORCK_NO ) ;

				m_CBCnt = 50 ;
				m_CBFlg = eSTActionNow ;
			}	
			break ;
		/*	ボタン高速点滅モード中、疑似タイマーを使用
		===========================================================*/
		case eSTActionNow :
			m_CBCnt-- ;
			if(( m_CBCnt & 0x04 ) != 4 )
				m_UseFlg = FALSE ;
			else{
				m_UseFlg = TRUE ;
			}
			/*	疑似タイマーがゼロになったらシーン移動用のモードへ
			=======================================================*/
			if( m_CBCnt < 0 )
					m_CBFlg = eSTActionNon ;
			break ;
		/*	シーン移動用のフラグをTRUEにする
		========================================================*/
		case eSTActionNon :
			ASceneFlg = TRUE ;
			break ;
	}
	return 0 ;
}

/*	Initialize(初期化)
=======================================================================*/
int TitleButton::Initialize(int TBInit){

	BDFlg = 0 ;
	ASceneFlg = FALSE ; 
	m_StartCnt = FALSE ;
	m_CBFlg = eSTActionInit ;

	m_DrawRect.left		= 210 ;			// --- 描画開始x座標
	m_DrawRect.top		= 400	;		// --- 描画開始y座標
	m_DrawRect.right	= 700 ;			// --- 描画終了x座標
	m_DrawRect.bottom	= 400 ;			// --- 描画終了x座標

	m_CutPt.x		  =	   0 ;			// --- 切り取りx座標
	m_CutPt.y		  =	   150 ;		// --- 切り取りy座標

	m_hGraph = g_hGraph[IMAGE_STARTBUT_NO] ;	// --- 

	m_UseFlg = TRUE ;
	

	return m_hGraph ;
} 

/*	Draw	/	描画
=======================================================================*/
int TitleButton::Draw(){
	DrawRectGraph(
	m_DrawRect.left,
	m_DrawRect.top,
	m_CutPt.x,
	m_CutPt.y,
	m_DrawRect.right,
	m_DrawRect.bottom,
	m_hGraph,
	TRUE, FALSE, FALSE
	) ;

	return 0 ;
}

/*	キー情報入力用
========================================================*/
int TitleButton::keyInput(){

	m_Skey[STKeyTrg]	&= 0x00000000 ;									// ---トリガー情報をリセット
	m_Skey[STKeyOld]	= m_Skey[STKeyData] ;							// --- 1フレーム前の情報を保存
	m_Skey[STKeyData]	= GetJoypadInputState( DX_INPUT_KEY_PAD1 ) ;	// --- 現在のキー情報を取得
	
	// --- STARTキートリガー
	if ( ((m_Skey[STKeyOld] & 0x00002000) == 0)
			&& ((m_Skey[STKeyData] & 0x00002000) != 0) ) {
				m_StartCnt = TRUE ;
	}

	return 0 ;

}
/*	ロード
==============================================================================================*/
int TitleButton::LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo ){
	m_hGraph = DerivationGraph(
		arg_CutRect->left,
		arg_CutRect->top,
		arg_CutRect->right,
		arg_CutRect->bottom,
		g_hGraph[arg_gGraphNo]
		) ;

	return m_hGraph ;
}


