//////////////////////////////////////////////////////////////////////////////////////////////
//																							//
//							Bullet.cpp														//
//																							//
//							作成日時 : 2020.10/25											//
//							作成者   : 天童律希												//
//																							//
//							弾丸オブジェクトクラスのメソッド定義							//
//																							//
//////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h> 
#include <math.h>
#include "Common.h"

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		グローバルデータ
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//==================================================================//
//		データセット												//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int BulletData::InitializeData( int arg_Type, int arg_gModelNo, int arg_gAnimNo, float arg_HomPow, int arg_HomTime , float arg_Spd, float arg_Power )
{
	m_Type			= arg_Type ;		// --- 種類
	m_gModelNo		= arg_gModelNo ;	// --- モデル配列番号
	m_gAnimNo		= arg_gAnimNo ;		// --- アニメーション配列番号
	m_HomingPower	= arg_HomPow ;		// --- ホーミング力
	m_HomingTime	= arg_HomTime ;		// --- ホーミング持続時間
	m_SpdAbs		= arg_Spd ;			// --- 速度
	m_DamegePower	= arg_Power ;		// --- 威力

	return 0 ;
}

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		オブジェクトメソッド
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//==================================================================//
//																	//
//		コンストラクタ												//
//																	//
//==================================================================//
BulletObj::BulletObj()
{
	m_HitCaps.m_CapRad = BULLET_HITCHECK_RADIUS ;
}

//==================================================================//
//		空きオブジェクトのサーチ									//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	配列番号											//
//==================================================================//
int	BulletObj::Search()
{
	for ( int i = 0 ; i < OBJ_BULLET_MAX ; i++ ) {
		if ( g_Bullet[i].GetUseFlg() != TRUE ) {
			return i ;
		}
	}

	return -1 ;
}

//==================================================================//
//		アクション													//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int BulletObj::Action()
{
	// --- ホーミングする時
	if ( m_HomingFlg ) {
		Homing() ;											// --- ホーミング制御
		LookMoveDir() ;										// --- 進行方向を向く
		m_HomingTime-- ;									// --- ホーミング持続時間--
		if ( m_HomingTime <= 0) {
			m_HomingFlg = FALSE ;
		}
	}

	m_Pos					= VAdd( m_Pos,m_Spd ) ;		// --- 移動
	m_HitCaps.m_Param[0]	= m_Pos ;					// --- ヒットチェック座標の移動

	// --- 壁とのヒットチェック
	if ( HitCheck_Bullet_Stage( m_HitCaps, g_Stage[0].m_HitStg ) ) {
		m_ActionNo	= -1 ;
		m_UseFlg	= FALSE ;
	}
	// --- 箱とのヒットチェック
	for ( int i = 0 ; i < OBJ_BOX_MAX ; i++ ) {
		if ( g_Box[i].GetUseFlg() ) {
			for ( int j = 0 ; j < 4 ; j++ ) {
				if ( HitCheck_Bullet_Box(m_HitCaps,g_Box[i].m_HitSide[j]) ) {
					m_ActionNo	= -1 ;
					m_UseFlg = FALSE ;
				}
			}
		}
	}

	return 0 ;
}

//==================================================================//
//		データセット												//
//------------------------------------------------------------------//
//		IN  :	参照するグローバルデータ,モード						//
//		OUT :	終了判定											//
//==================================================================//
int BulletObj::InitBul( BulletData* argp_gData, VECTOR* argp_Target, int arg_PlNo )
{
	mp_Target				= argp_Target ;										// --- ターゲットを設定
	m_PlayerNo				= arg_PlNo ;

	m_Type					= argp_gData->m_Type ;								// --- 種類
	m_hModel				= MV1DuplicateModel( g_hModel[argp_gData->m_gModelNo] ) ;	// --- モデル

	m_AnimationData.SetAnim( argp_gData->m_gAnimNo, 1 ) ;						// --- アニメーション
	m_AnimationData.AnimAttach( m_hModel, 0 ) ;
	m_AnimationData.m_AnimSpd		= 1.0f ;
	m_AnimationData.m_Loop			= TRUE ;

	m_HomingFlg				= TRUE ;
	m_HomingPower			= argp_gData->m_HomingPower ;						// --- ホーミング力
	m_HomingTime			= argp_gData->m_HomingTime ;						// --- ホーミング持続時間
	m_SpdAbs				= argp_gData->m_SpdAbs ;							// --- 速度絶対値
	m_DamegePower			= argp_gData->m_DamegePower ;						// --- 威力

	m_HitCaps.m_Param[0]	= m_Pos ;											// --- ヒットチェック座標

	m_ActionNo				= 1 ;												// --- アクションモード
	m_UseFlg				= TRUE ;											// --- 使用フラグをオン

	return 0 ;
}

//==================================================================//
//		初速セット													//
//------------------------------------------------------------------//
//		IN  :	角度補正値X,Y										//
//		OUT :	終了判定											//
//==================================================================//
int BulletObj::SetFirstSpeed( float arg_AngleOffset_Y )
{
	VECTOR	MoveVec ;			// --- ベクトルの向き
	MATRIX	DirectMat_Y ;		// --- 方向補正用マトリックス

	MoveVec = VNorm( VSub(*mp_Target,m_Pos) ) ;		// --- 相手への向きを取得

	DirectMat_Y = MGetRotY( EulerToRadian(arg_AngleOffset_Y) ) ;		// --- Yの回転行列を取得

	m_Spd = VScale( VTransformSR(MoveVec,DirectMat_Y), m_SpdAbs ) ;	// --- 方向に速度の絶対値を掛けてXYZ速度を設定

	LookMoveDir() ;

	return 0 ;
}

//==================================================================//
//		進行方向を向く												//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int BulletObj::LookMoveDir()
{
	VECTOR		VecXZ ;		// --- Y回転を取り除いた速度

	m_Rotate.y	= atan2( m_Spd.x, m_Spd.z ) + DX_PI_F ;							// --- XZ速度からY回転を取得
	
	VecXZ		= VTransformSR( m_Spd, MGetRotY(m_Rotate.y * -1.0f) ) ;			// --- 速度のベクトルからYの回転を取り除いたベクトルを取得

	m_Rotate.x	= atan2( VecXZ.z, VecXZ.y ) + DX_PI_F / 2.0f ;					// --- Yの回転を取り除いたベクトルの向きからX回転を設定

	return 0 ;
}

//==================================================================//
//		ホーミング													//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int BulletObj::Homing()
{
	VECTOR	ToTarget ;		// --- ターゲットの方向
	VECTOR	MoveDir ;		// --- 進行方向
	VECTOR	NewDir ;		// --- 新しい進行方向

	ToTarget	= VNorm( VSub(*mp_Target,m_Pos) ) ;		// --- ターゲットの方向を取得
	MoveDir		= VNorm( m_Spd ) ;						// --- 進行方向

	NewDir		= VAdd( VScale(ToTarget,m_HomingPower), VScale(MoveDir,1.0f-m_HomingPower) ) ;	// --- ホーミング力でブレンドする

	NewDir		= VNorm( NewDir ) ;				// --- 正規化する

	m_Spd = VScale( NewDir, m_SpdAbs ) ;		// --- 速度の絶対値を掛ける

	return 0 ;
}

//==================================================================//
//		威力の取得													//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	威力												//
//==================================================================//
float BulletObj::GetDamege()
{
	return m_DamegePower ;
}

//==================================================================//
//		ホーミングフラグセット										//
//------------------------------------------------------------------//
//		IN  :	ホーミングフラグ									//
//		OUT :	終了判定											//
//==================================================================//
int BulletObj::SetHoming( int arg_HomingFlg )
{
	m_HomingFlg = arg_HomingFlg ;

	return 0 ;
}
