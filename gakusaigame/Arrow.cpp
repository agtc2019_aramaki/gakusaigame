///////////////////////////////////////////////////////////////////////////////////////////
//																						//
//							Arrow.cpp													//
//																						//
//							作成日時 : 2020.11/08										//
//							作成者   : 荒巻伸悟											//
//																						//
//							矢印クラスのメソッド定義									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
#include <DxLib.h>
#include "Common.h"


//==============================================================//
//		アクション												//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int Arrow::Action(){

	DrawPos = g_Player[PlayerNo].GetSelecting() ;

	return 0 ;
}

//==============================================================//
//		初期セット												//
//--------------------------------------------------------------//
//		IN  : enumの番号										//
//		OUT : 格納したハンドル									//
//==============================================================//
int Arrow::Initialize( int arg_ArrowNo ){

	PlayerNo = arg_ArrowNo ;

	switch( arg_ArrowNo ){
		case 0:
			m_DrawRect.right = 80 ;
			m_DrawRect.bottom = 50 ;
			m_hGraph = g_hGraph[IMAGE_ARROW_NO] ;
			break ;

		case 1:
			m_DrawRect.right = 80 ;
			m_DrawRect.bottom = 50 ;
			m_hGraph = g_hGraph[IMAGE_ARROW_NO] ;
			break ;
	}
	return m_hGraph ;
}

//==============================================================//
//		描画													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int Arrow::Draw()
{
	if ( PlayerNo == 0 ) {
		DrawRectGraph(
			150,
			330 + (110 * DrawPos) ,
			0,0,
			m_DrawRect.right,
			m_DrawRect.bottom,
			m_hGraph,
			TRUE, FALSE, FALSE
			) ;
	}
	else {
		DrawRectGraph(
			WINDOW_SIZE_W - (150 + 80),
			330 + (110 * DrawPos) ,
			0,0,
			m_DrawRect.right,
			m_DrawRect.bottom,
			m_hGraph,
			TRUE, TRUE, FALSE
			) ;
	}



	return 0 ;
}


