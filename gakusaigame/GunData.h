//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							GunData.h															//
//																								//
//							作成日時 : 2020.10/7												//
//							作成者   : 天童律希													//
//																								//
//							銃データクラス定義													//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		define定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
#define GUN_DATA_MAXBUL					5			// --- 最大弾数
#define GUN_DATA_RELSPD					3			// --- リロード速度

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		enum定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
// --- 銃の種類
enum RefGunType {
	eGunSyuriken,		// --- 手裏剣
	eGunBeard,			// --- 鳥
	eGunBee,			// --- 蜂
	eGunFish,			// --- 魚
} ;

// --- 参照するデータ
enum RefGunData {
	eGunType,			// --- 種類
	eGunModel,			// --- モデルハンドル
} ;

// --- 関数ポインタ定義
typedef int (*GunShotTbl)( VECTOR arg_ShotPos, VECTOR* argp_Tgt, int argPlNo ) ;

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		クラス定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
// --- グローバルに保持する銃データ
class GunData : public EquipData {
	public :
		int			Activate( VECTOR* argp_Target, int arg_ActModel, int arg_FrameIdx, int arg_PlayerNo ) ;	// --- 発射
		int			GetData( int arg_eData ) ;																// --- データ参照
		int			InitializeData( int arg_Type ) ;														// --- データセット
		static int	SetActivateFunction() ;																	// --- 射撃関数ポインタを保存する

	private :
} ;

// --- 個別の射撃関数
int Shot_S( VECTOR arg_ShotPos, VECTOR* argp_Tgt, int argPlNo ) ;			// --- 手裏剣
int Shot_B( VECTOR arg_ShotPos, VECTOR* argp_Tgt, int argPlNo ) ;			// --- 蜂
int Shot_E( VECTOR arg_ShotPos, VECTOR* argp_Tgt, int argPlNo ) ;			// --- 鳥
int Shot_F( VECTOR arg_ShotPos, VECTOR* argp_Tgt, int argPlNo ) ;			// --- 魚

// --- // --- メンバーに持つ銃データ
class GunMember : public EquipMember {
	public :
		int			SetGrobalDeta( int arg_GunNo ) ;				// --- 参照するグローバルデータセット
		int			Shot( VECTOR* arg_Target, int arg_PlayerNo ) ;	// --- 発射
		int			ResetBul() ;									// --- 残弾リセット

	private :
		GunData*	mp_GrobalData ;									// --- 参照するグローバルデータポインタ

} ;

