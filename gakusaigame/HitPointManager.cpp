///////////////////////////////////////////////////////////////////////////////////////////
//																						 //
//							HitPointManager.cpp			 								 //
//																						 //
//							作成日時 : 2020.10/20										 //
//							作成者   : 荒巻伸悟											 //
//																						 //
//					ヒットポイントの画像や処理クラスのメソッド定義						 //
//																						 //
///////////////////////////////////////////////////////////////////////////////////////////
#include <DxLib.h>
#include "Common.h"

//==============================================================//
//		アクション												//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int HitPointManager::Action(){
	m_DispHp = g_Player[m_PlayerNo].m_HitPoint ;
	if ( (m_DispHp < m_HitBullet) && (g_LimitTime[0].AbsFlg = TRUE) ){
		m_HitBullet -= 0.4f ;
	}

	if ( m_DispHp > m_HitBullet ){
		m_HitBullet = m_DispHp ;
	}

	return 0 ;
}


//==============================================================//
//		初期セット+フレーム										//
//--------------------------------------------------------------//
//		IN  : enumの番号										//
//		OUT : 格納したハンドル									//
//==============================================================//
int HitPointManager::Initialize( int arg_HpNo ){

	m_PlayerNo = arg_HpNo ;
	m_HitBullet = PLAYER_PARAM_HP_MAX ;

	switch( arg_HpNo ){
		case eHit1PBgFrame:
			m_DrawRect.left   =			 10 ;		// --- 描画開始x座標
			m_DrawRect.top    =			 10 ;		// --- 描画開始y座標
			m_DrawRect.right  =			500 ;		// --- 画像のwidth
			m_DrawRect.bottom =			 90 ;		// --- 画像のheight

			m_CutPt.x		  =	   0 ;		// --- 切り取りx座標
			m_CutPt.y		  =	   0 ;		// --- 切り取りy座標

			m_hGraph = g_hGraph[IMAGE_REDHPFRAME_NO] ;
			break ;

		case eHit2PBgFrame:
			m_DrawRect.left   =		    620 ;		// --- 描画開始x座標
			m_DrawRect.top    =			 10 ;		// --- 描画開始y座標
			m_DrawRect.right  =			500 ;		// --- 画像のwidth
			m_DrawRect.bottom =			 90 ;		// --- 画像のheight

			m_CutPt.x		  =	   0 ;		// --- 切り取りx座標
			m_CutPt.y		  =	   0 ;		// --- 切り取りy座標

			m_hGraph = g_hGraph[IMAGE_BLUEHPFRAME_NO] ;
			break ;
	}
	return m_hGraph ;
}


//==============================================================//
//		1PのHPゲージ処理										//
//--------------------------------------------------------------//
//		IN  : enumの番号										//
//		OUT : 格納したハンドル									//
//==============================================================//
int HitPointManager::HpGaugeManager( int arg_plyHpNo )
{
	if ( arg_plyHpNo == ePlayer_1 )
	{
		m_HPDrawRect[0].left   =	      21 ;		// --- 描画開始x座標
		m_HPDrawRect[0].top    =		  54 ;		// --- 描画開始y座標
		m_HPDrawRect[0].right  = HPGAUGE_MAX ;		// --- 画像のwidth
		m_HPDrawRect[0].bottom =		  40 ;		// --- 画像のheight

		m_hGraph_Gauge[0] = g_hGraph[IMAGE_DAMAGE_HP01_NO] ;

		m_HPDrawRect[1].left   =		  21 ;		// --- 描画開始x座標
		m_HPDrawRect[1].top    =		  54 ;		// --- 描画開始y座標
		m_HPDrawRect[1].right  = HPGAUGE_MAX ;		// --- 画像のwidth
		m_HPDrawRect[1].bottom =	 	  40 ;		// --- 画像のheight

		m_hGraph_Gauge[1] = g_hGraph[IMAGE_HP01_NO] ;
	}else{
		m_HPDrawRect[0].left   =	     632 ;		// --- 描画開始x座標
		m_HPDrawRect[0].top    =		  54 ;		// --- 描画開始y座標
		m_HPDrawRect[0].right  = HPGAUGE_MAX ;		// --- 画像のwidth
		m_HPDrawRect[0].bottom =		  40 ;		// --- 画像のheight

		m_hGraph_Gauge[0] = g_hGraph[IMAGE_DAMAGE_HP02_NO] ;

		m_HPDrawRect[1].left   =	     632 ;		// --- 描画開始x座標
		m_HPDrawRect[1].top    =		  54 ;		// --- 描画開始y座標
		m_HPDrawRect[1].right  = HPGAUGE_MAX ;		// --- 画像のwidth
		m_HPDrawRect[1].bottom =		  40 ;		// --- 画像のheight

		m_hGraph_Gauge[1] = g_hGraph[IMAGE_HP02_NO] ;
	}
	return 0 ;
}



//==============================================================//
//		グラフィックハンドルをグローバル変数から切り取り複製	//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : ロードしたグラフィックハンドル					//
//==============================================================//
int HitPointManager::LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo ){
	m_hGraph = DerivationGraph(
		arg_CutRect->left,
		arg_CutRect->top,
		arg_CutRect->right,
		arg_CutRect->bottom,
		g_hGraph[arg_gGraphNo]
		) ;

	return m_hGraph ;
}



//==============================================================//
//		描画													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int HitPointManager::Draw() {

	float DispLength = m_DispHp ;
	float DamageLen = m_HitBullet ;

	DispLength /= PLAYER_PARAM_HP_MAX ;
	DispLength *= HPGAUGE_MAX ;

	DamageLen /= PLAYER_PARAM_HP_MAX ;
	DamageLen *= HPGAUGE_MAX ;

	// --- HPのフレームドロー
	DrawRectGraph(
		m_DrawRect.left,
		m_DrawRect.top,
		m_CutPt.x,
		m_CutPt.y,
		m_DrawRect.right,
		m_DrawRect.bottom,
		m_hGraph,
		TRUE, FALSE, FALSE
		) ;

	// --- m_hGraph_Gauge[0]が赤色のダメージバー
	// --- m_hGraph_Gauge[1]が黄色の体力バー
	if ( m_PlayerNo == ePlayer_1 ) {
		// --- 1HPのゲージドロー
		DrawRectGraph(
			(m_HPDrawRect[0].left + (HPGAUGE_MAX - (int)DamageLen)),
			m_HPDrawRect[0].top,
			(m_CutPt_HP[0].x + (HPGAUGE_MAX - (int)DamageLen)),
			m_CutPt_HP[0].y,
			(int)DamageLen,
			m_HPDrawRect[0].bottom,
			m_hGraph_Gauge[0],
			TRUE, FALSE
			) ;
		
		DrawRectGraph(
			(m_HPDrawRect[1].left + (HPGAUGE_MAX - (int)DispLength)),
			m_HPDrawRect[1].top,
			(m_CutPt_HP[1].x + (HPGAUGE_MAX - (int)DispLength)),
			m_CutPt_HP[1].y,
			(int)DispLength,
			m_HPDrawRect[1].bottom,
			m_hGraph_Gauge[1],
			TRUE, FALSE
			) ;
	}
	else {
		// --- 2HPのゲージドロー
		DrawRectGraph(
			m_HPDrawRect[0].left,
			m_HPDrawRect[0].top,
			m_CutPt_HP[0].x,
			m_CutPt_HP[0].y,
			(int)DamageLen,
			m_HPDrawRect[0].bottom,
			m_hGraph_Gauge[0],
			TRUE, FALSE
			) ;

		DrawRectGraph(
			m_HPDrawRect[1].left,
			m_HPDrawRect[1].top,
			m_CutPt_HP[1].x,
			m_CutPt_HP[1].y,
			(int)DispLength,
			m_HPDrawRect[1].bottom,
			m_hGraph_Gauge[1],
			TRUE, FALSE
			) ;
	}


	return 0 ;
}
