///////////////////////////////////////////////////////////////////////////////////////////
//																						//
//							GunBackGround.h												//
//																						//
//							作成日時 : 2020.10/14										//
//							作成者   : 荒巻伸悟											//
//																						//
//							銃背景オブジェクトのクラス									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

enum GunBgGHn {
	eGunBgFrame,
	eGunBgGun,
	eGunBgBullet
} ;

// --- 銃ポジション
enum GunBgNum {
	eGunBg1PlGunL,	// --- 左上
	eGunBg1PlGunR,	// --- 左中
	eGunBg1PlBack,	// --- 左下
	eGunBg2PlGunL,	// --- 右上
	eGunBg2PlGunR,	// --- 右中
	eGunBg2PlBack,	// --- 右下
	eGunBgSubBack	// --- もう一つの背中
} ;

class GunBackGround : public GameObject_2D{
	public :
		int Action() override ;		// --- アクション...使用するオブジェクトのクラスでオーバーライド
		int Draw() override ;		// --- 描画
		int Initialize( int arg_GunNo ) ;			// --- 背景画像の初期セット
		int SetActionNo( int arg_ActNo ) ;			// --- アクション番号のセット
		int	LoadCutGraph( int arg_gGraphNo , RECT* arg_CutRect , int arg_mGraphNo = NULL ) ;// --- グラフィックのロード
		int GunBackGraph( int arg_GnBkGp ) ;		// --- 背景画像の初期セット
		int ReloadManager( int arg_BulNo ) ;		// --- リロード管理
		int GunToGraph( int arg_GtoGNo ) ;			// --- 銃の2D画像
		int Getter() ;		// --- ゲッター

	private :
		int				m_PlayerNo ;		// --- プレイヤーの識別
		int				m_EquipNo ;			// --- 残弾数
		int				m_GunGraphNo ;		// --- 選択中の銃の識別
		RECT			m_DrawRectGun[3] ;	// --- 描画矩形
		POINT			m_CutPtNo[3] ;		// --- 切り取り矩形

		int				m_hGraph_Back ;		// --- 銃の背景ハンドル
		int				m_hGraph_Number ;	// --- 残弾数のハンドル
		int				m_hGraph_Wepon ;	// --- 武器関係のハンドル 
		int				m_GunReverse ;		// --- 武器の表示分け
		int				m_StorGnNo ;		// --- 残弾数の位置分けの変数
		int				m_ShotNumber ;		// --- 残弾数格納変数
		int				m_GunNumber ;		// --- 銃格納変数
} ;

