///////////////////////////////////////////////////////////////////////////////////////////
//																						//
//							Finish.h													//
//																						//
//							作成日時 : 2020.11/08										//
//							作成者   : 荒巻伸悟											//
//																						//
//							終了オブジェクトのクラス									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

class Finish : public GameObject_2D{
	public:
		int Action() override ;		// --- アクション...使用するオブジェクトのクラスでオーバーライド
		int Draw() override ;		// --- 描画
		int Initialize( int arg_FinishNo ) ;			// --- 背景画像の初期セット
		int SetActionNo( int arg_ActNo ) ;			// --- アクション番号のセット
		int	LoadCutGraph( int arg_gGraphNo , RECT* arg_CutRect , int arg_mGraphNo = NULL ) ;// --- グラフィックのロード
		int Getter() ;

		int PosiDec ;
		int SceneTimer ;		// --- シーンを変える時間を格納


} ;