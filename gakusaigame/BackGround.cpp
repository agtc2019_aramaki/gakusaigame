///////////////////////////////////////////////////////////////////////////////////////////
//																						//
//							BackGround.h												//
//																						//
//							作成日時 : 2020.10/13										//
//							作成者   : 荒巻伸悟											//
//																						//
//							背景クラスのメソッド定義									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
#include <DxLib.h>
#include "Common.h"

//==============================================================//
//		アクション												//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int BackGround::Action(){
	return 0 ;
}


//==============================================================//
//					ゲッター									//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int BackGround::Getter(){
	return 0 ;
}


//==============================================================//
//		初期セット												//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int BackGround::Initialize( int arg_BgInit ){

	ChangeBack = arg_BgInit ;

	m_DrawRect.left   =    0 ;		// --- 描画開始x座標
	m_DrawRect.top    =    0 ;		// --- 描画開始y座標
	m_DrawRect.right  = WINDOW_SIZE_W ;		// --- 画像のwidth
	m_DrawRect.bottom = WINDOW_SIZE_H ;		// --- 画像のheight

	m_CutPt.x		  =	   0 ;		// --- 切り取りx座標
	m_CutPt.y		  =	   0 ;		// --- 切り取りy座標

	if( ChangeBack == 0 ){
		m_hGraph = g_hGraph[IMAGE_TITLE_NO] ;
	}

	if( ChangeBack == 1 )
		m_hGraph = g_hGraph[IMAGE_SELECT_NO] ;

	if( ChangeBack == 2 )
		m_hGraph = g_hGraph[IMAGE_SKY_NO] ;

	if( ChangeBack == 3 )
		m_hGraph = g_hGraph[IMAGE_SKY_NO] ;

	return m_hGraph ;
}


//==============================================================//
//		グラフィックハンドルをグローバル変数から切り取り複製	//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : ロードしたグラフィックハンドル					//
//==============================================================//
int BackGround::LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo ){
	m_hGraph = DerivationGraph(
		arg_CutRect->left,
		arg_CutRect->top,
		arg_CutRect->right,
		arg_CutRect->bottom,
		g_hGraph[arg_gGraphNo]
		) ;

	return m_hGraph ;
}


//==============================================================//
//		描画													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int BackGround::Draw(){
	DrawRectGraph(
		m_DrawRect.left,
		m_DrawRect.top,
		m_CutPt.x,
		m_CutPt.y,
		m_DrawRect.right,
		m_DrawRect.bottom,
		m_hGraph,
		TRUE, FALSE, FALSE
		) ;

	return 0 ;
}










