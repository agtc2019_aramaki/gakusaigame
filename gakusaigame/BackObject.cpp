/*	======================================================================================================================

										BackObject.cpp
										作成日時 : 2020/10/27
										作成者	 : 加藤諒人

										box関連

	====================================================================================================================*/
#include <DxLib.h>
#include "Common.h"

/*	Action		[ IN => void   /   OUT => 終了判定 ]
======================================================*/
int BackObject::Action()
{
	return 0 ;
}

/*	Initialize(初期化)
===============================================================================*/
int BackObject::Initialize( int arg_BackObjNo )
{
	m_hModel = LoadModel(MODEL_OBJECT_NO) ;	// --- モデルを読み込む
	switch(arg_BackObjNo){
		case 0 :	// --- 中心
			m_Rotate = VGet(0.0f,0.0f,0.0f) ;		// --- モデルの回転
			m_Pos = VGet(-400.0f,-200.0f,300.0f) ;	// --- モデルの位置
			m_Size	 = VGet(80.0f,80.0f,80.0f) ;		// --- モデルのサイズ
			break ;
		case 1:		// --- 右上
			m_Rotate = VGet(0.0f,110.0f,0.0f) ;		// --- モデルの回転
			m_Pos	 = VGet(400.0f,-200.0f,300.0f) ;	// --- モデルの位置
			m_Size	 = VGet(80.0f,80.0f,80.0f) ;		// --- モデルのサイズ
			break ;
	}
	m_UseFlg = TRUE ;
	return 0 ;
}

/*	Draw	
========================================================================*/
int BackObject::Draw()
{
	MV1SetPosition		( m_hModel, m_Pos ) ;		// --- 座標セット
	MV1SetRotationXYZ	( m_hModel,	m_Rotate ) ;	// --- 回転セット
	MV1SetScale			( m_hModel, m_Size ) ;		// --- 拡縮セット

	MV1DrawModel		( m_hModel ) ;				// --- 描画

	return 0 ;
}
