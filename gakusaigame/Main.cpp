//////////////////////////////////////////////////////////////////////
//																	//
//							2020.9/16								//
//																	//
//							学園祭ゲーム							//
//																	//
//							荒巻,加藤,天童							//
//																	//
//////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include"Common.h"

int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int)
{
	// --- ウィンドウモードの切り替え
	ChangeWindowMode(SCREEN_MODE) ;

	// --- ウィンドウサイズの変更
	SetGraphMode(WINDOW_SIZE_W,WINDOW_SIZE_H,COLOR_BIT) ;

	// --- 裏画面の描画の設定
	SetDrawScreen(DX_SCREEN_BACK) ;

	// --- DXライブラリの初期化
	if(DxLib_Init() == -1) return -1 ;

//	ChangeLightTypeDir( VGet(1.0f,-1.0f,1.0f) ) ;
//	SetUseLighting( FALSE ) ;

	LoadAllData() ;		// --- データロード
//	InitializeAll() ;	// --- 初期化

	while(ProcessMessage() == 0 && CheckHitKey(KEY_INPUT_ESCAPE) == 0){

		SceneLoop() ;
		DrawLoop() ;			// --- 描画ループ

	}

	// --- DXライブラリの終了
	DxLib_End() ;

	return 0 ;
}







/* [EOF] */