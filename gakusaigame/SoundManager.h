//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							SoundManager.h														//
//																								//
//							作成日時 : 2020.10/02												//
//							作成者   : 天童律希													//
//																								//
//							BGM,SEファイル管理クラス											//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		define定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
#define SOUND_MAX			32			// --- サウンド最大数

#define SOUND_TITLEBGM_NO			0		// --- タイトルBGM
#define SOUND_CUSTOMBGM_NO			1		// --- カスタマイズBGM
#define SOUND_BATTLEBGM_NO			2		// --- バトルBGM
#define SOUND_SHOTSE_NO				3		// --- 銃声
#define SOUND_DASHSE_NO				4		// --- ダッシュ
#define SOUND_MISSILESE_NO			5		// --- ミサイル
#define SOUND_CANNONSE_NO			6		// --- 大砲
#define SOUND_WINGSE_NO				7		// --- ウィング
#define SOUND_DRUMROLL_NO			8		// --- ドラムロール
#define SOUND_RESURTSE_NO			9		// --- 発表
#define SOUND_CURSORMOVE_NO			10		// --- カーソル移動
#define SOUND_CURSORCK_NO			11		// --- カーソル確定
#define SOUND_JUMP_NO				12		// --- ジャンプ
#define SOUND_RANDINGSE_NO			13		// --- 着地
#define SOUND_DAMEGESE_NO			14		// --- 着弾

#define SOUND_TITLEBGM_PATH			"Data\\Sound\\bgm_Title.mp3"		// --- タイトルBGM
#define SOUND_CUSTOMBGM_PATH		"Data\\Sound\\bgm_select.mp3"		// --- カスタマイズBGM
#define SOUND_BATTLEBGM_PATH		"Data\\Sound\\bgm_Battle.mp3"		// --- バトルBGM
#define SOUND_SHOTSE_PATH			"Data\\Sound\\se_gun04.mp3"		// --- 銃声
#define SOUND_DASHSE_PATH			"Data\\Sound\\se_dash.mp3"		// --- ダッシュ
#define SOUND_MISSILESE_PATH		"Data\\Sound\\se_explosion1.mp3"		// --- ミサイル
#define SOUND_CANNONSE_PATH			"Data\\Sound\\.mp3"		// --- 大砲
#define SOUND_WINGSE_PATH			"Data\\Sound\\se_wing.mp3"		// --- ウィング
#define SOUND_DRUMROLL_PATH			"Data\\Sound\\se_resultroll.mp3"		// --- ドラムロール
#define SOUND_RESURTSE_PATH			"Data\\Sound\\se_resultDen.mp3"		// --- 発表
#define SOUND_CURSORMOVE_PATH		"Data\\Sound\\se_select3.mp3"		// --- カーソル移動
#define SOUND_CURSORCK_PATH			"Data\\Sound\\se_select2.mp3"		// --- カーソル確定
#define SOUND_JUMP_PATH				"Data\\Sound\\se_jump.mp3"		// --- ジャンプ
#define SOUND_RANDINGSE_PATH		"Data\\Sound\\se_landing.mp3"		// --- 着地
#define SOUND_DAMEGESE_PATH			"Data\\Sound\\se_damage.mp3"		// --- 着弾

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		enum定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		クラス定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
typedef struct {
	int	m_Type ;		// --- BGMかSEか
	int m_hSound ;		// --- サウンドハンドル
} SoundData ;

class SoundManager {
	public :
		int			Play			( int arg_SoundNo ) ;		// --- 再生
		int			Check			( int arg_SoundNo ) ;		// --- 再生中かどうか
		int			Stop			( int arg_SoundNo ) ;		// --- 停止
		int			AllStop			() ;						// --- 全てのサウンドの停止
		int			LoadData		() ;		// --- データロード


	private :
		SoundData	m_Sound[SOUND_MAX] ;		// --- サウンドデータ
} ;
