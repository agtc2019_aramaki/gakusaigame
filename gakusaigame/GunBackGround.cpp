///////////////////////////////////////////////////////////////////////////////////////////
//																						//
//							GunBackGround.cpp											//
//																						//
//							作成日時 : 2020.10/14										//
//							作成者   : 荒巻伸悟											//
//																						//
//							銃背景クラスのメソッド定義									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
#include <DxLib.h>
#include "Common.h"

//==============================================================//
//		アクション												//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int GunBackGround::Action(){

	Getter() ;

	return 0 ;
}

//==============================================================//
//		アクション												//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int GunBackGround::SetActionNo( int arg_ActNo )
{
	m_ActionNo	= arg_ActNo ;
	m_UseFlg	= TRUE ;

	return 0 ;
}

//==============================================================//
//					ゲッター									//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int GunBackGround::Getter(){
	// --- プレイヤーの残弾数を格納
	m_ShotNumber = g_Player[m_PlayerNo].GetEquipNowBul( m_EquipNo ) ;
	// --- 現在の銃を格納
	m_GunNumber = g_Player[m_PlayerNo].GetEquipType( m_EquipNo ) ;
	return 0 ;
}


//==============================================================//
//		初期セット												//
//--------------------------------------------------------------//
//		IN  : enumの番号										//
//		OUT : 格納したハンドル									//
//==============================================================//
int GunBackGround::Initialize( int arg_GunNo ){

	m_PlayerNo = arg_GunNo / 3 ;
	m_EquipNo = arg_GunNo % 3 ;

	// --- 左側のポジ決め
	if ( arg_GunNo > 2 ){
		m_GunReverse = 1015 ;
		m_StorGnNo = 960 ;
	}else{
		// --- 右側のポジ決め
		m_GunReverse = 20 ;
		m_StorGnNo = 130 ;
	} 

	switch( arg_GunNo ){
		// --- 銃の1P上背景
		case eGunBg1PlGunL:
			m_DrawRectGun[0].left   =    0 ;		// --- 描画開始x座標
			m_DrawRectGun[0].top    =  310 ;		// --- 描画開始y座標
			m_DrawRectGun[0].right  =  230 ;		// --- 画像のwidth
			m_DrawRectGun[0].bottom =   90 ;		// --- 画像のheight

			m_CutPt.x		  =	   0 ;		// --- 切り取りx座標
			m_CutPt.y		  =	   0 ;		// --- 切り取りy座標

			m_hGraph = g_hGraph[IMAGE_BB_NO] ;
			break ;

		// --- 銃の1P中背景
		case eGunBg1PlGunR:
			m_DrawRectGun[0].left   =    0 ;		// --- 描画開始x座標
			m_DrawRectGun[0].top    =  420 ;		// --- 描画開始y座標
			m_DrawRectGun[0].right  =  230 ;		// --- 画像のwidth
			m_DrawRectGun[0].bottom =   90 ;		// --- 画像のheight

			m_CutPt.x		  =	   0 ;		// --- 切り取りx座標
			m_CutPt.y		  =	   0 ;		// --- 切り取りy座標

			m_hGraph = g_hGraph[IMAGE_BB_NO] ;
			break ;

		// --- 銃の1P下背景
		case eGunBg1PlBack:
			m_DrawRectGun[0].left   =    0 ;		// --- 描画開始x座標
			m_DrawRectGun[0].top    =  530 ;		// --- 描画開始y座標
			m_DrawRectGun[0].right  =  230 ;		// --- 画像のwidth
			m_DrawRectGun[0].bottom =   90 ;		// --- 画像のheight

			m_CutPt.x		  =	   0 ;		// --- 切り取りx座標
			m_CutPt.y		  =	   0 ;		// --- 切り取りy座標

			m_hGraph = g_hGraph[IMAGE_BB_NO] ;
			break ;

		// --- 銃の2P上背景
		case eGunBg2PlGunL:
			m_DrawRectGun[0].left   =  890 ;		// --- 描画開始x座標
			m_DrawRectGun[0].top    =  310 ;		// --- 描画開始y座標
			m_DrawRectGun[0].right  =  230 ;		// --- 画像のwidth
			m_DrawRectGun[0].bottom =   90 ;		// --- 画像のheight

			m_CutPt.x		  =	   0 ;		// --- 切り取りx座標
			m_CutPt.y		  =	   0 ;		// --- 切り取りy座標

			m_hGraph = g_hGraph[IMAGE_BB_NO] ;
			break ;

		// --- 銃の2P中背景
		case eGunBg2PlGunR:
			m_DrawRectGun[0].left   =  890 ;		// --- 描画開始x座標
			m_DrawRectGun[0].top    =  420 ;		// --- 描画開始y座標
			m_DrawRectGun[0].right  =  230 ;		// --- 画像のwidth
			m_DrawRectGun[0].bottom =   90 ;		// --- 画像のheight

			m_CutPt.x		  =	   0 ;		// --- 切り取りx座標
			m_CutPt.y		  =	   0 ;		// --- 切り取りy座標

			m_hGraph = g_hGraph[IMAGE_BB_NO] ;
			break ;

		// --- 銃の2P下背景
		case eGunBg2PlBack:
			m_DrawRectGun[0].left   =  890 ;		// --- 描画開始x座標
			m_DrawRectGun[0].top    =  530 ;		// --- 描画開始y座標
			m_DrawRectGun[0].right  =  230 ;		// --- 画像のwidth
			m_DrawRectGun[0].bottom =   90 ;		// --- 画像のheight

			m_CutPt.x		  =	   0 ;		// --- 切り取りx座標
			m_CutPt.y		  =	   0 ;		// --- 切り取りy座標

			m_hGraph = g_hGraph[IMAGE_BB_NO] ;
			break ;
	}

	return m_hGraph ;
}


//==============================================================//
//				リロードの処理(残弾数)							//
//--------------------------------------------------------------//
//		IN  : enumの番号										//
//		OUT : 格納したハンドル									//
//==============================================================//
int GunBackGround::ReloadManager( int arg_BulNo ){
	
	switch( arg_BulNo ){
		case 0:
			m_DrawRectGun[1].left   = m_StorGnNo ;		// --- 描画開始x座標
			m_DrawRectGun[1].top    = 330 ;		// --- 描画開始y座標
			m_DrawRectGun[1].right  =  40 ;		// --- 画像のwidth
			m_DrawRectGun[1].bottom = 450 ;		// --- 画像のheight

			m_hGraph_Number = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case 1:
			m_DrawRectGun[1].left   =  m_StorGnNo ;		// --- 描画開始x座標
			m_DrawRectGun[1].top    =  440 ;		// --- 描画開始y座標
			m_DrawRectGun[1].right  =   40 ;		// --- 画像のwidth
			m_DrawRectGun[1].bottom =  450 ;		// --- 画像のheight

			m_hGraph_Number = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case 2:
			m_DrawRectGun[1].left   =  m_StorGnNo ;		// --- 描画開始x座標
			m_DrawRectGun[1].top    =  550 ;		// --- 描画開始y座標
			m_DrawRectGun[1].right  =   40 ;		// --- 画像のwidth
			m_DrawRectGun[1].bottom =  450 ;		// --- 画像のheight

			m_hGraph_Number = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case 3:
			m_DrawRectGun[1].left   =  m_StorGnNo ;		// --- 描画開始x座標
			m_DrawRectGun[1].top    =  330 ;		// --- 描画開始y座標
			m_DrawRectGun[1].right  =   40 ;		// --- 画像のwidth
			m_DrawRectGun[1].bottom =  450 ;		// --- 画像のheight

			m_hGraph_Number = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case 4:
			m_DrawRectGun[1].left   =  m_StorGnNo ;		// --- 描画開始x座標
			m_DrawRectGun[1].top    =  440 ;		// --- 描画開始y座標
			m_DrawRectGun[1].right  =   40 ;		// --- 画像のwidth
			m_DrawRectGun[1].bottom =  450 ;		// --- 画像のheight

			m_hGraph_Number = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case 5:
			m_DrawRectGun[1].left   =  m_StorGnNo ;		// --- 描画開始x座標
			m_DrawRectGun[1].top    =  550 ;		// --- 描画開始y座標
			m_DrawRectGun[1].right  =   40 ;		// --- 画像のwidth
			m_DrawRectGun[1].bottom =  450 ;		// --- 画像のheight

			m_hGraph_Number = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;
	}

	return m_hGraph_Number ;
}


//==============================================================//
//				銃のモデルの2D画像の表示						//
//--------------------------------------------------------------//
//		IN  : enumの番号										//
//		OUT : 格納したハンドル									//
//==============================================================//
int GunBackGround::GunToGraph( int arg_GtoGNo ){

	switch( arg_GtoGNo ){
		// --- 手裏剣
		case eGunBg1PlGunL:
			m_DrawRectGun[2].left   = m_GunReverse ;	// --- 描画開始x座標
			m_DrawRectGun[2].top    = 320 ;		// --- 描画開始y座標
			m_DrawRectGun[2].right  = GUN_MAX_X ;		// --- 画像のwidth
			m_DrawRectGun[2].bottom = GUN_MAX_Y ;		// --- 画像のheight

			m_CutPtNo[1].x		  =	  GUN_MAX_X ;		// --- 切り取りx座標
			m_CutPtNo[1].y		  =   GUN_MAX_Y ;		// --- 切り取りy座標

			m_hGraph_Wepon = g_hGraph[IMAGE_WEPON_NO] ;	// --- モデルハンドル番号
			break ;

		// --- 鳥
		case eGunBg1PlGunR:
			m_DrawRectGun[2].left   = m_GunReverse ;	// --- 描画開始x座標
			m_DrawRectGun[2].top    = 425 ;		// --- 描画開始y座標
			m_DrawRectGun[2].right  = GUN_MAX_X ;		// --- 画像のwidth
			m_DrawRectGun[2].bottom = GUN_MAX_Y ;		// --- 画像のheight

			m_CutPtNo[1].x		  =   GUN_MAX_X ;		// --- 切り取りx座標
			m_CutPtNo[1].y		  =   GUN_MAX_Y ;		// --- 切り取りy座標

			m_hGraph_Wepon = g_hGraph[IMAGE_WEPON_NO] ;	// --- モデルハンドル番号
			break ;

		// --- 蜂
		case eGunBg2PlGunL:
			m_DrawRectGun[2].left   = m_GunReverse ;	// --- 描画開始x座標
			m_DrawRectGun[2].top    = 320 ;		// --- 描画開始y座標
			m_DrawRectGun[2].right  = GUN_MAX_X ;		// --- 画像のwidth
			m_DrawRectGun[2].bottom = GUN_MAX_Y ;		// --- 画像のheight

			m_CutPtNo[1].x		  =   GUN_MAX_X ;		// --- 切り取りx座標
			m_CutPtNo[1].y		  =   GUN_MAX_Y ;		// --- 切り取りy座標

			m_hGraph_Wepon = g_hGraph[IMAGE_WEPON_NO] ;	// --- モデルハンドル番号
			break ;

		// --- 船
		case eGunBg2PlGunR:
			m_DrawRectGun[2].left   = m_GunReverse ;	// --- 描画開始x座標
			m_DrawRectGun[2].top    = 425 ;		// --- 描画開始y座標
			m_DrawRectGun[2].right  = GUN_MAX_X ;		// --- 画像のwidth
			m_DrawRectGun[2].bottom = GUN_MAX_Y ;		// --- 画像のheight

			m_CutPtNo[1].x		  =  GUN_MAX_X ;		// --- 切り取りx座標
			m_CutPtNo[1].y		  =  GUN_MAX_Y ;		// --- 切り取りy座標

			m_hGraph_Wepon = g_hGraph[IMAGE_WEPON_NO] ;	// --- モデルハンドル番号
			break ;

		// --- 翼
		case eGunBg1PlBack:
			m_DrawRectGun[2].left   = m_GunReverse ;		// --- 描画開始x座標
			m_DrawRectGun[2].top    = 540 ;		// --- 描画開始y座標
			m_DrawRectGun[2].right  = GUN_MAX_X ;		// --- 画像のwidth
			m_DrawRectGun[2].bottom = GUN_MAX_Y ;		// --- 画像のheight

			m_CutPtNo[2].x		  =   GUN_MAX_X ;		// --- 切り取りx座標
			m_CutPtNo[2].y		  =  75 ;		// --- 切り取りy座標

			m_hGraph_Wepon = g_hGraph[IMAGE_WEPON_NO] ;	// --- モデルハンドル番号
			break ;

		// --- 大砲
		case eGunBg2PlBack:
			m_DrawRectGun[2].left   = m_GunReverse ;	// --- 描画開始x座標
			m_DrawRectGun[2].top    = 540 ;				// --- 描画開始y座標
			m_DrawRectGun[2].right  = GUN_MAX_X ;		// --- 画像のwidth
			m_DrawRectGun[2].bottom = GUN_MAX_Y ;		// --- 画像のheight

			m_CutPtNo[2].x		  =   GUN_MAX_X ;		// --- 切り取りx座標
			m_CutPtNo[2].y		  =   GUN_MAX_Y ;		// --- 切り取りy座標

			m_hGraph_Wepon = g_hGraph[IMAGE_WEPON_NO] ;	// --- モデルハンドル番号
			break ;

		// --- ミサイル
		case eGunBgSubBack:
			m_DrawRectGun[2].left   = m_GunReverse ;	// --- 描画開始x座標
			m_DrawRectGun[2].top    = 540 ;				// --- 描画開始y座標
			m_DrawRectGun[2].right  = GUN_MAX_X ;		// --- 画像のwidth
			m_DrawRectGun[2].bottom = GUN_MAX_Y ;		// --- 画像のheight

			m_CutPtNo[2].x		  =   GUN_MAX_X ;		// --- 切り取りx座標
			m_CutPtNo[2].y		  =   GUN_MAX_Y ;		// --- 切り取りy座標

			m_hGraph_Wepon = g_hGraph[IMAGE_WEPON_NO] ;	// --- モデルハンドル番号
			break ;	
	}

	return m_hGraph_Wepon ;
}


//==============================================================//
//		グラフィックハンドルをグローバル変数から切り取り複製	//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : ロードしたグラフィックハンドル					//
//==============================================================//
int GunBackGround::LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo ){
	m_hGraph = DerivationGraph(
		arg_CutRect->left,
		arg_CutRect->top,
		arg_CutRect->right,
		arg_CutRect->bottom,
		g_hGraph[arg_gGraphNo]
		) ;

	return m_hGraph ;
}


//==============================================================//
//		描画													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int GunBackGround::Draw()
{
	if ( m_PlayerNo == ePlayer_1 ) {
		// --- 銃の背景のドロー
		DrawRectGraph(
			m_DrawRectGun[0].left,
			m_DrawRectGun[0].top,
			m_CutPt.x,
			m_CutPt.y,
			m_DrawRectGun[0].right,
			m_DrawRectGun[0].bottom,
			m_hGraph,
			TRUE, FALSE
			) ;	

		DrawRectGraph(
			m_DrawRectGun[2].left,	// --- 描画開始位置x座標
			m_DrawRectGun[2].top,	// --- 描画開始位置y座標
			GUN_MAX_X * (m_GunNumber),
			m_CutPtNo[2].y,
			m_DrawRectGun[2].right,	// --- 描画開始位置x座標
			m_DrawRectGun[2].bottom,	// --- 描画開始位置y座標
			m_hGraph_Wepon,			// --- 描画するハンドル
			TRUE,TRUE					// --- 透明にするか
			) ;

	}
	else {		// --- 2P
		// --- 反転を勝手にする関数
		DrawRectGraph(
			m_DrawRectGun[0].left,	// --- 描画開始位置x座標
			m_DrawRectGun[0].top,	// --- 描画開始位置y座標
			m_CutPt.x,
			m_CutPt.y,
			m_DrawRectGun[0].right,
			m_DrawRectGun[0].bottom,
			m_hGraph,				// --- 描画するハンドル
			TRUE,TRUE					// --- 透明にするか
			) ;

		// --- 銃のドロー
		DrawRectGraph(
			m_DrawRectGun[2].left,
			m_DrawRectGun[2].top,
			GUN_MAX_X * (m_GunNumber),
			m_CutPtNo[2].y,
			m_DrawRectGun[2].right,
			m_DrawRectGun[2].bottom,
			m_hGraph_Wepon,
			TRUE, FALSE
			) ;
	}

	if ( m_ActionNo != 0 ) {
		// --- 数字のドロー
		DrawRectGraph(
			m_StorGnNo,
			m_DrawRectGun[1].top,
			40 * (m_ShotNumber),
			0,
			m_DrawRectGun[1].right,
			m_DrawRectGun[1].bottom,
			m_hGraph_Number,
			TRUE, FALSE
			) ;
	}

	return 0 ;
}

