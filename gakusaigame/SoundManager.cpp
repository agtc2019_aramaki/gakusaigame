//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							SoundManager.h														//
//																								//
//							作成日時 : 2020.10/02												//
//							作成者   : 天童律希													//
//																								//
//							BGM,SEファイル管理クラス											//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include "Common.h"

//==================================================================//
//						データのロード								//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int SoundManager::LoadData()
{
	m_Sound[SOUND_TITLEBGM_NO].m_hSound = LoadSoundMem( SOUND_TITLEBGM_PATH ) ;
	m_Sound[SOUND_TITLEBGM_NO].m_Type= DX_PLAYTYPE_LOOP ;

	m_Sound[SOUND_CUSTOMBGM_NO].m_hSound = LoadSoundMem( SOUND_CUSTOMBGM_PATH ) ;
	m_Sound[SOUND_CUSTOMBGM_NO].m_Type= DX_PLAYTYPE_LOOP ;

	m_Sound[SOUND_BATTLEBGM_NO].m_hSound = LoadSoundMem( SOUND_BATTLEBGM_PATH ) ;
	m_Sound[SOUND_BATTLEBGM_NO].m_Type= DX_PLAYTYPE_LOOP ;

	m_Sound[SOUND_SHOTSE_NO].m_hSound = LoadSoundMem( SOUND_SHOTSE_PATH ) ;
	m_Sound[SOUND_SHOTSE_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_DASHSE_NO].m_hSound = LoadSoundMem( SOUND_DASHSE_PATH ) ;
	m_Sound[SOUND_DASHSE_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_MISSILESE_NO].m_hSound = LoadSoundMem( SOUND_MISSILESE_PATH ) ;
	m_Sound[SOUND_MISSILESE_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_CANNONSE_NO].m_hSound = LoadSoundMem( SOUND_CANNONSE_PATH ) ;
	m_Sound[SOUND_CANNONSE_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_WINGSE_NO].m_hSound = LoadSoundMem( SOUND_WINGSE_PATH ) ;
	m_Sound[SOUND_WINGSE_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_DRUMROLL_NO].m_hSound = LoadSoundMem( SOUND_DRUMROLL_PATH ) ;
	m_Sound[SOUND_DRUMROLL_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_RESURTSE_NO].m_hSound = LoadSoundMem( SOUND_RESURTSE_PATH ) ;
	m_Sound[SOUND_RESURTSE_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_CURSORMOVE_NO].m_hSound = LoadSoundMem( SOUND_CURSORMOVE_PATH ) ;
	m_Sound[SOUND_CURSORMOVE_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_CURSORCK_NO].m_hSound = LoadSoundMem( SOUND_CURSORCK_PATH ) ;
	m_Sound[SOUND_CURSORCK_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_JUMP_NO].m_hSound = LoadSoundMem( SOUND_JUMP_PATH ) ;
	m_Sound[SOUND_JUMP_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_RANDINGSE_NO].m_hSound = LoadSoundMem( SOUND_RANDINGSE_PATH ) ;
	m_Sound[SOUND_RANDINGSE_NO].m_Type= DX_PLAYTYPE_BACK ;

	m_Sound[SOUND_DAMEGESE_NO].m_hSound = LoadSoundMem( SOUND_DAMEGESE_PATH ) ;
	m_Sound[SOUND_DAMEGESE_NO].m_Type= DX_PLAYTYPE_BACK ;

	return 0 ;
}

//==================================================================//
//						再生										//
//------------------------------------------------------------------//
//		IN  :	サウンド番号										//
//		OUT :	終了判定											//
//==================================================================//
int SoundManager::Play( int arg_SoundNo )
{
	PlaySoundMem( m_Sound[arg_SoundNo].m_hSound, m_Sound[arg_SoundNo].m_Type ) ;

	return 0 ;
}

//==================================================================//
//						再生中かどうか								//
//------------------------------------------------------------------//
//		IN  :	サウンド番号										//
//		OUT :	判定												//
//==================================================================//
int SoundManager::Check( int arg_SoundNo )
{
	return CheckSoundMem( m_Sound[arg_SoundNo].m_hSound ) ;
}

//==================================================================//
//						停止										//
//------------------------------------------------------------------//
//		IN  :	サウンド番号										//
//		OUT :	終了判定											//
//==================================================================//
int SoundManager::Stop( int arg_SoundNo )
{
	StopSoundMem( m_Sound[arg_SoundNo].m_hSound ) ;

	return 0 ;
}

//==================================================================//
//						全てのサウンドの停止						//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int SoundManager::AllStop()
{
	for ( int i = 0 ; i < SOUND_MAX ; i++ ) {
		StopSoundMem( m_Sound[i].m_hSound ) ;
	}

	return 0 ;
}
