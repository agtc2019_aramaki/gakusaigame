//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							Player.h															//
//																								//
//							作成日時 : 2020.10/02												//
//							作成者   : 天童律希													//
//																								//
//					アニメーションを持った3Dオブジェクト(プレイヤー)							//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		define定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
// --- フラグ系 -------------------------------------------------------------------------------------- //
#define PLAYER_FLG_EQCONFIRM					0x00000001					// --- 装備確定フラグ
#define PLAYER_FLG_DASH							0x00000002					// --- ダッシュフラグ
#define PLAYER_FLG_INVINCIBLE					0x00000004					// --- 無敵フラグ
#define PLAYER_FLG_USESHADOW					0x00000008					// --- 影使用フラグ
#define PLAYER_FLG_GETKEY						0x00000010					// --- キー受付フラグ

#define PLAYER_FLG_FAILURE						0xffffffff					// --- 失敗

// --- アニメーション系 ------------------------------------------------------------------------------ //
#define PLAYER_ANIM_NUMBER						16							// --- プレイヤーアニメーションの数	
#define PLAYER_ANIM_PARAM_SHOTOFS				4							// --- 射撃アニメーションオフセット

// --- 移動方向のキー情報 -------------------------------------------------------------------------------------- //
#define PLAYER_MOVE_UP							0x01						// --- 上
#define PLAYER_MOVE_DOWN						0x02						// --- 下
#define PLAYER_MOVE_LEFT						0x04						// --- 左
#define PLAYER_MOVE_RIGHT						0x08						// --- 右

#define PLAYER_MOVE_DIRECTION					DX_PI_F / 4.0f				// --- 移動方向の差

// --- パラメータ系 ---------------------------------------------------------------------------------------------- //
#define PLAYER_PARAM_JUMPSPD					5.0f						// --- ジャンプ初速
#define PLAYER_PARAM_JUMPGRAV					0.2f						// --- 重力加速度
#define PLAYER_PARAM_MOVEAXL					0.4f						// --- 移動加速度
#define	PLAYER_PARAM_MOVESPD_MIN				1.0f						// --- 移動初速
#define	PLAYER_PARAM_MOVESPD_MAX				5.0f						// --- 移動最高速度
#define PLAYER_PARAM_SHOTGRAV					0.05f						// --- 射撃中の重力加速度
#define PLAYER_PARAM_HP_MAX						100.0f						// --- 体力上限
#define PLAYER_PARAM_AIRAXL						0.1f						// --- 空中制御の加速度
#define PLAYER_PARAM_DASHSPD					10.0f						// --- ダッシュ速度
#define PLAYER_PARAM_DASHSPD_Y					0.2f						// --- ダッシュ速度

// --- ヒットチェック用データ ------------------------------------------------------------ //
#define PLAYER_HITCHECK_HIGHT					50.0f						// --- 身長
#define PLAYER_HITCHECK_WIDTH					15.0f						// --- 半径

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		enum定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
// --- プレイヤー番号
enum PlNumber {
	ePlayer_1,		// --- 1P
	ePlayer_2		// --- 2P
} ;

// --- アクション管理番号
enum PlActNo {
	ePlDebug = -1,	// --- デバッグ用
	ePlInit,		// --- 初期セット
	ePlStay,		// --- 静止
	ePlMove,		// --- 移動
	ePlStop,		// --- 停止
	ePlJump,		// --- ジャンプ
	ePlFall,		// --- 降下
	ePlLanding,		// --- 着地硬直
	ePlDash,		// --- ダッシュ
	ePlShot,		// --- 射撃
	ePlBackActiv,	// --- 背中装備展開
	ePlDie,			// --- 死
	ePlRespawn,		// --- 復帰
	ePlCustEq		// --- 武器カスタマイズ
} ;

// --- アニメーション番号
enum PlAnimNo {
	ePlAnimJp,		// --- ジャンプ
	ePlAnimDs,		// --- ダッシュ
	ePlAnimDm,		// --- ダメージ
	ePlAnimHn,		// --- 変形
	ePlAnimLsh,		// --- 射撃L
	ePlAnimRsh,		// --- 射撃R
	ePlAnimStt,		// --- 棒立ち
	ePlAnimMv,		// --- 移動
	ePlAnimSty,		// --- 静止
	ePlAnimFall,	// --- 降下
	ePlAnimLanding,	// --- 着地硬直
	ePlAnimStop,	// --- 停止
	ePlAnimCn,		// --- 大砲
	ePlAnimDie,		// --- 死
} ;

// --- 銃左右
enum PlGunAndBack {
	ePlGunR,	// --- 右
	ePlGunL,	// --- 左
	ePlBack		// --- 背面ユニット
} ;

// --- キー情報
enum PlKeyState {
	ePlKeyTrg,		// --- トリガー
	ePlKeyData,		// --- データ
	ePlKeyOld		// --- 1フレーム前
} ;

// --- 各種フラグ
enum PlGetFlg {
	ePlConfEq,		// --- 装備確定フラグ
} ;

// --- 移動方向
enum PlDirection {
	ePlNoMove,		// --- 移動なし
	ePlLeftDown,	// --- 左手前
	ePlLeft,		// --- 左
	ePlLeftUp,		// --- 左奥
	ePlUp,			// --- 奥
	ePlRightUp,		// --- 右奥
	ePlRight,		// --- 右
	ePlRightDown,	// --- 右手前
	ePlDown			// --- 手前
} ;

// --- バトル中トリガーチェック
enum PlBattleTrg {
	ePlTrgNon,			// --- 入力なし
	ePlTrgXButton,		// --- △ボタン
	ePlTrgAButton,		// --- 〇ボタン
	ePlTrgBButton,		// --- ×ボタン
	ePlTrgR1Button,		// --- R1ボタン
	ePlTrgL1Button		// --- L1ボタン
} ;

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		クラス定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
class Player : public Character {
	public :
		int		Action				() override ;					// --- アクション
		int		Draw				() override ;					// --- 描画
		int		InitializeData		() ;							// --- 全体初期セット
		int		InitializeBattle	() ;							// --- バトルシーン初期セット
		int		InitializeCustom	() ;							// --- カスタマイズシーン初期セット
		int		InitializeResult	() ;							// --- リザルトシーン初期セット
		int		KeyInput			() ;							// --- キー情報取得
		int		SetController		( int arg_ConNo ) ;				// --- コントローラーの割り当て
		int		SetPlayerNo			( int arg_PNo ) ;				// --- プレイヤー番号のセット
		int		SetTarget			( Player* argp_Tgt ) ;			// --- ターゲットをセット
		int		SelectEquip			() ;							// --- 装備変更
		int		MoveState			() ;							// --- 平行移動
		int		TriggerState		() ;							// --- 〇×等トリガーチェック
		int		InitStay			() ;							// --- 静止に移行
		int		InitJump			() ;							// --- ジャンプに必要なパラメータを設定
		int		InitShot			( int arg_ShotLR ) ;			// --- 射撃に必要なパラメータを設定
		int		InitBackActiv		() ;							// --- 背中装備発動に必要なパラメータを設定
		int		InitDash			() ;							// --- ダッシュに必要なパラメータを設定
		int		InitDie				() ;							// --- 死亡に必要なパラメータを設定
		int		GetFlg				( int arg_FlgType ) ;			// --- 各種フラグ取得
		int		LookTarget			() ;							// --- ターゲットの方向を向く
		int		Move				() ;							// --- 移動系ヒットチェック
		int		HitCheckStage		( VECTOR &arg_Spd ) ;			// --- 床ヒットチェック
		int		HitCheckWall		( VECTOR &arg_Spd ) ;			// --- 壁ヒットチェック
		int		GetEquipType		( int arg_EquipNo ) ;			// --- 装備の種類を返す
		int		GetEquipNowBul		( int arg_EquipNo ) ;			// --- 装備の残弾数を返す
		int		ControlAirSpd		( int arg_Direction ) ;			// --- 空中での速度制御
		int		CutHoming			() ;							// --- 誘導切り
		int		KeyLock				( int arg_LockFlg ) ;			// --- キー受付切り替え
		int		GetSelecting		() ;							// --- 選択中の武器をゲット

		// --- 参照頻度が高いので公開
		int					m_Key[3] ;						// --- キー情報 : データ,トリガー,1フレーム前
		HitCheckInfo		m_HitCaps ;						// --- ヒットチェック用カプセル
		float				m_HitPoint ;					// --- 体力
		VECTOR				m_HomingPt ;					// --- ホーミング目標座標

	private :
		int					m_PlayerNo ;					// --- プレイヤー番号
		int					m_ConNo ;						// --- コントローラーの番号
//		int					m_Key[3] ;						// --- キー情報 : データ,トリガー,1フレーム前
		int					m_EquipNo[3] ;					// --- 装備の種類
		GunMember			m_EquipGun[2] ;					// --- 装備する銃
		BackUnitMember		m_EquipBack ;					// --- 装備する背中装備
		u_int				m_Flg ;							// --- 各種フラグ
		// --- バトル中に必要な情報
		Player*				mp_Target ;						// --- 標的(相手プレイヤー)
		int					m_MoveDir ;						// --- 移動方向
		float				m_SpdAbs ;						// --- 平行移動速度絶対値
		int					m_RespawnTime ;					// --- 復活までの時間
		Shadow				m_Shadow ;						// --- 影
		// --- カスタマイズ中に必要な情報
		int					m_SelectingEq ;					// --- 変更中の装備
} ;


/* [EOF] */
