//////////////////////////////////////////////////////////////////////////////
//																			//
//							Common.h										//
//																			//
//							作成日時 : 2020.9/16							//
//							作成者   : 天童律希								//
//																			//
//							共通ヘッダーファイル							//
//																			//
//////////////////////////////////////////////////////////////////////////////

// --- クラス定義ヘッダーファイルのインクルード ------------------------------------------------------- //
#include "GameObject.h"													// --- 基底クラス
#include "HitCheckInfo.h"												// --- ヒットチェック情報
#include "Animation.h"													// --- アニメーション
#include "Character.h"													// --- キャラクター
#include "Equip.h"														// --- 装備
#include "GunData.h"													// --- 銃
#include "BackUnit.h"													// --- 背中装備
#include "Shadow.h"														// --- 影
#include "Player.h"														// --- プレイヤー
#include "BackGround.h"													// --- 背景
#include "GunBackGround.h"												// --- 銃表示小背景
#include "Stage.h"														// --- ステージ
#include "HItPointManager.h"											// --- HPバー
#include "LimitTime.h"													// --- 制限時間
#include "Bullet.h"														// --- 弾丸
#include "BoxObj.h"														// --- ステージ障害物
#include "BackObject.h"													// --- ステージ外オブジェクト
#include "Camera.h"														// --- カメラオブジェクト
#include "TitleButton.h"												// --- タイトルボタン
#include "BallteManager.h"												// --- 勝敗管理オブジェクト
#include "SceneLoop.h"													// --- シーン管理関数
#include "ResultButton.h"												// --- リザルト管理
#include "Ready.h"														// --- 開始前判定
#include "Start.h"														// --- 開始判定
#include "Finish.h"														// --- 終了判定
#include "Score.h"														// --- 撃破
#include "CustomCheck.h"												// --- YesNoの判定
#include "SoundManager.h"												// --- BGM,SEファイル管理クラス
#include "Arrow.h"														// --- 矢印の判定

// --- 画面サイズ --------------------------------------------------------------------------- //
#define SCREEN_MODE						FALSE							// --- スクリーンモード

#define WINDOW_SIZE_W					1120							// --- 画面横幅ウィンドウモード
#define WINDOW_SIZE_H					630								// --- 画面縦幅ウィンドウモード

#define COLOR_BIT						32								// --- カラービット

// --- 各種オブジェクト使用数 --------------------------------------------------------------- //
#define MODEL_MEMBER					16								// --- 各メンバーモデル管理最大数
#define MODEL_MAX						MODEL_MEMBER * 3				// --- モデル最大数
#define GRAPH_MAX						25								// --- グラフィック最大数

// --- アニメーション使用数 ----------------------------------------------------------------- //
#define ANIM_MEMBER						12								// --- 各メンバーアニメーション管理最大数
#define ANIM_MAX						ANIM_MEMBER * 3					// --- アニメーション最大数

// --- モデルハンドル担当番号 ------------------------------------------------------------------- //
#define MODEL_ARAMAKI					MODEL_MEMBER * 0				// --- 荒巻
#define MODEL_KATO						MODEL_MEMBER * 1				// --- 加藤
#define MODEL_TENDO						MODEL_MEMBER * 2				// --- 天童

// --- アニメーションハンドル担当番号 ------------------------------------------------------------------- //
#define ANIM_ARAMAKI					ANIM_MEMBER * 0					// --- 荒巻
#define ANIM_KATO						ANIM_MEMBER * 1					// --- 加藤
#define ANIM_TENDO						ANIM_MEMBER * 2					// --- 天童

// --- モデルファイルパス ------------------------------------------------------------------- //

// --- オブジェクト類
#define MODEL_STAGE_PATH				"Data\\mv1\\Stage.mv1"				// --- 床モデル
#define MODEL_LWALL_PATH				"Data\\mv1\\Lwall.mv1"				// --- 壁（左）モデル
#define MODEL_RWALL_PATH				"Data\\mv1\\Rwall.mv1"				// --- 壁（右）モデル
#define MODEL_BOX_PATH					"Data\\mv1\\Box.mv1"				// --- 箱モデル
#define MODEL_OBJECT_PATH				"Data\\mv1\\Object.mv1"			// --- ステージ埋めのモデル

// --- プレイヤー
#define MODEL_PCHARA_01_PATH			"Data\\mv1\\PChara_01.mv1"		// --- 1Pプレイヤーモデル
#define MODEL_PCHARA_02_PATH			"Data\\mv1\\PChara_02.mv1"		// --- 2Pプレイヤーモデル

// --- アニメーションあり(弾など)
#define MODEL_HORNET_PATH				"Data\\mv1\\Hornet.mv1"			// --- 蜂モデル
#define MODEL_EAGLE_PATH				"Data\\mv1\\Eagle.mv1"				// --- 鳥モデル
#define MODEL_FISH_PATH					"Data\\mv1\\Fish.mv1"				// --- 魚モデル
#define MODEL_SYURIKEN_PATH				"Data\\mv1\\Syuriken.mv1"			// --- 手裏剣モデル
#define MODEL_CANNONBUL_PATH			"Data\\mv1\\CannonBullet.mv1"		// --- 大砲の弾モデル
#define MODEL_MISSILE_PATH				"Data\\mv1\\Missile.mv1"			// --- ミサイルモデル

// --- 銃火器
#define MODEL_BEE_GUN_PATH				"Data\\mv1\\Bee_Gun.mv1"			// --- 蜂の銃
#define MODEL_EAGLE_GUN_PATH			"Data\\mv1\\Eagle_Gun.mv1"			// --- 鳥の銃
#define MODEL_SHIP_GUN_PATH				"Data\\mv1\\Ship_Gun.mv1"			// --- 船の銃
#define MODEL_SYURIKEN_GUN_PATH			"Data\\mv1\\Syuriken_Gun.mv1"		// --- 手裏剣の銃

#define MODEL_WING_PATH					"Data\\mv1\\Wing.mv1"				// --- 羽モデル
#define MODEL_CANNON_PATH				"Data\\mv1\\Cannon.mv1"			// --- 大砲モデル
#define MODEL_MISSILEUN_PATH			"Data\\mv1\\MissileUnit.mv1"		// --- ミサイルユニットモデル

#define MODEL_SHADOW_PATH				"Data\\mv1\\Shadow.mv1"			// --- 影

// --- 2D画像
#define IMAGE_SKY_PATH					"Data\\png\\Sky.png"				// --- 空の画像
#define IMAGE_BG_PATH					"Data\\png\\Bg.png"				// --- セレクト背景の画像
#define IMAGE_TITLE_PATH				"Data\\png\\Title.png"				// --- セレクト背景の画像
#define IMAGE_TITLE_BUTTON_PATH			"Data\\png\\PUSH START.png"		// --- スタートボタン画像

#define IMAGE_BACK_BULLET_PATH			"Data\\png\\BulletsLeft.png"		// --- 銃の後ろの画像
#define IMAGE_GAUGE_PATH				"Data\\png\\Gauge.png"				// --- ゲージの画像
#define IMAGE_HP01_PATH					"Data\\png\\NewGauge01.png"			// --- 1HPゲージの画像
#define IMAGE_DAMAGE_HP01_PATH			"Data\\png\\NewDamageGauge01.png"	// --- ダメージの1HPゲージの画像
#define IMAGE_HP02_PATH					"Data\\png\\NewGauge02.png"			// --- 2HPゲージの画像
#define IMAGE_DAMAGE_HP02_PATH			"Data\\png\\NewDamageGauge02.png"	// --- ダメージの2HPゲージの画像
#define IMAGE_REDHPFRAME_PATH			"Data\\png\\1PFrame.png"			// --- HPの赤枠組みの画像
#define IMAGE_BLUEHPFRAME_PATH			"Data\\png\\2PFrame.png"			// --- HPの青枠組みの画像
#define IMAGE_NUMBER_PATH				"Data\\png\\Number.png"			// --- 数字の画像
#define IMAGE_FONT_PATH					"Data\\png\\Letter.png"			// --- 文字関連の画像
#define IMAGE_WINTEXT_PATH				"Data\\png\\WinnerText.png"		// --- 勝者表示の画像
#define IMAGE_SCORE_PATH				"Data\\png\\Score.png"			// --- スコア
#define IMAGE_ARROW_PATH				"Data\\png\\Arrow.png"			// --- 矢印

// --- 2Dの銃の画像
#define IMAGE_WEPON_PATH				"Data\\png\\Wepon.png"		// --- 武器関係の画像

// --- カスタマイズ画面関連
#define IMAGE_CUSTMIZE_PATH				"Data\\png\\CustomSceneText.png"	// --- カスタマイズ画面UI
#define IMAGE_CUSTTEXT_PATH				"Data\\png\\SelectText.png"		// --- "SELECT YOUR WEAPONS"

// --- アニメーションファイルパス ---------------------------------------------------- //
#define ANIM_PCHARA_PATH				"Data\\mv1\\PChara_A.mv1"				// --- プレイヤー

#define ANIM_WING_PATH					"Data\\mv1\\Wing_Anim.mv1"				// --- 羽アニメーション
#define ANIM_CANNON_PATH				"Data\\mv1\\Cannon_Anim.mv1"			// --- 大砲アニメーション
#define ANIM_MISSILEUN_PATH				"Data\\mv1\\MissileUnit_Anim.mv1"		// --- ミサイルユニットアニメーション

#define ANIM_SYURIKEN_PATH				"Data\\mv1\\Syuriken_Anim.mv1"			// --- 手裏剣アニメーション
#define ANIM_BEE_PATH					"Data\\mv1\\Hornet_Anim.mv1"			// --- 蜂アニメーション
#define ANIM_FISH_PATH					"Data\\mv1\\Fish_Anim.mv1"				// --- 魚アニメーション
#define ANIM_EAGLE_PATH					"Data\\mv1\\Eagle_Anim.mv1"			// --- 鳥アニメーション
#define ANIM_CANNONBUL_PATH				"Data\\mv1\\CannonBullet_Anim.mv1"		// --- 大砲の弾モデル
#define ANIM_MISSILE_PATH				"Data\\mv1\\Missile_Anim.mv1"			// --- ミサイルモデル

// --- 外部参照宣言 ------------------------------------------------------------------------- //
extern GameObject*		g_GameObject		[] ;						// --- 全てのゲームオブジェクト管理ポインタ
extern int				g_TotalObj ;									// --- オブジェクト総数
extern int				g_hModel			[] ;						// --- モデルハンドル
extern int				g_hGraph			[] ;						// --- グラフィックハンドル
extern int				g_hAnim				[] ;						// --- アニメーションハンドル

// --- グローバル関数プロトタイプ宣言 ------------------------------------------------------- //
int		ActionLoop			() ;
int		DrawLoop			() ;
int		SceneLoop			() ;
int		LoadAllData			() ;
int		LoadDataAramaki		() ;
int		LoadDataKato		() ;
int		LoadDataTendo		() ;
int		InitializeAll		() ;
int		InitializeAramaki	() ;
int		InitializeKato		() ;
int		InitializeTendo		() ;


// --- 各自の担当箇所設定用ヘッダーファイルのインクルード ---------------------------------------- //
#include "CommonAramaki.h"												// --- 荒巻
#include "CommonKato.h"													// --- 加藤
#include "CommonTendo.h"												// --- 天童

/* [EOF] */