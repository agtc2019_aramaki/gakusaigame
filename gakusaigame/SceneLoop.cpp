//////////////////////////////////////////////////////////////////////////////
//																			//
//							SceneLoop.cpp									//
//																			//
//							作成日時 : 2020.9/16							//
//							作成者   : 天童律希								//
//																			//
//							シーン遷移管理関数								//
//																			//
//////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include"Common.h"

SCENETABLE g_SceneFunc[] = {
	AllInit ,
	TitleInit ,
	SceneTitle ,
	CustomInit ,
	CustomScene ,
	PlayInit ,
	PlayScene ,
	ResultInit ,
	ResultScene ,
	SceneDebug
} ;


//==============================================================//
//		シーン遷移管理											//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : int 終了値										//
//==============================================================//
int SceneLoop()
{
	g_SceneFunc[g_sNo]() ;

	return 0 ;
}

// --- 全体初期セット --------------------------------------------------------------------------------------- //
int AllInit()
{
	InitializeAll() ;	// --- 全てのオブジェクトの初期化

	g_Sound.AllStop() ;

	g_sNo = eTitleInit ;

	return 0 ;
}

// --- タイトル初期セット --------------------------------------------------------------------------------------- //
int TitleInit()
{
	for ( int i = 0 ; i < g_TotalObj ; i++) {
		g_GameObject[i]->SetUseFlg( FALSE ) ;
	}
	for (int i = 0 ; i < OBJ_TITLE_MAX ; i++){
		g_TButton[i].Initialize(i) ;
	}
	g_Sound.AllStop() ;
	g_Sound.Play( SOUND_TITLEBGM_NO ) ;

	g_sNo = eSceneTitle ;

	return 0 ;
}

// --- タイトル --------------------------------------------------------------------------------------- //
int SceneTitle()
{
	g_BackGround[eBgTitle].SetUseFlg( TRUE ) ;						//	タイトルロゴ
	g_TButton[0].Action() ;
	//	g_TButton[0].SetUseFlg( TRUE ) ;								//	ボタン
	if( g_TButton[0].ASceneFlg )
	{
		g_sNo = eCustomInit ;
	}

	return 0 ;
}

// --- カスタマイズ初期セット --------------------------------------------------------------------------------------- //
int CustomInit()
{
	for ( int i = 0 ; i < g_TotalObj ; i++) {
		g_GameObject[i]->SetUseFlg( FALSE ) ;
	}
	g_BackGround[eBgSelect].SetUseFlg( TRUE ) ;						//	背景

	for (int i = 0 ; i < OBJ_GUNGRAPH_MAX ; i++){
		g_GunBackGround[i].SetActionNo(0) ;	//	銃背景など
	}
	for( int i = 0 ; i < OBJ_ARROWGRAPH_MAX ; i++ )
	{
		g_Arrow[i].SetUseFlg( TRUE ) ;
	}

	g_Player[ePlayer_1].SetPosition( VGet(-90.0f,0.0f,0.0f) ) ;
	g_Player[ePlayer_1].InitializeCustom() ;
	g_Player[ePlayer_2].SetPosition( VGet(90.0f,0.0f,0.0f) ) ;
	g_Player[ePlayer_2].InitializeCustom() ;

	g_Camera[0].SetSceneNo( eCustomScene ) ;
	g_Camera[0].SetUseFlg( TRUE ) ;

	g_CustCheck[0].Initialize() ;

	g_Sound.AllStop() ;
	g_Sound.Play( SOUND_CUSTOMBGM_NO ) ;

	g_sNo = eCustomScene ;

	return 0 ;
}

// --- カスタマイズ --------------------------------------------------------------------------------------- //
int CustomScene()
{
	ActionLoop() ;

	if ( g_CustCheck[0].m_SceneFlg != FALSE ) {
		if ( g_CustCheck[0].m_SceneFlg != -1 ) {
			g_sNo = ePlayInit ;
		}
		else {
			g_sNo = eCustomInit ;
		}
	}

	return 0 ;
}

// --- 戦闘初期セット --------------------------------------------------------------------------------------- //
int PlayInit()
{
	for ( int i = 0 ; i < g_TotalObj ; i++) {
		g_GameObject[i]->SetUseFlg( FALSE ) ;
	}
	/*	Boxを最大数表示するためのInirialize()
	------------------------------------------------------*/
	for (int i = 0 ; i < OBJ_BOX_MAX ; i++){
		g_Box[i].Initialize( i ) ;
	}
	for (int i = 0 ; i < OBJ_BACKOBJ_MAX ; i++){
		g_BackObj[i].Initialize(i) ;			//	背景オブジェクトの表示など
	}
	g_BackGround[eBgSky].SetUseFlg( TRUE ) ;						//	背景
	for (int i = 0 ; i < OBJ_GUNGRAPH_MAX ; i++){
		g_GunBackGround[i].SetActionNo(1) ;	//	銃背景など
	}
	for (int i = 0 ; i < OBJ_HPGRAPH_MAX ; i++){
		g_HitPointManager[i].SetUseFlg(TRUE) ;	//	HPバー
	}
	for (int i = 0 ; i < OBJ_SCOREGRAPH_MAX ; i++){
		g_Score[i].SetUseFlg(TRUE) ;	//	スコア関係
	}

	g_Player[ePlayer_1].SetPosition( VGet(-70.0f,0.0f,0.0f) ) ;
	g_Player[ePlayer_1].InitializeBattle() ;
	g_Player[ePlayer_2].SetPosition( VGet(70.0f,0.0f,0.0f) ) ;
	g_Player[ePlayer_2].InitializeBattle() ;

	g_Camera[0].SetUseFlg( TRUE ) ;
	g_Camera[0].SetSceneNo( ePlayScene ) ;
	g_Stage[0].InitializeBattle() ;

	g_Sound.AllStop() ;
	g_Sound.Play( SOUND_BATTLEBGM_NO ) ;

	g_sNo = ePlayScene ;

	return 0 ;
}

// --- 戦闘 --------------------------------------------------------------------------------------- //
int PlayScene()
{
	ActionLoop() ;
	if( g_Ready[0].ReadyTime > 0 ){
		g_Ready[0].ReadyTime-- ;
		g_Ready[0].SetUseFlg( TRUE ) ;
		for (int i = 0 ; i < OBJ_TIMEGRAPH_MAX ; i++){
			g_LimitTime[i].StartBattle() ;
		}
	}else{
		g_Ready[0].SetUseFlg( FALSE ) ;
	}

	if( (g_Ready[0].ReadyTime <= 0) && (g_Start[0].StartTime > 0)){
		g_Start[0].StartTime-- ;
		g_Start[0].SetUseFlg( TRUE ) ;
		for (int i = 0 ; i < OBJ_TIMEGRAPH_MAX ; i++){
			g_LimitTime[i].StartBattle() ;
		}
	}else{
		g_Start[0].SetUseFlg( FALSE ) ;
	}
	if( g_Start[0].StartTime <= 0 ){
		g_LimitTime[0].AbsFlg = TRUE ;
	}
	if( (g_LimitTime[0].m_time_s <= 0) && (g_Finish[0].SceneTimer > 0) && (g_Start[0].StartTime <= 0) && (g_LimitTime[0].AbsFlg == TRUE) ){
		g_LimitTime[0].AbsFlg = FALSE ;
		g_Finish[0].SetUseFlg( TRUE ) ;						// 終了
		g_Finish[0].SceneTimer-- ;
	}

	if ( (g_LimitTime[0].ChangeTime) && (g_Finish[0].SceneTimer == 0)  ){
		g_sNo = eResultInit ;
	}

	return 0 ;}

// --- 結果発表初期セット --------------------------------------------------------------------------------------- //
int ResultInit()
{
	for ( int i = 0 ; i < g_TotalObj ; i++) {
		g_GameObject[i]->SetUseFlg( FALSE ) ;
	}

	g_BackGround[eBgSelect].SetUseFlg( TRUE ) ;						//	背景

	g_Player[ePlayer_1].SetPosition( VGet(-110.0f,0.0f,0.0f) ) ;
	g_Player[ePlayer_1].InitializeResult() ;
	g_Player[ePlayer_2].SetPosition( VGet(110.0f,0.0f,0.0f) ) ;
	g_Player[ePlayer_2].InitializeResult() ;

	g_Camera[0].SetSceneNo( eCustomScene ) ;
	g_Camera[0].SetUseFlg( TRUE ) ;

	for( int i = 0 ; i < OBJ_RESULT_MAX ; i++ ){
		g_RScene[i].Initialize(i) ;
	}

	g_Sound.AllStop() ;

	g_sNo = eResultScene ;

	return 0 ;
}

// --- 結果発表 --------------------------------------------------------------------------------------- //
int ResultScene()
{
	ActionLoop() ;
	g_RScene[0].Action() ;

	return 0 ;
}

// --- デバッグ用 --------------------------------------------------------------------------------------- //
int SceneDebug()
{
	g_sNo = eAllInit ;

	return 0 ;
}










/* [EOF] */
