/*	=============================================================================================================

		CommonKato.cpp
				作成日時 : 2020/10/08
				作成者	 : 加藤諒人

				加藤担当部分のグローバル変数宣言など

	=============================================================================================================*/
#include<Dxlib.h>
#include "Common.h"


/* グローバル変数宣言
------------------------------------------------------------------------------------------------------*/
Box g_Box[OBJ_BOX_MAX] ;				// --- box
BackObject g_BackObj[OBJ_BACKOBJ_MAX] ;	// --- 背景オブジェクト
TitleButton g_TButton[OBJ_TITLE_MAX] ;	// --- タイトルボタン
ResultButton g_RScene[OBJ_RESULT_MAX] ;	// --- リザルト
int		g_sNo ;
/*	データロード
------------------------------------------------------------------------------------*/
int LoadDataKato()
{
	g_hModel[MODEL_BOX_NO] = MV1LoadModel( MODEL_BOX_PATH ) ;		// --- box
	if ( g_hModel[MODEL_BOX_NO] == -1 ) {
		return 1 ;
	}

	g_hModel[MODEL_OBJECT_NO] = MV1LoadModel( MODEL_OBJECT_PATH ) ; // --- 背景オブジェクト
	if ( g_hModel[MODEL_OBJECT_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_STARTBUT_NO] = LoadGraph( IMAGE_TITLE_BUTTON_PATH ) ; // --- スタートボタン画像
	if ( g_hGraph[IMAGE_STARTBUT_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_WINTEXT_NO] = LoadGraph( IMAGE_WINTEXT_PATH ) ;			// --- 勝利画像
	if( g_hGraph[IMAGE_WINTEXT_NO] == -1 ){
		return 1 ;
	}
	return 0 ;
}

/*	初期セット
------------------------------------------------------------------------------------*/
int InitializeKato()
{
	int i ;				// --- ループ用

	/*	Inirialize()
	------------------------------------------------------*/
	for (i = 0 ; i < OBJ_BOX_MAX ; i++){
		g_Box[i].SystemInitialize() ;
	}

	/*	背景オブジェクトを最大数表示するためのInirialize()
	------------------------------------------------------*/
	for (i = 0 ; i < OBJ_BACKOBJ_MAX ; i++){
		g_BackObj[i].Initialize( i ) ;
	}
	/*	スタートボタンのInitialize()
	======================================================*/
	for ( i = 0 ; i < OBJ_TITLE_MAX ; i++ ){
		g_TButton[i].Initialize(i) ;
		
	}
	/*	リザルトのInitialize()
	======================================================*/
	for ( i = 0 ; i < OBJ_RESULT_MAX ; i++ ){
		g_RScene[i].Initialize(i) ;
	}
	return 0 ;
}


