///////////////////////////////////////////////////////////////////////////////////////////
//																						//
//							Score.cpp													//
//																						//
//							作成日時 : 2020.11/08										//
//							作成者   : 荒巻伸悟											//
//																						//
//							撃破クラスのメソッド定義									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
#include <DxLib.h>
#include "Common.h"

//==============================================================//
//		アクション												//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int Score::Action(){

	Getter() ;

	return 0 ;
}

int Score::Getter(){
	m_ScoreNumber = g_BattleManager.GetScore( m_PlayerNo ) ;
	return 0 ;
} ;

//==============================================================//
//		初期セット												//
//--------------------------------------------------------------//
//		IN  : enumの番号										//
//		OUT : 格納したハンドル									//
//==============================================================//
int Score::Initialize( int arg_ScoreNo ){

	m_PlayerNo = arg_ScoreNo ;

	m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;

	switch( arg_ScoreNo ){
		case 0:
			ScDrawRect[0].left   =   15 ;		// --- 描画開始x座標
			ScDrawRect[0].top    =  100 ;		// --- 描画開始y座標
			ScDrawRect[0].right  =  125 ;		// --- 画像のwidth
			ScDrawRect[0].bottom =   35 ;		// --- 画像のheight

			ScCut_Pt[0].x		  =	   0 ;		// --- 切り取りx座標
			ScCut_Pt[0].y		  =	   0 ;		// --- 切り取りy座標

			m_ScGraph[0] = g_hGraph[IMAGE_SCORE_NO] ;
			break ;

		case 1:
			ScDrawRect[0].left =    820 ;
			ScDrawRect[0].top    =  100 ;		// --- 描画開始y座標
			ScDrawRect[0].right  =  125 ;		// --- 画像のwidth
			ScDrawRect[0].bottom =   35 ;		// --- 画像のheight

			ScCut_Pt[0].x		  =	   0 ;		// --- 切り取りx座標
			ScCut_Pt[0].y		  =	   0 ;		// --- 切り取りy座標

			m_ScGraph[0] = g_hGraph[IMAGE_SCORE_NO] ;
			break ;
	}
	return m_hGraph ;
}

//==============================================================//
//		グラフィックハンドルをグローバル変数から切り取り複製	//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : ロードしたグラフィックハンドル					//
//==============================================================//
int Score::LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo ){
	m_hGraph = DerivationGraph(
		arg_CutRect->left,
		arg_CutRect->top,
		arg_CutRect->right,
		arg_CutRect->bottom,
		g_hGraph[arg_gGraphNo]
		) ;

	return m_hGraph ;
}


//==============================================================//
//		描画													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int Score::Draw()
{
	if ( g_LimitTime[0].m_time_s >= 30 ) {
		// --- "SCORE"
		DrawRectGraph(
			ScDrawRect[0].left,
			ScDrawRect[0].top + 10 ,
			ScCut_Pt[0].x,
			ScCut_Pt[0].y,
			ScDrawRect[0].right,
			ScDrawRect[0].bottom,
			m_ScGraph[0],
			TRUE, FALSE, FALSE
			) ;

		// --- 1番目数字ドロー
		DrawRectGraph(
			ScDrawRect[0].left + 120 ,
			ScDrawRect[0].top,
			40 * (m_ScoreNumber / 10) ,
			0,
			40,
			50,
			m_hGraph,
			TRUE, FALSE, FALSE
			) ;

		// --- 2番目数字ドロー
		DrawRectGraph(
			ScDrawRect[0].left + 120 + 40,
			ScDrawRect[0].top,
			40 * (m_ScoreNumber % 10) ,
			0,
			40,
			50,
			m_hGraph,
			TRUE, FALSE, FALSE
			) ;
	}
	else {
	}

	return 0 ;
}

