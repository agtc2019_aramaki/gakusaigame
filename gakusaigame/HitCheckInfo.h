//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							HitCheckInfo.h														//
//																								//
//							作成日時 : 2020.10/15												//
//							作成者   : 天童律希													//
//																								//
//							ヒットチェック情報													//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		enum定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
// --- 線分/カプセルのデータ -------------------------- //
enum HitInfoCap {
	eHitStart,		// --- 始点
	eHitEnd,		// --- 終点
} ;

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		構造体定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
// --- ヒットチェック用データ
typedef struct {
		VECTOR	m_Param[3] ;		// --- 必要パラメータ
		float	m_CapRad ;			// --- 半径
} HitCheckInfo ;

// --- 床用ヒットチェック結果
typedef struct {
	int		Hit ;					// --- ヒット判定
	VECTOR	Speed ;					// --- 補正後の速度
} HitResultStg ;

// --- プレイヤー対プレイヤー用ヒットチェック結果
typedef struct {
	int		Hit ;					// --- 判定
	float	xDif ;					// --- 補正後のx速度
	float	zDif ;					// --- 補正後のz速度
} HitResultSide ;

// --- プレイヤー対床
HitResultStg	HitCheck_Player_Stage		( const HitCheckInfo &arg_HitPlayer, const VECTOR &arg_Spd, const HitCheckInfo* argp_HitStage ) ;
// --- プレイヤー対壁
float	HitCheck_Player_Wall				( const HitCheckInfo &arg_HitPlayer, const VECTOR &arg_Spd, const HitCheckInfo* argp_HitStage ) ;
// --- 弾丸対床
int				HitCheck_Bullet_Stage		( const HitCheckInfo &arg_HitBullet, const HitCheckInfo* argp_HitStage ) ;
// --- プレイヤー対弾丸
int				HitCheck_Player_Bullet		( const HitCheckInfo &arg_HitPlayer, const HitCheckInfo &arg_HitBullet ) ;
// --- 弾丸対箱
int				HitCheck_Bullet_Box			( const HitCheckInfo &arg_Bullet, const HitCheckInfo* argp_HitBox ) ;
// --- プレイヤー対プレイヤー
HitResultSide	Hitcheck_Player_Player		( const HitCheckInfo &arg_HitMainPl, const VECTOR &arg_Spd,  const HitCheckInfo &arg_HittargetPl ) ;
// --- 移動量の線分対壁(プレイヤー対壁の中で呼び出す)
int				HitCheck_MoveSegment_Wall	( const VECTOR* arg_NowPos, const VECTOR* arg_MovePos, const HitCheckInfo* argp_HitStage ) ;

