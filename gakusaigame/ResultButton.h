///////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							ResultScene.h														//
//																								//
//							作成日時 : 2020.11/8												//
//							作成者   : 加藤諒人													//
//																								//
//							リザルト関連														//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>

//	キー情報格納
enum RBKeyState {
	RBKeyTrg ,		//	トリガー
	RBKeyData ,		//	データ
	RBKeyOld		//	1フレーム前
} ;

//	ActionNo
enum RBActionNo {
	eRBActionInit ,
	eRBActionNow ,
	eRBActionNon ,
} ;

//	1切り取り用
enum WT1No {
	eWT1NoX ,
	eWT1NoY
} ;

//	2切り取り用
enum WT2No {
	eWT2NoX ,
	eWT2NoY
} ;


/*	タイトルボタンクラス
====================================================================================*/
class ResultButton : public GameObject_2D {
	public :
		int Action() override ;					// --- アクション
		int Draw() override ;					// --- 描画
		int Initialize(int RSInit) ;			// --- 初期セット
		int LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo = NULL ) ;	// --- グラフィックのロード
		int KeyInput() ;
		
		int m_RSCnt ;
		int m_KeyTrg[3] ;			// --- キートリガー用フラグ
		int m_ModeFlg ;				// --- ActionNo用フラグ
		int m_SceneMove ;			// --- シーン遷移フラグ
		int m_StartKey ;			// --- スタートボタンフラグ
		int m_L1Key ;				// --- L1ボタンフラグ
		int m_R1Key ;				// --- L2ボタンフラグ
		int m_L2Key ;				// --- R1ボタンフラグ
		int m_R2Key ;				// --- R2ボタンフラグ
		int m_WTDrwFlg ;
		int m_WTDrwNoFlg ;
		int m_WinnerFlg ;
	
private :	
		RECT m_WTDrawRect[3];		// --- 描画矩形
		POINT m_CutPt_WT[3] ;		// --- 切り取り開始位置


} ;







