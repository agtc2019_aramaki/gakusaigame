///////////////////////////////////////////////////////////////////////////////////////////
//																						//
//							BackGround.h												//
//																						//
//							作成日時 : 2020.10/13										//
//							作成者   : 荒巻伸悟											//
//																						//
//							背景オブジェクトのクラス									//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

enum BgGHn {
	eBgTitle,
	eBgSelect,
	eBgSky,
	eBgresult
} ;

class BackGround : public GameObject_2D
{
	public :
		int Action() override ;		// --- アクション...実際に使用するオブジェクトのクラスでオーバーライド
		int	LoadCutGraph( int arg_gGraphNo , RECT* arg_CutRect , int arg_mGraphNo = NULL ) ;// --- グラフィックのロード
		int Draw() override ;
		int Initialize( int arg_BgInit ) ;			// --- 背景画像の初期セット
		int Getter() ;

		int ChangeBack ;
} ;

