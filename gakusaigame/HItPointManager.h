///////////////////////////////////////////////////////////////////////////////////////////
//																						 //
//							HitPointManager.h					 				 		 //
//																	 					 //
//							作成日時 : 2020.10/20					 					 //
//							作成者   : 荒巻伸悟						 					 //
//																	 					 //
//						ヒットポイントの画像や処理のクラス				 				 //
//																						 //
///////////////////////////////////////////////////////////////////////////////////////////
enum HitPBgHn{
	eHit1PBgFrame,
	eHit2PBgFrame,

} ;

enum PHitGauge1ply{
	ehit1PBgDameG,
	eHit1PBgGauge
} ;

enum PHitGauge2ply{
	ehit2PBgDameG,
	eHit2PBgGauge
} ;

class HitPointManager : public GameObject_2D
{
	public :
		int Action() override ;				// --- アクション...実際に使用するオブジェクトのクラスでオーバーライド
		int Draw() override ;				// --- 描画
		int LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo = NULL ) ;	// --- グラフィックのロード
		int Initialize( int arg_HpNo ) ;	// --- HP関連画像の初期セット
		int HpGaugeManager( int arg_plyHpNo ) ;	// --- HPゲージの処理

	private:
		RECT m_HPDrawRect[2] ;		// --- 描画矩形
		POINT m_CutPt_HP[2] ;		// --- 切り取り開始位置
		int m_hGraph_Gauge[2] ;		// --- グラフィックハンドル
		int m_PlayerNo ;			// --- プレイヤー番号を保存
		float m_DispHp ;			// --- HPの減少をしていく格納変数
		float m_HitBullet ;			// --- 赤ゲージの状態
} ;
