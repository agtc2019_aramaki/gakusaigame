//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							Camera.cpp															//
//																								//
//							作成日時 : 2020.11/2												//
//							作成者   : 天童律希													//
//																								//
//							カメラオブジェクト													//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h> 
#include <math.h>
#include "Common.h"

//==================================================================//
//						アクション									//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int Camera::Action()
{
	VECTOR	PlPos[OBJ_PLAYER_MAX] ;		// --- プレイヤー座標
	VECTOR	PosSub ;					
	VECTOR	PosAvg ;
	VECTOR	AddPos ;
	float	SubSize ;
	switch ( m_ActionNo ) 
	{
		// --- 戦闘シーンのアクション
		case ePlayScene :
			for ( int i = 0 ; i < OBJ_PLAYER_MAX ; i++ ) {
				PlPos[i] = g_Player[i].m_HitCaps.m_Param[eHitEnd] ;
			}

			// --- プレイヤー座標の平均値を取得
			PosAvg = VScale( VAdd(PlPos[0], PlPos[1]), 0.5f ) ;

			m_Target = PosAvg ;					// --- 注視点を設定

			// --- プレイヤー座標の差を取得
			PosSub		= VSub( PlPos[0], PlPos[1] ) ;
			PosSub.x	= abs( PosSub.x ) ;
			PosSub.y	= abs( PosSub.y ) ;
			PosSub.z	= abs( PosSub.z ) ;

			SubSize		= VSize( PosSub ) + CAMERA_PARAM_MINDIF ;

			// --- カメラの最遠距離を超えたら
			if ( SubSize > CAMERA_PARAM_MAXDIF ) {
				SubSize = CAMERA_PARAM_MAXDIF ;	// --- カメラの距離を設定し直し
			}

			AddPos.x		= 0.0f ;
			AddPos.y		= cos( DX_PI_F / 3 ) * SubSize ;
			AddPos.z		= sin( DX_PI_F / 3 ) * SubSize * -1 ;

			m_Pos	= VAdd( PosAvg, AddPos ) ;
			m_Pos.y	-= PosSub.y ;
			break ;

		// --- カスタマイズシーン
		case eCustomScene :
			m_Pos		= VGet( 0.0f, 80.0f, -200.0f ) ;
			m_Target	= VGet( 0.0f, 80.0f, 0.0f ) ;
			break ;

		// --- それ以外(エラー)
		default :
			break ;
	}

	return 0 ;
}

//==================================================================//
//		描画(カメラの情報セット)									//
//------------------------------------------------------------------//
//		IN  :	void												//
//		OUT :	終了判定											//
//==================================================================//
int Camera::Draw()
{
	SetCameraPositionAndTargetAndUpVec(
		m_Pos,
		m_Target,
		VGet(0.0f,1.0f,0.0f) ) ;

	return 0 ;
}

//==================================================================//
//		シーン番号のセット											//
//------------------------------------------------------------------//
//		IN  :	シーン番号											//
//		OUT :	終了判定											//
//==================================================================//
int Camera::SetSceneNo( int arg_SceneNo )
{
	m_ActionNo = arg_SceneNo ;

	return 0 ;
}
