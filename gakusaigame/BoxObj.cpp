/*	======================================================================================================================

										BocObj.cpp
										作成日時 : 2020/10/27
										作成者	 : 加藤諒人

										box関連

	====================================================================================================================*/
#include <DxLib.h>
#include "Common.h"
	
/*	Action		[ IN => void   /   OUT => 終了判定 ]
======================================================*/
int Box::Action()
{
	return 0 ;
}

/*	Initialize(初期化)
=======================================================================*/
int Box::Initialize( int arg_BoxNo )
{
		
	/* --- モデルを複数表示する為の座標設定
	------------------------------------------------------------------*/
	switch(arg_BoxNo){
		case 0 :	// --- 中心
			m_Pos = VGet(0.0f , 0.0f , 0.0f) ;
			break ;
		case 1:		// --- 右上
			m_Pos = VGet(150.0f , 0.0f , 150.0f) ;
			break ;
		case 2:		// --- 左上
			m_Pos = VGet(-150.0f , 0.0f , 150.0f) ;
			break ;
		case 3:		// --- 右下
			m_Pos = VGet(150.0f , 0.0f , -150.0f) ;
			break ;
		case 4:		// --- 左下
			m_Pos = VGet(-150.0f , 0.0f , -150.0f) ;
			break ;
	}

	/* --- ヒットチェック用の初期セット
	-----------------------------------------------------------------*/
	/*	boxの上面
	---------------------------------*/
	m_HitTop[0].m_Param[0].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitTop[0].m_Param[0].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitTop[0].m_Param[0].z = m_Pos.z + BOX_HITCHECK_WIDTH ;

	m_HitTop[0].m_Param[1].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitTop[0].m_Param[1].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitTop[0].m_Param[1].z = m_Pos.z + BOX_HITCHECK_WIDTH ;
	
	m_HitTop[0].m_Param[2].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitTop[0].m_Param[2].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitTop[0].m_Param[2].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitTop[1].m_Param[0].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitTop[1].m_Param[0].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitTop[1].m_Param[0].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitTop[1].m_Param[1].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitTop[1].m_Param[1].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitTop[1].m_Param[1].z = m_Pos.z + BOX_HITCHECK_WIDTH ;

	m_HitTop[1].m_Param[2].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitTop[1].m_Param[2].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitTop[1].m_Param[2].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	/*	boxの側面(手前)	
	----------------------------------------------------------*/
	m_HitSide[eBoxTop][0].m_Param[0].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxTop][0].m_Param[0].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxTop][0].m_Param[0].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxTop][0].m_Param[1].x = m_Pos.x + -BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxTop][0].m_Param[1].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxTop][0].m_Param[1].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxTop][0].m_Param[2].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxTop][0].m_Param[2].y = m_Pos.y + 0 ;
	m_HitSide[eBoxTop][0].m_Param[2].z = m_Pos.z - BOX_HITCHECK_WIDTH ;
	
	m_HitSide[eBoxTop][1].m_Param[0].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxTop][1].m_Param[0].y = m_Pos.y + 0 ;
	m_HitSide[eBoxTop][1].m_Param[0].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxTop][1].m_Param[1].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxTop][1].m_Param[1].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxTop][1].m_Param[1].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxTop][1].m_Param[2].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxTop][1].m_Param[2].y = m_Pos.y + 0 ;
	m_HitSide[eBoxTop][1].m_Param[2].z = m_Pos.z - BOX_HITCHECK_WIDTH ;
	
	/*	boxの側面(奥)	
	----------------------------------------------------------*/
	m_HitSide[eBoxBottom][0].m_Param[0].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxBottom][0].m_Param[0].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxBottom][0].m_Param[0].z = m_Pos.z + BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxBottom][0].m_Param[1].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxBottom][0].m_Param[1].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxBottom][0].m_Param[1].z = m_Pos.z + BOX_HITCHECK_WIDTH ;
	
	m_HitSide[eBoxBottom][0].m_Param[2].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxBottom][0].m_Param[2].y = m_Pos.y + 0 ;
	m_HitSide[eBoxBottom][0].m_Param[2].z = m_Pos.z + BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxBottom][1].m_Param[0].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxBottom][1].m_Param[0].y = m_Pos.y + 0 ;
	m_HitSide[eBoxBottom][1].m_Param[0].z = m_Pos.z + BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxBottom][1].m_Param[1].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxBottom][1].m_Param[1].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxBottom][1].m_Param[1].z = m_Pos.z + BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxBottom][1].m_Param[2].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxBottom][1].m_Param[2].y = m_Pos.y + 0 ;
	m_HitSide[eBoxBottom][1].m_Param[2].z = m_Pos.z + BOX_HITCHECK_WIDTH ;
	
	/*	boxの側面(左)
	----------------------------------------------------------*/
	m_HitSide[eBoxLeft][0].m_Param[0].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxLeft][0].m_Param[0].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxLeft][0].m_Param[0].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxLeft][0].m_Param[1].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxLeft][0].m_Param[1].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxLeft][0].m_Param[1].z = m_Pos.z + BOX_HITCHECK_WIDTH ;
	
	m_HitSide[eBoxLeft][0].m_Param[2].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxLeft][0].m_Param[2].y = m_Pos.y + 0 ;
	m_HitSide[eBoxLeft][0].m_Param[2].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxLeft][1].m_Param[0].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxLeft][1].m_Param[0].y = m_Pos.y + 0 ;
	m_HitSide[eBoxLeft][1].m_Param[0].z = m_Pos.z + BOX_HITCHECK_WIDTH ;
	
	m_HitSide[eBoxLeft][1].m_Param[1].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxLeft][1].m_Param[1].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxLeft][1].m_Param[1].z = m_Pos.z + BOX_HITCHECK_WIDTH ;
	
	m_HitSide[eBoxLeft][1].m_Param[2].x = m_Pos.x - BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxLeft][1].m_Param[2].y = m_Pos.y + 0 ;
	m_HitSide[eBoxLeft][1].m_Param[2].z = m_Pos.z - BOX_HITCHECK_WIDTH ;
	
	/*	boxの側面(右)
	----------------------------------------------------------*/
	m_HitSide[eBoxRight][0].m_Param[0].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxRight][0].m_Param[0].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxRight][0].m_Param[0].z = m_Pos.z + BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxRight][0].m_Param[1].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxRight][0].m_Param[1].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxRight][0].m_Param[1].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxRight][0].m_Param[2].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxRight][0].m_Param[2].y = m_Pos.y + 0 ;
	m_HitSide[eBoxRight][0].m_Param[2].z = m_Pos.z + BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxRight][1].m_Param[0].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxRight][1].m_Param[0].y = m_Pos.y + 0 ;
	m_HitSide[eBoxRight][1].m_Param[0].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxRight][1].m_Param[1].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxRight][1].m_Param[1].y = m_Pos.y + BOX_HITCHECK_HEIGHT ;
	m_HitSide[eBoxRight][1].m_Param[1].z = m_Pos.z - BOX_HITCHECK_WIDTH ;

	m_HitSide[eBoxRight][1].m_Param[2].x = m_Pos.x + BOX_HITCHECK_WIDTH ;
	m_HitSide[eBoxRight][1].m_Param[2].y = m_Pos.y + 0 ;
	m_HitSide[eBoxRight][1].m_Param[2].z = m_Pos.z + BOX_HITCHECK_WIDTH ;

	m_UseFlg = TRUE ;

	return 0 ;
}
/*	SystemInitialize()	/	Boxの読み込みやサイズ、回転設定
========================================================================*/
int Box::SystemInitialize()
{
	m_hModel = LoadModel(MODEL_BOX_NO) ;	// --- モデルを読み込む
	m_Rotate = VGet(0.0f,0.0f,0.0f) ;		// --- モデルの回転
	m_Size	 = VGet(1.0f,1.0f,1.0f) ;		// --- モデルのサイズ

	return 0 ;
}


/*	Draw	/	描画
========================================================================*/
int Box::Draw()
{
	MV1SetPosition		( m_hModel, m_Pos ) ;		// --- 座標セット
	MV1SetRotationXYZ	( m_hModel,	m_Rotate ) ;	// --- 回転セット
	MV1SetScale			( m_hModel, m_Size ) ;		// --- 拡縮セット

	MV1DrawModel		( m_hModel ) ;				// --- 描画

	return 0 ;
}


