///////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							ResultScene.cpp														//
//																								//
//							作成日時 : 2020.11/8												//
//							作成者   : 加藤諒人													//
//																								//
//							リザルト関連の処理													//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////




#include <DxLib.h>
#include "Common.h"
/*	Action
===============================================================*/
int ResultButton::Action(){
	switch( m_ModeFlg )
	{
		/*	UseFlgをTRUEにして疑似タイマーをセット
		=============================================*/
		case eRBActionInit :
			m_UseFlg = TRUE ;
			m_WTDrwFlg = TRUE  ;
			m_RSCnt = 450 ;
			m_ModeFlg = eRBActionNow ;

			g_Sound.Play( SOUND_DRUMROLL_NO ) ;
			break ;
		/*	疑似タイマーを稼働させつつロール開始
		==============================================*/
		case eRBActionNow :
			m_RSCnt-- ;
			if(( m_RSCnt & 0x02 ) != 2 ){
				m_WTDrwNoFlg = TRUE ;
			}
			else{
				m_WTDrwNoFlg = FALSE ;
			}
			/*	ロール速度を下げる
			===============================================*/
			if( m_RSCnt < 308 ){
				if(( m_RSCnt & 0x08 ) != 8 ){
					m_WTDrwNoFlg = TRUE ;
				}
				else{
					m_WTDrwNoFlg = FALSE ;
				}
			}
			/*	ロール速度をさらに下げる
			===============================================*/
			if( m_RSCnt < 136 ){
				if(( m_RSCnt & 0x10 ) != 16 ){
					m_WTDrwNoFlg = TRUE ;
				}
				else{
					m_WTDrwNoFlg = FALSE ;
				}
			}

			/*	疑似タイマーが0になったら勝敗を表示する
			==============================================*/
			if( m_RSCnt < 0 ){	
				if( m_WinnerFlg != FALSE ){
					m_WTDrwNoFlg = TRUE ;
				}
				else{
					m_WTDrwNoFlg = FALSE ;
				}
				m_ModeFlg = eRBActionNon ;

				g_Sound.Play( SOUND_RESURTSE_NO ) ;

				g_Player[m_WinnerFlg].ChangeAnim( ePlAnimDie, FALSE ) ;
				g_Player[m_WinnerFlg ^ TRUE].ChangeAnim( ePlAnimRsh, FALSE ) ;

			}
			break ;

		/*	START+R1+R2+L1+L2でタイトルへ戻る処理
		===========================================================================*/
		case eRBActionNon :
			KeyInput() ;
			if( ( m_StartKey == TRUE ) && ( m_L1Key == TRUE ) && ( m_R1Key = TRUE )
				&& (m_L2Key = TRUE) && (m_R2Key == TRUE)){
				g_sNo = eAllInit ;
			}
			break ;
	}

		return 0 ;
}


/*	Initialize
===============================================================*/
int ResultButton::Initialize( int RSInit ){

	m_ModeFlg = eRBActionInit ;
	m_SceneMove = FALSE ;
	m_WTDrwFlg = FALSE ;
	m_WTDrwNoFlg = FALSE ;

	/*	各種キーフラグの初期化
	===========================*/
	m_StartKey = FALSE ;	// --- START
	m_L1Key = FALSE ;		// --- L1
	m_L2Key = FALSE ;		// --- L2
	m_R1Key = FALSE ;		// --- R1
	m_R2Key = FALSE ;		// --- R2

	m_WTDrawRect[0].left	= 200 ;			// --- 描画開始x座標
	m_WTDrawRect[0].top		= 70  ;			// --- 描画開始y座標
	m_WTDrawRect[0].right	= 700 ;			// --- 描画終了x座標
	m_WTDrawRect[0].bottom	= 195 ;			// --- 描画終了x座標

	m_CutPt_WT[0].x		  =	   0 ;			// --- 切り取りx座標
	m_CutPt_WT[0].y		  =	   0 ;			// --- 切り取りy座標

	m_WTDrawRect[1].left	= 630 ;			// --- 描画開始x座標
	m_WTDrawRect[1].top		= 10  ;			// --- 描画開始y座標
	m_WTDrawRect[1].right	= 100 ;			// --- 描画終了x座標
	m_WTDrawRect[1].bottom	= 197 ;			// --- 描画終了x座標

	m_CutPt_WT[1].x		  =	   0 ;			// --- 切り取りx座標
	m_CutPt_WT[1].y		  =	   197 ;			// --- 切り取りy座標

	m_WTDrawRect[2].left	= 630 ;			// --- 描画開始x座標
	m_WTDrawRect[2].top		= 10  ;			// --- 描画開始y座標
	m_WTDrawRect[2].right	= 150 ;			// --- 描画終了x座標
	m_WTDrawRect[2].bottom	= 197 ;			// --- 描画終了x座標

	m_CutPt_WT[2].x		  =	   100 ;			// --- 切り取りx座標
	m_CutPt_WT[2].y		  =	   197 ;			// --- 切り取りy座標

	switch( g_BattleManager.CheckWinner() )
	{
		/*	プレイヤー1が勝利したとき1を表示
		==========================================================================*/
		case ePlayer_1:
			m_WinnerFlg = TRUE ;
			break ;

		/*	プレイヤー2が勝利したとき2を表示
		=========================================================================*/
		case ePlayer_2 :
			m_WinnerFlg = FALSE ;
			break ;
			
	}

	m_hGraph = g_hGraph[IMAGE_WINTEXT_NO] ;	// --- 勝者表示

	m_UseFlg = FALSE ;
	
	return m_hGraph ;

}

/*	Draw
===============================================================*/
int ResultButton::Draw(){
	
	
	/*	PLAYERWINNER
	===============================*/
	if ( m_WTDrwFlg == TRUE ){
		DrawRectGraph(
		m_WTDrawRect[0].left,
		m_WTDrawRect[0].top,
		m_CutPt_WT[0].x,
		m_CutPt_WT[0].y,
		m_WTDrawRect[0].right,
		m_WTDrawRect[0].bottom,
		m_hGraph,
		TRUE, FALSE, FALSE
		) ;
	}

	/*	1 or 2 
	================================*/
	if( m_WTDrwNoFlg == TRUE ){
		DrawRectGraph(
		m_WTDrawRect[1].left,
		m_WTDrawRect[1].top,
		m_CutPt_WT[1].x,
		m_CutPt_WT[1].y,
		m_WTDrawRect[1].right,
		m_WTDrawRect[1].bottom,
		m_hGraph,
		TRUE, FALSE, FALSE
		) ;
	}

	if( m_WTDrwNoFlg == FALSE ){
		DrawRectGraph(
		m_WTDrawRect[2].left,
		m_WTDrawRect[2].top,
		m_CutPt_WT[2].x,
		m_CutPt_WT[2].y,
		m_WTDrawRect[2].right,
		m_WTDrawRect[2].bottom,
		m_hGraph,
		TRUE, FALSE, FALSE
		) ;
	}
	return 0 ;
}

/*	ロード
==============================================================================================*/
int ResultButton::LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo ){
	m_hGraph = DerivationGraph(
		arg_CutRect->left,
		arg_CutRect->top,
		arg_CutRect->right,
		arg_CutRect->bottom,
		g_hGraph[arg_gGraphNo]
		) ;

	return m_hGraph ;
}

/*	キー情報入力待ち
========================================================================================================*/
int ResultButton::KeyInput(){

	m_KeyTrg[RBKeyTrg]	&= 0x00000000 ;									// ---トリガー情報をリセット
	m_KeyTrg[RBKeyOld]	= m_KeyTrg[RBKeyData] ;							// --- 1フレーム前の情報を保存
	m_KeyTrg[RBKeyData]	= GetJoypadInputState( DX_INPUT_KEY_PAD1 ) ;	// --- 現在のキー情報を取得
	
	// --- 各種キートリガー
	if ( ((m_KeyTrg[RBKeyOld] & PAD_INPUT_L) == 0)
			&& ((m_KeyTrg[RBKeyData] & PAD_INPUT_L) != 0) ) {
			m_L1Key = TRUE ;		// --- L1ボタン
	}

	if ( ((m_KeyTrg[RBKeyOld] & PAD_INPUT_R) == 0)
			&& ((m_KeyTrg[RBKeyData] & PAD_INPUT_R) != 0) ) {
			m_L2Key = TRUE ;		// --- L2ボタン
	}
	
	if ( ((m_KeyTrg[RBKeyOld] & PAD_INPUT_R) == 0)
			&& ((m_KeyTrg[RBKeyData] & PAD_INPUT_R) != 0) ) {
			m_R1Key = TRUE ;		// --- R1ボタン
	}

	if ( ((m_KeyTrg[RBKeyOld] & PAD_INPUT_Z) == 0)
			&& ((m_KeyTrg[RBKeyData] & PAD_INPUT_Z) != 0) ) {
			m_R2Key = TRUE ;		// --- R2
	}

	if ( ((m_KeyTrg[RBKeyOld] & PAD_INPUT_M) == 0)
			&& ((m_KeyTrg[RBKeyData] & PAD_INPUT_M) != 0) ) {
			m_StartKey = TRUE ;		// --- STARTボタン
	}

	return 0; 
}