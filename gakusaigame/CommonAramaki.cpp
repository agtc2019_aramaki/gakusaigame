//////////////////////////////////////////////////////////////////////////////////////////////
//																							//
//							CommonAramaki.cpp												//
//																							//
//							作成日時 : 2020.10/08											//
//							作成者   : 荒巻伸悟												//
//																							//
//							荒巻担当部分のグローバル変数宣言など							//
//																							//
//////////////////////////////////////////////////////////////////////////////////////////////
#include <DxLib.h>
#include "Common.h"

BackGround g_BackGround[OBJ_BACKGROUND_MAX] ;			// --- 背景用
GunBackGround g_GunBackGround[OBJ_GUNGRAPH_MAX] ;		// --- 銃関係用
HitPointManager g_HitPointManager[OBJ_HPGRAPH_MAX] ;	// --- HP関係用
LimitTime g_LimitTime[OBJ_TIMEGRAPH_MAX] ;				// --- 制限時間用
Ready g_Ready[OBJ_READYGRAPH_MAX] ;						// --- 開始前用
Start g_Start[OBJ_STARTGRAPH_MAX] ;						// --- 開始用
Finish g_Finish[OBJ_FINISHGRAPH_MAX] ;					// --- 終了用
Score g_Score[OBJ_SCOREGRAPH_MAX] ;						// --- スコア用
Arrow g_Arrow[OBJ_ARROWGRAPH_MAX] ;						// --- 矢印用

int LoadDataAramaki()
{	
	// --- 2D画像 -------------------------------------------------------------------

	g_hGraph[IMAGE_TITLE_NO] = LoadGraph( IMAGE_TITLE_PATH ) ;			// --- タイトル画像
	if ( g_hGraph[IMAGE_TITLE_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_SELECT_NO] = LoadGraph( IMAGE_BG_PATH ) ;			// --- セレクト時背景画像
	if ( g_hGraph[IMAGE_SELECT_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_SKY_NO] = LoadGraph( IMAGE_SKY_PATH ) ;			// --- 空画像
	if ( g_hGraph[IMAGE_SKY_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_BB_NO] = LoadGraph( IMAGE_BACK_BULLET_PATH ) ;	// --- 銃背景画像
	if ( g_hGraph[IMAGE_BB_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_GAUGE_NO] = LoadGraph( IMAGE_GAUGE_PATH ) ;		// --- ゲージ画像
	if ( g_hGraph[IMAGE_GAUGE_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_HP01_NO] = LoadGraph( IMAGE_HP01_PATH ) ;		// --- HPゲージ画像
	if ( g_hGraph[IMAGE_HP01_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_DAMAGE_HP01_NO] = LoadGraph( IMAGE_DAMAGE_HP01_PATH ) ;	// --- ダメージを受けたHP画像
	if ( g_hGraph[IMAGE_DAMAGE_HP01_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_HP02_NO] = LoadGraph( IMAGE_HP02_PATH ) ;		// --- HPゲージ画像
	if ( g_hGraph[IMAGE_HP02_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_DAMAGE_HP02_NO] = LoadGraph( IMAGE_DAMAGE_HP02_PATH ) ;	// --- ダメージを受けたHP画像
	if ( g_hGraph[IMAGE_DAMAGE_HP02_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_REDHPFRAME_NO] = LoadGraph( IMAGE_REDHPFRAME_PATH ) ;		// --- HP赤枠組み画像
	if ( g_hGraph[IMAGE_REDHPFRAME_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_BLUEHPFRAME_NO] = LoadGraph( IMAGE_BLUEHPFRAME_PATH ) ;		// --- HP青枠組み画像
	if ( g_hGraph[IMAGE_BLUEHPFRAME_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_NUMBER_NO] = LoadGraph( IMAGE_NUMBER_PATH ) ;	// --- 数字画像
	if ( g_hGraph[IMAGE_NUMBER_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_WEPON_NO] = LoadGraph( IMAGE_WEPON_PATH ) ;		// --- 武器の画像
	if( g_hGraph[IMAGE_WEPON_NO] == -1 ){
		return 1 ;
	}

	g_hGraph[IMAGE_FONT_NO] = LoadGraph( IMAGE_FONT_PATH ) ;		// --- 文字の画像
	if( g_hGraph[IMAGE_FONT_NO] == -1 ){
		return 1 ;
	}

	g_hGraph[IMAGE_SCORE_NO] = LoadGraph( IMAGE_SCORE_PATH ) ;		// --- 文字の画像
	if( g_hGraph[IMAGE_SCORE_NO] == -1 ){
		return 1 ;
	}

	g_hGraph[IMAGE_ARROW_NO] = LoadGraph( IMAGE_ARROW_PATH ) ;		// --- 矢印の画像
	if( g_hGraph[IMAGE_ARROW_NO] == -1 ){
		MessageBox(NULL, TEXT(""),TEXT(""), MB_OK ) ;
		return 1 ;
	}

	return 0 ;
}

// --- 動かない者たち ----------------------
int InitializeAramaki()
{
	RECT CopyRect ;		// --- 切り取り複製用矩形
	int i ;

	// --- 画像
	CopyRect.top	=    0 ;
	CopyRect.left	=    0 ;
	CopyRect.bottom	= WINDOW_SIZE_H ;
	CopyRect.right	= WINDOW_SIZE_W ;

	// --- 背景の描画ループ
	for( i = 0 ; i < OBJ_BACKGROUND_MAX ; i++ )
	{
		g_BackGround[i].LoadCutGraph( i, &CopyRect ) ;
		g_BackGround[i].Initialize( i ) ;
	}

	// --- 銃関係の描画ループ
	for( i = 0 ; i < OBJ_GUNGRAPH_MAX ; i++ )
	{
		g_GunBackGround[i].LoadCutGraph( i, &CopyRect ) ;
		g_GunBackGround[i].Initialize( i ) ;
		g_GunBackGround[i].ReloadManager( i ) ;
		g_GunBackGround[i].GunToGraph( i ) ;
	}

	// --- HP関係の描画ループ
	for( i = 0 ; i < OBJ_HPGRAPH_MAX ; i++ )
	{
		g_HitPointManager[i].LoadCutGraph( i, &CopyRect ) ;
		g_HitPointManager[i].Initialize( i ) ;
		g_HitPointManager[i].HpGaugeManager( i ) ;
	}

	// --- 制限時間数字の描画ループ
	for( i = 0 ; i < OBJ_TIMEGRAPH_MAX ; i++ )
	{
		g_LimitTime[i].LoadCutGraph( i, &CopyRect ) ;
		g_LimitTime[i].Initialize( i ) ;
	}

	// --- 開始前の描画ループ
	for( i = 0 ; i < OBJ_READYGRAPH_MAX ; i++ )
	{
		g_Ready[i].LoadCutGraph( i, &CopyRect ) ;
		g_Ready[i].Initialize( i ) ;
	}

	// --- 開始の描画ループ
	for( i = 0 ; i < OBJ_STARTGRAPH_MAX ; i++ )
	{
		g_Start[i].LoadCutGraph( i, &CopyRect ) ;
		g_Start[i].Initialize( i ) ;
	}

	// --- 終了の描画ループ
	for( i = 0 ; i < OBJ_SCOREGRAPH_MAX ; i++ )
	{
		g_Score[i].LoadCutGraph( i, &CopyRect ) ;
		g_Score[i].Initialize( i ) ;
	}

	// --- 終了の描画ループ
	for( i = 0 ; i < OBJ_FINISHGRAPH_MAX ; i++ )
	{
		g_Finish[i].LoadCutGraph( i, &CopyRect ) ;
		g_Finish[i].Initialize( i ) ;
	}

	// --- 矢印の描画ループ
	for( i = 0 ; i < OBJ_ARROWGRAPH_MAX ; i++ )
	{
		g_Arrow[i].LoadCutGraph( i, &CopyRect ) ;
		g_Arrow[i].Initialize( i ) ;
	}
	return 0 ;
}

















