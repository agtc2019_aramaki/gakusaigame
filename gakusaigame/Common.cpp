//////////////////////////////////////////////////////////////////////////////
//																			//
//							Common.cpp										//
//																			//
//							作成日時 : 2020.9/16							//
//							作成者   : 天童律希								//
//																			//
//							グローバル関数定義								//
//							グローバル変数宣言								//
//																			//
//////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include"Common.h"

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//	グローバル変数宣言
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
// --- 全てのゲームオブジェクト管理ポインタ --------------------------------------------------------- //
GameObject*		g_GameObject		[
	OBJ_PLAYER_MAX + 
	OBJ_BACKGROUND_MAX +
	OBJ_GUNGRAPH_MAX + 
	OBJ_HPGRAPH_MAX +
	OBJ_TIMEGRAPH_MAX +
	OBJ_STAGE_MAX +
	OBJ_BOX_MAX +
	OBJ_BULLET_MAX +
	OBJ_READYGRAPH_MAX +
	OBJ_STARTGRAPH_MAX +
	OBJ_FINISHGRAPH_MAX +
	OBJ_SCOREGRAPH_MAX +
	OBJ_ARROWGRAPH_MAX + 
	OBJ_CAMERA_MAX +
	OBJ_TITLE_MAX +
	OBJ_CUSTOMCK_MAX
] ;


int		g_TotalObj ;								// --- ゲームオブジェクト総数
// --- モデル等のデータハンドル -------------------------------------------------------------------- //
int		g_hModel			[MODEL_MAX] ;			// --- モデルハンドル
int		g_hGraph			[GRAPH_MAX] ;			// --- グラフィックハンドル
int		g_hAnim				[ANIM_MAX] ;			// --- アニメーションハンドル


// --- ゲームオブジェクト -------------------------------------------------------------------------- //
GameObject* gp_GameObject[] ;						// --- ゲームオブジェクトポインタ

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//	グローバル関数定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//

//==============================================================//
//		全ての画像,モデル,アニメーションのロード				//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : int 成功判定 										//
//==============================================================//
int LoadAllData()
{
	LoadDataAramaki		() ;		// --- 荒巻担当箇所
	LoadDataKato		() ;		// --- 加藤担当箇所
	LoadDataTendo		() ;		// --- 天童担当箇所

	return 0 ;
}

//==============================================================//
//		全てのオブジェクトの初期化								//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : int 成功判定 										//
//==============================================================//
int InitializeAll()
{
	int i ;

	InitializeAramaki		() ;		// --- 荒巻担当箇所
	InitializeKato			() ;		// --- 加藤担当箇所
	InitializeTendo			() ;		// --- 天童担当箇所

	// --- 管理ポインタのセット --------------------------------------------------------------- //
	g_TotalObj = 0 ;
	for ( i = 0 ; i < OBJ_BACKGROUND_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_BackGround[i] ;
		g_TotalObj++ ;
	}
	for (i = 0 ; i < OBJ_BOX_MAX ; i++ ){
		g_GameObject[g_TotalObj] = &g_Box[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_BACKOBJ_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_BackObj[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_STAGE_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_Stage[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_BULLET_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_Bullet[i] ;
		g_TotalObj++ ;
	}

	for ( i = 0 ; i < OBJ_PLAYER_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_Player[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_GUNGRAPH_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_GunBackGround[i] ;
		g_TotalObj++ ;
	}
		for ( i = 0 ; i < OBJ_RESULT_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_RScene[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_HPGRAPH_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_HitPointManager[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_TIMEGRAPH_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_LimitTime[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_READYGRAPH_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_Ready[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_STARTGRAPH_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_Start[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_FINISHGRAPH_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_Finish[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_SCOREGRAPH_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_Score[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_CAMERA_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_Camera[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_TITLE_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_TButton[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_CUSTOMCK_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_CustCheck[i] ;
		g_TotalObj++ ;
	}
	for ( i = 0 ; i < OBJ_ARROWGRAPH_MAX ; i++ ) {
		g_GameObject[g_TotalObj] = &g_Arrow[i] ;
		g_TotalObj++ ;
	}

	g_sNo = eSceneDebug ;

	return 0 ;
}

//==============================================================//
//		全てのオブジェクトのアクション							//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : int 終了値										//
//==============================================================//
int ActionLoop()
{
	for ( int i = 0 ; i < g_TotalObj ; i++ ) {
		if ( g_GameObject[i]->GetUseFlg() ) {
			g_GameObject[i]->Action() ;
		}
	}

	return 0 ;
}


