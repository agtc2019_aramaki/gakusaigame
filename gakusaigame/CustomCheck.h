//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							CustomCheck.h														//
//																								//
//							作成日時 : 2020.11/08												//
//							作成者   : 天童律希													//
//																								//
//							カスタマイズシーンに使用するアレコレ								//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		define定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
#define SIZE_SELECTTEXT_W		870				// --- "SelectYourWeapons"_W
#define SIZE_SELECTTEXT_H		160				// --- "SelectYourWeapons"_H
#define SIZE_STANDBYTEXT_W		400				// --- "Standby"_W
#define SIZE_STANDBYTEXT_H		68				// --- "Standby"_H
#define SIZE_RUREADY_W			982				// --- "AreYouReady"_W
#define SIZE_RUREADY_H			82				// --- "AreYouReady"_H
#define SIZE_YESNO_W			269				// --- "YES""NO"_W
#define SIZE_YESNO_H			117				// --- "YES""NO"_H
#define SIZE_CURSOR_W			75				// --- カーソルW
#define SIZE_CURSOR_H			68				// --- カーソルH

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		enum定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
enum CustomChekGraphNo {
	eCsText_Select,			// --- "SelectYourWeapons"
	eCsText_Standby_1P,		// --- "Standby"
	eCsText_Standby_2P,		// --- "Standby"
	eCsText_RUReady,		// --- "AreYouReady?"
	eCsYes,					// --- "YES"
	eCsNo,					// --- "NO"
	eCsCursor_1P,			// --- 1P選択カーソル
	eCsCursor_2P			// --- 2P選択カーソル
} ;

enum CustCkActNo {
	eCsSelect,				// --- 選択中
	eCsConfirmationInit,	// --- 確認
	eCsConfirmation,		// --- 確認
	eCsSceneAdd,			// --- シーン遷移
} ;

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		クラス定義
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
typedef struct {
	int		m_DrawFlg ;			// --- 使用フラグ
	int		m_hGraph ;			// --- グラフィックハンドル
	POINT	m_DrawPt ;			// --- 描画座標
	POINT	m_DrawOffset ;		// --- 描画オフセット
} DrawData ;

class CustomCheck : public GameObject_2D {
	public :
		int		Action			() override ;	// --- アクション
		int		Draw			() override ;	// --- 描画
		int		LoadData		() ;			// --- データのロード
		int		KeyCheck		() ;			// --- キーデータチェック
		int		Initialize		() ;			// --- 使用前セット
		int		BackToSelect	() ;			// --- 選択に戻る

		// --- 公開変数 ------------------------------------------------- //
		int		m_SceneFlg ;				// --- シーン遷移フラグ

	private :
		DrawData	m_DrawData[8] ;			// --- 描画データ
		int			m_SelectingCursor[2] ;	// --- 選択中のYES/No
		int			m_Key[2][3] ;				// --- プレイヤーキー情報
		int			m_Confirmation[2] ;		// --- 確定フラグ
		int			m_CursorPos[2] ;		// --- カーソル位置
} ;