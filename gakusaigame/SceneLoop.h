//////////////////////////////////////////////////////////////////////////////
//																			//
//							SceneLoop.h										//
//																			//
//							作成日時 : 2020.11/5							//
//							作成者   : 天童律希								//
//																			//
//							シーン遷移管理関数								//
//																			//
//////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>

// --- シーン番号定義 --------------------------------------------------------------------------------- //
enum SceneNo {
	eAllInit ,
	eTitleInit ,
	eSceneTitle ,
	eCustomInit ,
	eCustomScene ,
	ePlayInit ,
	ePlayScene ,
	eResultInit ,
	eResultScene ,
	eSceneDebug
} ;

// --- シーン関数ポインタ定義 ----------------------------------------------------------------------- //
typedef int (*SCENETABLE)() ;

// --- 関数プロトタイプ宣言 ------------------------------------------------------------------------ //
int AllInit() ;
int TitleInit() ;
int SceneTitle() ;
int CustomInit() ;
int CustomScene() ;
int PlayInit() ;
int PlayScene() ;
int ResultInit() ;
int ResultScene() ;
int SceneDebug() ;

