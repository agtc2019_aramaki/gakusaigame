///////////////////////////////////////////////////////////////////////////////////////////
//																						//
//							GameObject.h												//
//																						//
//							作成日時 : 2020.9/16										//
//							作成者   : 天童律希											//
//																						//
//							全てのオブジェクトの基底クラス								//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>

// --- 3D2D共通部分 --------------------------------------------------------------------------------------------------------------------------------- //
class GameObject {
	public :
		GameObject() ;
		int			GetUseFlg			() ;					// --- 使用フラグ取得
		int			SetUseFlg			( int arg_UseFlg ) ;	// --- 使用フラグセット
		virtual int	Action				() = 0 ;				// --- アクション...実際に使用するオブジェクトのクラスでオーバーライド
		virtual int Draw				() = 0 ;				// --- 描画...2Dオブジェクト基底クラスと3Dオブジェクト基底クラスでオーバーライド

	protected :
		int			m_UseFlg ;			// --- 使用フラグ
		int			m_ActionNo ;		// --- アクション番号
		VECTOR		m_Pos ;				// --- ワールド座標(2Dオブジェクトはx,yのみ使用)
} ;

// --- 3Dオブジェクト -------------------------------------------------------------------------------------------------------------------------------- //
class GameObject_3D : public GameObject {
	public :
		GameObject_3D					() ;
		virtual int	Draw				() override ;					// --- 描画...アニメーションを持つキャラクターで更にオーバーライド
		int			LoadModel			( int arg_gModelNo ) ;			// --- モデルのロード
		VECTOR		GetPosition			() ;							// --- 座標の取得
		int			SetPosition			( const VECTOR arg_Pos ) ;	// --- 座標セット
		int			SetRotate			( const VECTOR arg_Rot ) ;	// --- 回転セット
		int			SetScale			( const VECTOR arg_Siz ) ;	// --- 拡縮セット

	protected :
		int			m_hModel ;			// --- モデルハンドル
		VECTOR		m_Spd ;				// --- 速度
		VECTOR		m_Rotate ;			// --- 回転
		VECTOR		m_Size ;			// --- 拡大縮小
} ;

// --- 2Dオブジェクト -------------------------------------------------------------------------------------------------------------------------------- //
class GameObject_2D : public GameObject {
	public :
		virtual int	Draw				() override ;														// --- 描画...複数の画像を使用する場合はオーバーライドする
		virtual int	LoadCutGraph		( int arg_gGraphNo , RECT* arg_CutRect , int arg_mGraphNo = NULL );// --- グラフィックのロード...複数の画像を使用する場合はオーバーライドする

	protected :
		int			m_hGraph ;				// --- グラフィックハンドル
		RECT		m_DrawRect ;			// --- 描画矩形
		POINT		m_CutPt ;				// --- 切り取り開始位置
} ;







/* [EOF] */