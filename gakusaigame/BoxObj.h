/*======================================================================================================================

										BocObj.h
										作成日時 : 2020/10/27
										作成者	 : 加藤諒人

										box
========================================================================================================================*/
#include <DxLib.h>

#define BOX_HITCHECK_HEIGHT	60	// Boxの縦幅のヒットチェック(原点から)
#define BOX_HITCHECK_WIDTH	30	// Boxの横幅のヒットチェック(原点から)

/*  Boxの面に関するenum
===============================================*/
enum BoxHit {		
	eBoxTop ,		// --- 手前
	eBoxBottom ,	// --- 奥
	eBoxLeft ,		// --- 左
	eBoxRight ,		// --- 右
} ;

/*	Boxクラス作成(GameObject_3Dを継承)
===============================================================================================================================*/
class Box : public GameObject_3D {
	public :
		int Action()	override ;			// --- GameObject_3DをActionにオーバーライド（変数や関数などを使えるように）する
		int Draw()		override ;			// --- 描画
		int Initialize( int arg_BoxNo ) ;	// --- 初期セット
		int SystemInitialize() ;
		 
		HitCheckInfo	m_HitSide[4][2] ;		// --- Boxの横面
		HitCheckInfo	m_HitTop[2] ;			// --- Boxの上面
} ;


