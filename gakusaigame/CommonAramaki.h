//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							CommonAramaki.h														//
//																								//
//							作成日時 : 2020.10/08												//
//							作成者   : 荒巻伸悟													//
//																								//
//							荒巻担当部分のextern宣言など										//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

// --- クラス定義ヘッダーファイルのインクルード ------------------------------------------------------- //

#define MODEL_STAGE_NO			MODEL_ARAMAKI + 0								// --- 床
#define MODEL_LWALL_NO			MODEL_ARAMAKI + 1								// --- 壁（左）
#define MODEL_RWALL_NO			MODEL_ARAMAKI + 2								// --- 壁（右）
#define MODEL_BOX_NO			MODEL_ARAMAKI + 3								// --- 箱
#define MODEL_OBJECT_NO			MODEL_ARAMAKI + 4								// --- ステージ埋め

#define IMAGE_TITLE_NO					0						// --- タイトルの画像
#define IMAGE_SELECT_NO					1						// --- 背景の画像
#define IMAGE_SKY_NO					2						// --- 空の画像
#define IMAGE_BB_NO						3						// --- 銃の後ろの画像
#define IMAGE_GAUGE_NO					4						// --- ゲージの画像
#define IMAGE_HP01_NO					5						// --- 1HPゲージの画像
#define IMAGE_DAMAGE_HP01_NO			6						// --- ダメージの1HPゲージの画像
#define IMAGE_HP02_NO					7						// --- 2HPゲージの画像
#define IMAGE_DAMAGE_HP02_NO			8						// --- ダメージの2HPゲージの画像
#define IMAGE_REDHPFRAME_NO				9						// --- HPの赤枠組みの画像
#define IMAGE_BLUEHPFRAME_NO			10						// --- HPの青枠組みの画像
#define IMAGE_NUMBER_NO					11						// --- 数字の画像
#define IMAGE_WEPON_NO					12						// --- 武器関係の画像
#define IMAGE_STARTBUT_NO				13						// --- スタートボタン
#define IMAGE_FONT_NO					14						// --- 文字関連
#define IMAGE_WINTEXT_NO				15						// --- 勝利の画像
#define IMAGE_CUSTOMIZE_NO				16						// --- カスタマイズの画像
#define IMAGE_CUSTTEXT_NO				17						// --- でかいテキスト
#define IMAGE_SCORE_NO					18						// --- スコア
#define IMAGE_ARROW_NO					19						// --- 矢印

#define OBJ_BACKGROUND_MAX				4								// --- 背景最大数
#define OBJ_GUNGRAPH_MAX				OBJ_PLAYER_MAX * 3				// --- 銃器系の最大数
#define OBJ_HPGRAPH_MAX					OBJ_PLAYER_MAX					// --- HP系の最大数
#define OBJ_TIMEGRAPH_MAX			    1								// --- 制限時間の最大数
#define OBJ_READYGRAPH_MAX				1								// --- 開始前の最大数
#define OBJ_STARTGRAPH_MAX				1								// --- 開始の最大数
#define OBJ_FINISHGRAPH_MAX			    1								// --- 終了の最大数
#define OBJ_SCOREGRAPH_MAX				2								// --- スコアの最大数
#define OBJ_ARROWGRAPH_MAX				2								// --- 矢印の最大数

// --- HP最大の大きさ
#define HPGAUGE_MAX  480
#define GUN_MAX_X     80
#define GUN_MAX_Y     75

/* ========================================= */
/*			外部参照宣言					 */
/* ========================================= */
extern BackGround g_BackGround[] ;				// --- 背景用
extern GunBackGround g_GunBackGround[] ;		// --- 銃関係用
extern HitPointManager g_HitPointManager[] ;	// --- HP関係用
extern LimitTime g_LimitTime[] ;				// --- 制限時間用
extern Ready g_Ready[] ;						// --- 開始前用
extern Start g_Start[] ;						// --- 開始用
extern Finish g_Finish[] ;						// --- 終了用
extern Score g_Score[] ;						// --- スコア用
extern Arrow g_Arrow[] ;						// --- 矢印用