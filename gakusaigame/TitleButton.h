/*	=============================================================================================================

							TitleButton.h
									作成日時 : 2020/10/08
									作成者	 : 加藤諒人

									タイトルボタン関連

	=============================================================================================================*/
#include <DxLib.h>

//	キー情報格納
enum STKeyState {
	STKeyTrg ,		//	トリガー
	STKeyData ,		//	データ
	STKeyOld		//	1フレーム前
} ;

//	アクションNo
enum STActionNo {
	eSTActionInit ,
	eSTActionNow ,
	eSTActionNon ,
} ;


/*	タイトルボタンクラス
====================================================================================*/
class TitleButton : public GameObject_2D {
	public :
		int Action() override ;					// --- アクション
		int Draw() override ;					// --- 描画
		int Initialize(int TBInit) ;						// --- 初期セット
		int keyInput() ;						// --- キー入力
		int LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo = NULL ) ;	// --- グラフィックのロード

		int m_Skey[3] ;			//	ボタンの状態
		int BDFlg ;				//	確認用
		int ASceneFlg ;			//	シーン遷移フラグ
		int m_CBFlg ;			//	ボタン点滅用
		int m_CBCnt ;
		int m_StartCnt ;
} ;