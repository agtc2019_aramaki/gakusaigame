//////////////////////////////////////////////////////////////////////////////////////////////
//																							//
//							CommonTendo.cpp													//
//																							//
//							作成日時 : 2020.10/08											//
//							作成者   : 天童律希												//
//																							//
//							天童担当部分のグローバル変数宣言など							//
//																							//
//////////////////////////////////////////////////////////////////////////////////////////////

#include <DxLib.h>
#include "Common.h"

// --- グローバル変数宣言 ------------------------------------------------------------------------------ //
Player			g_Player[OBJ_PLAYER_MAX] ;		// --- プレイヤー
Stage			g_Stage[OBJ_STAGE_MAX] ;		// --- ステージ
BulletObj		g_Bullet[OBJ_BULLET_MAX] ;		// --- 弾丸
Camera			g_Camera[OBJ_CAMERA_MAX] ;		// --- カメラオブジェクト
CustomCheck		g_CustCheck[OBJ_CUSTOMCK_MAX] ;	// --- カスタマイズ画面UIオブジェクト

GunData			g_GunData[DATA_GUN_MAX] ;		// --- 銃データ
BackUnitData	g_BackData[DATA_BACK_MAX] ;		// --- 背部装備データ
BulletData		g_BulletData[DATA_BULLET_MAX] ;	// --- 弾丸データ
BattleManager	g_BattleManager ;				// --- 勝敗判定クラス
SoundManager	g_Sound ;						// --- サウンド管理クラス

//======================================================//
//		担当箇所のデータロード							//
//------------------------------------------------------//
//		IN  : void										//
//		OUT : 終了判定									//
//======================================================//
int LoadDataTendo()
{
	// --- モデル ------------------------------------------------------------------------------------------------- //
	g_hModel[MODEL_PCHARA_01_NO] = MV1LoadModel( MODEL_PCHARA_01_PATH ) ; // --- 1Pプレイヤー
	if ( g_hModel[MODEL_PCHARA_01_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_PCHARA_02_NO] = MV1LoadModel( MODEL_PCHARA_02_PATH ) ; // --- 2Pプレイヤー
	if ( g_hModel[MODEL_PCHARA_02_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_SYURIKEN_GUN_NO] = MV1LoadModel( MODEL_SYURIKEN_GUN_PATH ) ; // --- 手裏剣銃
	if ( g_hModel[MODEL_SYURIKEN_GUN_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_EAGLE_GUN_NO] = MV1LoadModel( MODEL_EAGLE_GUN_PATH ) ; // --- 鳥銃
	if ( g_hModel[MODEL_EAGLE_GUN_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_SHIP_GUN_NO] = MV1LoadModel( MODEL_SHIP_GUN_PATH ) ; // --- 魚銃
	if ( g_hModel[MODEL_SHIP_GUN_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_BEE_GUN_NO] = MV1LoadModel( MODEL_BEE_GUN_PATH ) ; // --- 蜂銃
	if ( g_hModel[MODEL_BEE_GUN_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_WING_NO] = MV1LoadModel( MODEL_WING_PATH ) ; // --- 翼
	if ( g_hModel[MODEL_WING_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_CANNON_NO] = MV1LoadModel( MODEL_CANNON_PATH ) ; // --- 大砲モデル
	if ( g_hModel[MODEL_CANNON_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_MISSILEUN_NO] = MV1LoadModel( MODEL_MISSILEUN_PATH ) ; // --- ミサイルユニットモデル
	if ( g_hModel[MODEL_MISSILEUN_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_STAGE_NO] = MV1LoadModel( MODEL_STAGE_PATH ) ;		// --- ステージ
	if ( g_hModel[MODEL_STAGE_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_SYURIKEN_NO] = MV1LoadModel( MODEL_SYURIKEN_PATH ) ;		// --- 手裏剣
	if ( g_hModel[MODEL_SYURIKEN_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_EAGLE_NO] = MV1LoadModel( MODEL_EAGLE_PATH ) ;		// --- 鳥
	if ( g_hModel[MODEL_EAGLE_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_FISH_NO] = MV1LoadModel( MODEL_FISH_PATH ) ;		// --- 魚
	if ( g_hModel[MODEL_FISH_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_HORNET_NO] = MV1LoadModel( MODEL_HORNET_PATH ) ;		// --- 蜂
	if ( g_hModel[MODEL_HORNET_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_MISSILE_NO] = MV1LoadModel( MODEL_MISSILE_PATH ) ;		// --- ミサイル
	if ( g_hModel[MODEL_MISSILE_NO] == -1 ) {
		return 1 ;
	}
	g_hModel[MODEL_CANNONBUL_NO] = MV1LoadModel( MODEL_CANNONBUL_PATH ) ;		// --- 大砲の弾
	if ( g_hModel[MODEL_CANNONBUL_NO] == -1 ) {
		return 1 ;
	}

	g_hModel[MODEL_SHADOW_NO] = MV1LoadModel( MODEL_SHADOW_PATH ) ;		// --- 影
	if ( g_hModel[MODEL_SHADOW_NO] == -1 ) {
		return 1 ;
	}

	// --- アニメーション ---------------------------------------------------------------------------------------------- //
	g_hAnim[ANIM_PLAYER_NO] = MV1LoadModel( ANIM_PCHARA_PATH ) ;	// --- プレイヤーアニメーション
	if ( g_hAnim[ANIM_PLAYER_NO] == -1 ) {
		return 1 ;
	}
	g_hAnim[ANIM_CANNON_NO] = MV1LoadModel( ANIM_CANNON_PATH ) ;	// --- 大砲アニメーション
	if ( g_hAnim[ANIM_CANNON_NO] == -1 ) {
		return 1 ;
	}
	g_hAnim[ANIM_WING_NO] = MV1LoadModel( ANIM_WING_PATH ) ;	// --- 翼アニメーション
	if ( g_hAnim[ANIM_WING_NO] == -1 ) {
		return 1 ;
	}
	g_hAnim[ANIM_MISSILEUN_NO] = MV1LoadModel( ANIM_MISSILEUN_PATH ) ;	// --- ミサイルユニットアニメーション
	if ( g_hAnim[ANIM_MISSILEUN_NO] == -1 ) {
		return 1 ;
	}
	g_hAnim[ANIM_SYURIKEN_NO] = MV1LoadModel( ANIM_SYURIKEN_PATH ) ;	// --- 手裏剣アニメーション
	if ( g_hAnim[ANIM_SYURIKEN_NO] == -1 ) {
		return 1 ;
	}
	g_hAnim[ANIM_EAGLE_NO] = MV1LoadModel( ANIM_EAGLE_PATH ) ;	// --- 鳥アニメーション
	if ( g_hAnim[ANIM_EAGLE_NO] == -1 ) {
		return 1 ;
	}
	g_hAnim[ANIM_FISH_NO] = MV1LoadModel( ANIM_FISH_PATH ) ;	// --- 魚アニメーション
	if ( g_hAnim[ANIM_FISH_NO] == -1 ) {
		return 1 ;
	}
	g_hAnim[ANIM_BEE_NO] = MV1LoadModel( ANIM_BEE_PATH ) ;	// --- 蜂アニメーション
	if ( g_hAnim[ANIM_BEE_NO] == -1 ) {
		return 1 ;
	}
	g_hAnim[ANIM_MISSILE_NO] = MV1LoadModel( ANIM_MISSILE_PATH ) ;		// --- ミサイル
	if ( g_hModel[ANIM_MISSILE_NO] == -1 ) {
		return 1 ;
	}
	g_hAnim[ANIM_CANNONBUL_NO] = MV1LoadModel( ANIM_CANNONBUL_PATH ) ;		// --- 大砲の弾
	if ( g_hModel[ANIM_CANNONBUL_NO] == -1 ) {
		return 1 ;
	}

	g_hGraph[IMAGE_CUSTOMIZE_NO] = LoadGraph( IMAGE_CUSTMIZE_PATH ) ;
	if ( g_hGraph[IMAGE_CUSTOMIZE_NO] == -1 ) {
		return 1 ;
	}
	g_hGraph[IMAGE_CUSTTEXT_NO] = LoadGraph( IMAGE_CUSTTEXT_PATH ) ;
	if ( g_hGraph[IMAGE_CUSTTEXT_NO] == -1 ) {
		return 1 ;
	}

	g_Sound.LoadData() ;

	return 0 ;
}

//======================================================//
//		担当箇所の初期セット							//
//------------------------------------------------------//
//		IN  : void										//
//		OUT : 終了判定									//
//======================================================//
int InitializeTendo()
{
	// --- 装備のグローバルデータセット
	g_GunData[eGunSyuriken].LoadModelEq( MODEL_SYURIKEN_GUN_NO ) ;			// --- 手裏剣銃
	g_GunData[eGunSyuriken].InitializeData( eGunSyuriken ) ;

	g_GunData[eGunBeard].LoadModelEq( MODEL_EAGLE_GUN_NO ) ;				// --- 鳥銃
	g_GunData[eGunBeard].InitializeData( eGunBeard ) ;

	g_GunData[eGunFish].LoadModelEq( MODEL_SHIP_GUN_NO ) ;					// --- 魚銃
	g_GunData[eGunFish].InitializeData( eGunFish ) ;

	g_GunData[eGunBee].LoadModelEq( MODEL_BEE_GUN_NO ) ;					// --- 蜂銃
	g_GunData[eGunBee].InitializeData( eGunBee ) ;

	g_BackData[eUnitWing].LoadModelEq( MODEL_WING_NO ) ;					// --- 翼
	g_BackData[eUnitWing].InitializeData( 0, eUnitWing, ANIM_WING_NO ) ;

	g_BackData[eUnitCannon].LoadModelEq( MODEL_CANNON_NO ) ;				// --- 大砲
	g_BackData[eUnitCannon].InitializeData( 0, eUnitCannon, ANIM_CANNON_NO ) ;

	g_BackData[eUnitMissile].LoadModelEq( MODEL_MISSILEUN_NO ) ;			// --- ミサイル
	g_BackData[eUnitMissile].InitializeData( 0, eUnitMissile, ANIM_MISSILEUN_NO ) ;

	// --- 弾丸のグローバルデータセット ------------------------------ //
	// --- 手裏剣
	g_BulletData[eBulSyuriken].InitializeData( 
		eBulSyuriken,			// --- 種類
		MODEL_SYURIKEN_NO,		// --- モデル番号
		ANIM_SYURIKEN_NO,		// --- アニメーション番号
		0.0f,					// --- ホーミング力
		0,						// --- ホーミング持続時間
		7.5f,					// --- 速度絶対値
		6.0f ) ;				// --- 威力

	// --- 鳥
	g_BulletData[eBulEagle].InitializeData( 
		eBulEagle,				// --- 種類
		MODEL_EAGLE_NO,			// --- モデル番号
		ANIM_EAGLE_NO,			// --- アニメーション番号
		0.04f,					// --- ホーミング力
		70,						// --- ホーミング持続時間
		5.0f,					// --- 速度絶対値
		7.0f ) ;				// --- 威力

	// --- 蜂
	g_BulletData[eBulBee].InitializeData( 
		eBulBee,				// --- 種類
		MODEL_HORNET_NO,		// --- モデル番号
		ANIM_BEE_NO,			// --- アニメーション番号
		0.04f,					// --- ホーミング力
		100,					// --- ホーミング持続時間
		4.0f,					// --- 速度絶対値
		8.0f ) ;				// --- 威力

	// --- 魚
	g_BulletData[eBulFish].InitializeData( 
		eBulFish,				// --- 種類
		MODEL_FISH_NO,			// --- モデル番号
		ANIM_FISH_NO,			// --- アニメーション番号
		0.08f,					// --- ホーミング力
		200,					// --- ホーミング持続時間
		3.0f,					// --- 速度絶対値
		10.0f ) ;				// --- 威力

	// --- 大砲
	g_BulletData[eBulCannon].InitializeData( 
		eBulCannon,				// --- 種類
		MODEL_CANNONBUL_NO,		// --- モデル番号
		ANIM_CANNONBUL_NO,		// --- アニメーション番号
		0.05f,					// --- ホーミング力
		10,						// --- ホーミング持続時間
		20.0f,					// --- 速度絶対値
		40.0f ) ;				// --- 威力

	// --- ミサイル
	g_BulletData[eBulMissile].InitializeData( 
		eBulMissile,			// --- 種類
		MODEL_MISSILE_NO,		// --- モデル番号
		ANIM_MISSILE_NO,		// --- アニメーション番号
		0.03f,					// --- ホーミング力
		200,					// --- ホーミング持続時間
		7.0f,					// --- 速度絶対値
		7.0f ) ;				// --- 威力

	// --- プレイヤー初期セット ------------------------------------------------------------------------- //
	g_Player[ePlayer_1].SetUseFlg( TRUE ) ;
	g_Player[ePlayer_1].LoadModel( MODEL_PCHARA_01_NO ) ;				// --- モデル
	g_Player[ePlayer_1].SetController( DX_INPUT_KEY_PAD1 ) ;			// --- 対応するコントローラーのセット
	g_Player[ePlayer_1].SetPlayerNo( ePlayer_1 ) ;						// --- プレイヤー番号のセット
	g_Player[ePlayer_1].SetTarget( &g_Player[ePlayer_2] ) ;				// --- ターゲットのセット
	g_Player[ePlayer_1].InitializeData() ;

	g_Player[ePlayer_2].SetUseFlg( TRUE ) ; ;
	g_Player[ePlayer_2].LoadModel( MODEL_PCHARA_02_NO ) ;				// --- モデル
	g_Player[ePlayer_2].SetController( DX_INPUT_PAD2 ) ;				// --- 対応するコントローラーのセット
	g_Player[ePlayer_2].SetPlayerNo( ePlayer_2 ) ;						// --- プレイヤー番号のセット
	g_Player[ePlayer_2].SetTarget( &g_Player[ePlayer_1] ) ;				// --- ターゲットのセット
	g_Player[ePlayer_2].InitializeData() ;

	// --- ステージ初期セット --------------------------------------------------------------------------- //
	g_Stage[0].Initialize() ;

	// --- 射撃関数ポインタセット ---------------------------------------------------------------------- //
	GunData::SetActivateFunction() ;

	g_CustCheck[0].LoadData() ;

	g_BattleManager.StartBattle() ;

	return 0 ;
}

//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//
//		基礎部分ではない自作グローバル関数
//
//*-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+//
//==============================================================//
//		度数→孤度変換											//
//--------------------------------------------------------------//
//		IN  : オイラー角										//
//		OUT : ラジアン											//
//==============================================================//
float EulerToRadian( float arg_EulerAngle )
{
	return arg_EulerAngle * (DX_PI_F / 180.0f) ;
}

//==============================================================//
//		孤度→度数変換											//
//--------------------------------------------------------------//
//		IN  : ラジアン											//
//		OUT : オイラー角										//
//==============================================================//
float RadianToEuler( float arg_RadianAngle )
{
	return arg_RadianAngle / (DX_PI_F / 180.0f) ;
}

