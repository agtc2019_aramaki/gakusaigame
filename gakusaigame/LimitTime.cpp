///////////////////////////////////////////////////////////////////////////////////////////
//																						//
//							LimitTime.cpp												//
//																						//
//							作成日時 : 2020.10/22										//
//							作成者   : 荒巻伸悟											//
//																						//
//							制限時間クラスのメソッド定義								//
//																						//
//////////////////////////////////////////////////////////////////////////////////////////
#include <DxLib.h>
#include "Common.h"

//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int LimitTime::Action(){

	switch ( m_ActionNo )
	{
		case 0 :
			// --- 時間計測	timediffは9000000〜0に減っていく(90〜0秒)
			timediff = int( timelimit - (GetNowHiPerformanceCount() - starttime ) ) ;
			m_time_s = timediff / 1000000 ;
			// 秒を出す
			if( timediff <= 0 ){
				m_ActionNo = 1 ;
			}
			break ;

		case 1 :
			ChangeTime = TRUE ;
			break ;
	}

	return 0 ;
}

//==============================================================//
//		時間の判定												//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int LimitTime::StartBattle(){
	starttime =  GetNowHiPerformanceCount() ;
	SetUseFlg(TRUE) ;		//	制限時間
	m_ActionNo = 0 ;
	ChangeTime = FALSE ;
	AbsFlg = FALSE ;
	return 0 ;
}


//==============================================================//
//		初期セット												//
//--------------------------------------------------------------//
//		IN  : enumの番号										//
//		OUT : 格納したハンドル									//
//==============================================================//
int LimitTime::Initialize( int arg_LimitNo ){

	StoringTime[0] = 510 ;
	StoringTime[1] = 550 ;
	AbsFlg = FALSE ;

	// 時間のリミットを設定(90秒)
	timelimit = 61000000 ;


	// --- 数字画像のずれ　多分数字の大きさによってるかも.1〜2 + 41, 8 - 1

	switch( m_time_s )
	{
		case Number_00:
			m_DrawRect.left   = StoringTime[0] ;		// --- 描画開始x座標
			m_DrawRect.top    =  20 ;		// --- 描画開始y座標
			m_DrawRect.right  =  42 ;		// --- 画像のwidth
			m_DrawRect.bottom =  50 ;		// --- 画像のheight

			m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case Number_01:
			m_DrawRect.left   =  StoringTime[0] ;		// --- 描画開始x座標
			m_DrawRect.top    =   20 ;		// --- 描画開始y座標
			m_DrawRect.right  =   40 ;		// --- 画像のwidth
			m_DrawRect.bottom =   50 ;		// --- 画像のheight

			m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case Number_02:
			m_DrawRect.left   =  StoringTime[0] ;		// --- 描画開始x座標
			m_DrawRect.top    =   20 ;		// --- 描画開始y座標
			m_DrawRect.right  =   40 ;		// --- 画像のwidth
			m_DrawRect.bottom =   50 ;		// --- 画像のheight

			m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case Number_03:
			m_DrawRect.left   =  StoringTime[0] ;		// --- 描画開始x座標
			m_DrawRect.top    =   20 ;		// --- 描画開始y座標
			m_DrawRect.right  =   40 ;		// --- 画像のwidth
			m_DrawRect.bottom =   50 ;		// --- 画像のheight

			m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case Number_04:
			m_DrawRect.left   =  StoringTime[0] ;		// --- 描画開始x座標
			m_DrawRect.top    =   20 ;		// --- 描画開始y座標
			m_DrawRect.right  =   40 ;		// --- 画像のwidth
			m_DrawRect.bottom =   50 ;		// --- 画像のheight

			m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case Number_05:
			m_DrawRect.left   =  StoringTime[0] ;		// --- 描画開始x座標
			m_DrawRect.top    =   20 ;		// --- 描画開始y座標
			m_DrawRect.right  =   40 ;		// --- 画像のwidth
			m_DrawRect.bottom =   50 ;		// --- 画像のheight

			m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case Number_06:
			m_DrawRect.left   =  StoringTime[0] ;		// --- 描画開始x座標
			m_DrawRect.top    =   20 ;		// --- 描画開始y座標
			m_DrawRect.right  =   40 ;		// --- 画像のwidth
			m_DrawRect.bottom =   50 ;		// --- 画像のheight

			m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case Number_07:
			m_DrawRect.left   =  StoringTime[0] ;		// --- 描画開始x座標
			m_DrawRect.top    =   20 ;		// --- 描画開始y座標
			m_DrawRect.right  =   40 ;		// --- 画像のwidth
			m_DrawRect.bottom =   50 ;		// --- 画像のheight

			m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case Number_08:
			m_DrawRect.left   =  StoringTime[0] ;		// --- 描画開始x座標
			m_DrawRect.top    =   20 ;		// --- 描画開始y座標
			m_DrawRect.right  =   40 ;		// --- 画像のwidth
			m_DrawRect.bottom =   50 ;		// --- 画像のheight

			m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;

		case Number_09:
			m_DrawRect.left   =  StoringTime[0] ;		// --- 描画開始x座標
			m_DrawRect.top    =   20 ;		// --- 描画開始y座標
			m_DrawRect.right  =   40 ;		// --- 画像のwidth
			m_DrawRect.bottom =   50 ;		// --- 画像のheight

			m_hGraph = g_hGraph[IMAGE_NUMBER_NO] ;
			break ;
	}
	return m_hGraph ;
}


//==============================================================//
//		グラフィックハンドルをグローバル変数から切り取り複製	//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : ロードしたグラフィックハンドル					//
//==============================================================//
int LimitTime::LoadCutGraph( int arg_gGraphNo, RECT* arg_CutRect, int arg_mGraphNo ){
	m_hGraph = DerivationGraph(
		arg_CutRect->left,
		arg_CutRect->top,
		arg_CutRect->right,
		arg_CutRect->bottom,
		g_hGraph[arg_gGraphNo]
		) ;

	return m_hGraph ;
}



//==============================================================//
//		描画													//
//--------------------------------------------------------------//
//		IN  : void												//
//		OUT : 終了判定											//
//==============================================================//
int LimitTime::Draw(){
	// --- 1番目数字ドロー
	DrawRectGraph(
		m_DrawRect.left = StoringTime[0],
		m_DrawRect.top,
		40 * (m_time_s / 10),
		0,
		m_DrawRect.right,
		m_DrawRect.bottom,
		m_hGraph,
		TRUE, FALSE, FALSE
		) ;

	// --- 2番目数字ドロー
	DrawRectGraph(
		m_DrawRect.left = StoringTime[1],
		m_DrawRect.top,
		40 * (m_time_s % 10),
		0,
		m_DrawRect.right,
		m_DrawRect.bottom,
		m_hGraph,
		TRUE, FALSE, FALSE
		) ;
	return 0 ;
}



