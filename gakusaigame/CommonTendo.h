//////////////////////////////////////////////////////////////////////////////////////////////////
//																								//
//							CommonTendo.h														//
//																								//
//							作成日時 : 2020.10/08												//
//							作成者   : 天童律希													//
//																								//
//							天童担当部分のextern宣言など										//
//																								//
//////////////////////////////////////////////////////////////////////////////////////////////////

// --- オブジェクト最大数 ---------------------------------------------------------------------------- //
#define OBJ_PLAYER_MAX			2		// --- プレイヤー最大数
#define OBJ_STAGE_MAX			1		// --- ステージ最大数
#define OBJ_BULLET_MAX			64		// --- 弾丸オブジェクト最大数
#define OBJ_CAMERA_MAX			1		// --- カメラオブジェクト最大数
#define OBJ_BTLMANAG_MAX		1		// --- 勝敗管理オブジェクト最大数
#define OBJ_CUSTOMCK_MAX		1		// --- カスタマイズ画面UIオブジェクト最大数

// --- 参照用データ最大数
#define DATA_GUN_MAX			4		// --- 銃データ最大数
#define DATA_BACK_MAX			3		// --- 背部装備データ最大数
#define DATA_BULLET_MAX			8		// --- 弾丸データ最大数

// --- モデルハンドル配列番号 ----------------------------------------------------------------------- //
#define MODEL_PCHARA_01_NO				MODEL_TENDO + 0					// --- キャラ１P
#define MODEL_PCHARA_02_NO				MODEL_TENDO + 1					// --- キャラ２P

#define MODEL_HORNET_NO					MODEL_TENDO + 2					// --- 蜂
#define MODEL_EAGLE_NO					MODEL_TENDO + 3					// --- 鳥
#define MODEL_FISH_NO					MODEL_TENDO + 4					// --- 魚
#define MODEL_SYURIKEN_NO				MODEL_TENDO + 5					// --- 手裏剣

#define MODEL_BEE_GUN_NO				MODEL_TENDO + 6					// --- 蜂の銃
#define MODEL_EAGLE_GUN_NO				MODEL_TENDO + 7					// --- 鳥の銃
#define MODEL_SHIP_GUN_NO				MODEL_TENDO + 8					// --- 魚の銃
#define MODEL_SYURIKEN_GUN_NO			MODEL_TENDO + 9					// --- 手裏剣の銃

#define MODEL_WING_NO					MODEL_TENDO + 10				// --- 翼ユニット
#define MODEL_CANNON_NO					MODEL_TENDO + 11				// --- 大砲ユニット
#define MODEL_MISSILEUN_NO				MODEL_TENDO + 12				// --- ミサイルユニット

#define MODEL_MISSILE_NO				MODEL_TENDO + 13				// --- ミサイル
#define MODEL_CANNONBUL_NO				MODEL_TENDO + 14				// --- 大砲の弾

#define MODEL_SHADOW_NO					MODEL_TENDO + 15				// --- 影

// --- アニメーションハンドル配列番号 ---------------------------------------------------------------- //
#define ANIM_PLAYER_NO					ANIM_TENDO + 0					// --- プレイヤーアニメーション
#define ANIM_CANNON_NO					ANIM_TENDO + 1					// --- 大砲アニメーション
#define ANIM_WING_NO					ANIM_TENDO + 2					// --- 翼アニメーション
#define ANIM_MISSILEUN_NO				ANIM_TENDO + 3					// --- ミサイルユニットアニメーション

#define ANIM_SYURIKEN_NO				ANIM_TENDO + 4					// --- 手裏剣アニメーション
#define ANIM_EAGLE_NO					ANIM_TENDO + 5					// --- 鳥アニメーション
#define ANIM_BEE_NO						ANIM_TENDO + 6					// --- 蜂アニメーション
#define ANIM_FISH_NO					ANIM_TENDO + 7					// --- 魚アニメーション

#define ANIM_MISSILE_NO					ANIM_TENDO + 8					// --- ミサイル
#define ANIM_CANNONBUL_NO				ANIM_TENDO + 9					// --- 大砲の弾

// --- 外部参照宣言 ---------------------------------------------------------------------------------- //
extern Player			g_Player[OBJ_PLAYER_MAX] ;		// --- プレイヤー
extern Stage			g_Stage[OBJ_STAGE_MAX] ;		// --- ステージ
extern BulletObj		g_Bullet[OBJ_BULLET_MAX] ;		// --- 弾丸
extern Camera			g_Camera[OBJ_CAMERA_MAX] ;		// --- カメラオブジェクト
extern CustomCheck		g_CustCheck[OBJ_CUSTOMCK_MAX] ;	// --- カスタマイズ画面UIオブジェクト

extern GunData			g_GunData[DATA_GUN_MAX] ;		// --- 銃データ
extern BackUnitData		g_BackData[DATA_BACK_MAX] ;		// --- 背部装備データ
extern BulletData		g_BulletData[DATA_BULLET_MAX] ;	// --- 弾丸データ
extern BattleManager	g_BattleManager ;				// --- 勝敗判定クラス
extern SoundManager		g_Sound ;						// --- サウンド管理クラス

// --- 基礎部分ではない自作グローバル関数 ------------------------------------------------------------------------------------- //
float	EulerToRadian( float arg_EulerAngle ) ;			// --- 度数→孤度変換
float	RadianToEuler( float arg_RadianAngle ) ;		// --- 孤度→度数変換

